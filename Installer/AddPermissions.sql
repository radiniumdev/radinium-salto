IF DATABASE_PRINCIPAL_ID('Network Service') IS NULL
BEGIN
	CREATE USER [Network Service] FOR LOGIN [NT AUTHORITY\Network Service];
END
go
ALTER ROLE [db_owner] ADD MEMBER [Network Service];
go