﻿using RadiniumSAS.Admin;
using RadiniumSAS.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VideoOS.Platform;
using VideoOS.Platform.Background;
using VideoOS.Platform.Messaging;

namespace RadiniumSAS.Background
{
    public class QueryResponder : BackgroundPlugin
    {
        public override Guid Id
        {
            get
            {
                return RadiniumSASDefinition.RadiniumSaltoQueryResponderPlugin;
            }
        }

        public override string Name
        {
            get
            {
                return "Radinium Access Query Responder";
            }
        }

        public override List<EnvironmentType> TargetEnvironments
        {
            get
            {
                return new List<EnvironmentType>() { EnvironmentType.Service, EnvironmentType.Standalone };
            }
        }

        public object QueryUsers(Message message, FQID destination, FQID sender)
        {
            List<SaltoDbUser> users = new List<SaltoDbUser>();
            try
            {
                string sqlConnection = Utility.SQL_CONNECTION_STRING();
                if (message.Data as string != null)
                {
                    sqlConnection = message.Data as string;
                }
                using (SqlConnection conn = new SqlConnection(sqlConnection))
                {
                    using (SaltoDbContext context = new SaltoDbContext(conn, true))
                    {
                        users = context.Users.OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToList();
                    }
                }
                users.ForEach(x => { x.Photo = null; });
            }
            catch (Exception e)
            {
            }
            Message responseUsers = new Message(RadiniumSASDefinition.RADINIUM_API_RESPONSE_USERS, users.ToArray());
            comms.TransmitMessage(responseUsers, message.ExternalMessageSourceEndPoint, null, null);
            return null;
        }

        public object QueryUserImage(Message message, FQID destination, FQID sender)
        {
            if (message.Data as string == null)
            {
                return null;
            }
            byte[] photo = null;
            string userId = message.Data as string;
            byte[] userIdBytes = UTF8Encoding.UTF8.GetBytes(userId);
            try
            {
                using (SqlConnection conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                {
                    using (SaltoDbContext context = new SaltoDbContext(conn, true))
                    {
                        SaltoDbUser user = context.Users.FirstOrDefault(x => x.ExtUserID == userId);
                        if (user == null)
                        {
                            photo = new byte[0];
                        }
                        else
                        {
                            photo = user.Photo;
                        }
                    }
                }
            }
            catch(Exception)
            {
                photo = new byte[0];
            }
            Message responseImage = new Message(RadiniumSASDefinition.RADINIUM_API_RESPONSE_PHOTO, new byte[][] { userIdBytes, photo });
            comms.TransmitMessage(responseImage, message.ExternalMessageSourceEndPoint, null, null);
            return null;
        }

        public object SaveUserImage(Message message, FQID destination, FQID sender)
        {
            if (message.Data as byte[][] == null)
            {
                return null;
            }

            byte[][] dataArr = message.Data as byte[][];
            if (dataArr.Length != 2)
            {
                return null;
            }
            byte[] userId = dataArr[0];
            byte[] photo = dataArr[1];
            string ExtUserId = UTF8Encoding.UTF8.GetString(userId);
            try
            {
                using (SqlConnection conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                {
                    using (SaltoDbContext context = new SaltoDbContext(conn, true))
                    {
                        SaltoDbUser user = context.Users.FirstOrDefault(x => x.ExtUserID == ExtUserId);
                        user.Photo = photo;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception)
            { }
            return null;
        }


        public object UpdateUserText(Message message, FQID destination, FQID sender)
        {
            string[] userData = message.Data as string[];
            if(userData == null)
            {
                return null;
            }
            if(userData.Length != 4)
            {
                return null;
            }
            string extUserId = userData[0];
            string phoneNumber = userData[1];
            string gpf1 = userData[2];
            string gpf2 = userData[3];

            try
            {
                using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                {
                    using (var context = new SaltoDbContext(conn, true))
                    {
                        SaltoDbUser user = context.Users.FirstOrDefault(x => x.ExtUserID == extUserId);
                        user.PhoneNumber = phoneNumber;
                        user.GPF1 = gpf1;
                        user.GPF2 = gpf2;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception)
            { }

            return null;
        }

        public override void Close()
        {
            comms.UnRegisterCommunicationFilter(queryUsers);
            comms.UnRegisterCommunicationFilter(savePhoto);
            comms.UnRegisterCommunicationFilter(getPhoto);
            comms.UnRegisterCommunicationFilter(updateUserText);
            MessageCommunicationManager.Stop(EnvironmentManager.Instance.CurrentSite.ServerId);
        }

        private object queryUsers, savePhoto, getPhoto, updateUserText;

        MessageCommunication comms = null;
        public override void Init()
        {
            ThreadPool.QueueUserWorkItem((x) =>
            {
                EnvironmentManager.Instance.Log(false, "SALTO QueryResponder", "Query responder starting!");
                MessageCommunicationManager.Start(EnvironmentManager.Instance.CurrentSite.ServerId);
                MessageCommunicationManager.Security = MessageCommunicationManager.SecurityLevel.Low;
                Thread.Sleep(10000);
                comms = MessageCommunicationManager.Get(EnvironmentManager.Instance.CurrentSite.ServerId);
                for (int i = 0; i < 50 && !comms.IsConnected; i++)
                {
                    Thread.Sleep(200);
                }
                queryUsers = comms.RegisterCommunicationFilter(QueryUsers, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_API_REQUEST_USERS));
                getPhoto = comms.RegisterCommunicationFilter(QueryUserImage, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_API_REQUEST_PHOTO));
                savePhoto = comms.RegisterCommunicationFilter(SaveUserImage, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_API_UPDATEPHOTO));
                updateUserText = comms.RegisterCommunicationFilter(UpdateUserText, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_API_UPDATEUSER));
                EnvironmentManager.Instance.Log(false, "SALTO QueryResponder", "Query responder started!");
            });
        }
    }
}