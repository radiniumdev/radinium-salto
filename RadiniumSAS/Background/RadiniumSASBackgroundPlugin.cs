using RadiniumSAS.Admin;
using RadiniumSAS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using VideoOS.Platform;
using VideoOS.Platform.Background;
using VideoOS.Platform.Data;

namespace RadiniumSAS.Background
{

    /// <summary>
    /// A background plugin will be started during application start and be running until the user logs off or application terminates.<br/>
    /// The Environment will call the methods Init() and Close() when the user login and logout, 
    /// so the background task can flush any cached information.<br/>
    /// The base class implementation of the LoadProperties can get a set of configuration, 
    /// e.g. the configuration saved by the Options Dialog in the Smart Client or a configuration set saved in one of the administrators.  
    /// Identification of which configuration to get is done via the GUID.<br/>
    /// The SaveProperties method can be used if updating of configuration is relevant.
    /// <br/>
    /// The configuration is stored on the server the application is logged into, and should be refreshed when the ApplicationLoggedOn method is called.
    /// Configuration can be user private or shared with all users.<br/>
    /// <br/>
    /// This plugin could be listening to the Message with MessageId == Server.ConfigurationChangedIndication to when when to reload its configuration.  
    /// This event is send by the environment within 60 second after the administrator has changed the configuration.
    /// </summary>
    public class RadiniumSASBackgroundPlugin : BackgroundPlugin
    {
        const int MAX_SALTO_EVENTS_COUNT = 2000;
        const int AUDIT_TRAIL_UPDATE_SLEEP_MS = 2500;
        const int CONFIGURATION_UPDATE_SLEEP_MS = 30000;

        private static volatile bool _stop = false;
        private Thread _thread;
        /// <summary>
        /// Gets the unique id identifying this plugin component
        /// </summary>
        public override Guid Id
        {
            get { return RadiniumSASDefinition.RadiniumSASBackgroundPlugin; }
        }

        /// <summary>
        /// The name of this background plugin
        /// </summary>
        public override String Name
        {
            get { return "RadiniumSAS BackgroundPlugin"; }
        }

        Dictionary<int, string> allSaltoOperations = new Dictionary<int, string>();

        /// <summary>
        /// Called by the Environment when the user has logged in.
        /// </summary>
        public override void Init()
        {

            Array arr = Enum.GetValues(typeof(SaltoOperation));
            for (int i = 0; i < arr.Length; i++)
            {
                allSaltoOperations[(int)arr.GetValue(i)] = ((SaltoOperation)arr.GetValue(i)).ToDescriptionString();
            }

            _stop = false;
            _thread = new Thread(new ThreadStart(Run));
            _thread.Name = "RadiniumSAS Background Thread";
            _thread.Start();

        }

        /// <summary>
        /// Called by the Environment when the user log's out.
        /// You should close all remote sessions and flush cache information, as the
        /// user might logon to another server next time.
        /// </summary>
        public override void Close()
        {
            _stop = true;
        }

        /// <summary>
        /// Define in what Environments the current background task should be started.
        /// </summary>
        public override List<EnvironmentType> TargetEnvironments
        {
            get { return new List<EnvironmentType>() { EnvironmentType.Service }; } // Default will run in the Event Server
        }

        private List<Item> doors = new List<Item>();
        private object doorsLock = new object();

        public Dictionary<long, SaltoAuditEntry> entryDictionary = new Dictionary<long, SaltoAuditEntry>();

        [EventGuid(RadiniumSASDefinition.RadiniumAccessAlarmStr)]
        [Description("SALTO Alarm event")]
        public static List<SaltoOperation> AlarmEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.AlarmIntrusionOnline,
            SaltoOperation.AlarmTamperOnline,
            SaltoOperation.DuressAlarm
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessStartOfficeModeStr)]
        [Description("Start of office mode event")]
        public static List<SaltoOperation> StartOfficeModeEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.StartOfOfficeMode,
            SaltoOperation.StartOfOfficeModeOnline,
            SaltoOperation.StartOfOfficeModeOnlineHost
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessEndOfficeModeStr)]
        [Description("Office mode ended")]
        public static List<SaltoOperation> EndOfficeModeEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.EndOfOfficeMode,
            SaltoOperation.EndOfOfficeModeKeypad,
            SaltoOperation.EndOfOfficeModeOnline,
            SaltoOperation.EndOfOfficeModeOnlineHost
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessPrivacyStartStr)]
        [Description("Start of privacy")]
        public static List<SaltoOperation> PrivacyStartEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.StartOfPrivacy
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessPrivacyEndStr)]
        [Description("End of privacy")]
        public static List<SaltoOperation> PrivacyEndEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.EndOfPrivacy
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessAdminStr)]
        [Description("Administrative or maintenance event")]
        public static List<SaltoOperation> AdminEventtypes = new List<SaltoOperation>()
        {
            SaltoOperation.AutomaticChange,
            SaltoOperation.BlacklistKeyDeleted,
            SaltoOperation.DoorProbablyOpenKeyAndPin,
            SaltoOperation.DoorProgrammedWithSpareKey,
            SaltoOperation.ExpirationAutomaticallyExtendedOffline,
            SaltoOperation.FirstDoubleCardRead,
            SaltoOperation.HotelGuestCancelled,
            SaltoOperation.IncorrectClockValue,
            SaltoOperation.KeyDeletedOnline,
            SaltoOperation.KeyInsertedEnergySavingDevice,
            SaltoOperation.KeyRemovedEnergySavingDevice,
            SaltoOperation.KeyUpdatedOnline,
            SaltoOperation.KeyUpdatedOutOfSiteOnline,
            SaltoOperation.NewHotelGuestKey,
            SaltoOperation.NewRenovationCode,
            SaltoOperation.NewRenovationCodeForKeyOnline,
            SaltoOperation.OnlinePeripheralUpdated,
            SaltoOperation.OpeningOrClosing,
            SaltoOperation.PPDConnection,
            SaltoOperation.RFLockDateAndTipeUpdated,
            SaltoOperation.RFLockUpdated,
            SaltoOperation.RoomPreparedEnergySavingDevice,
            SaltoOperation.TimeModifiedDaylightSaving,
            SaltoOperation.UnknownOperation,
            SaltoOperation.StartOfForcedClosingOnlineHost,
            SaltoOperation.EndOfForcedClosingOnlineHost,
            SaltoOperation.WarningKeyHasNotBeenCompletelyUpdatedOnline,
            SaltoOperation.SoftwareOperatorCommandOpen,
            SaltoOperation.SoftwareOperatorCommandClose,
            SaltoOperation.SoftwareOperatorEmergencyEnd,
            SaltoOperation.SoftwareOperatorEmergencyEvacuation,
            SaltoOperation.SoftwareOperatorEmergencyLockdown,
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessDeniedOpenStr)]
        [Description("Denied to open a door")]
        public static List<SaltoOperation> DeniedOpenEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.OpeningNotAllowedAntipassback,
            SaltoOperation.OpeningNotAllowedDeniedByHost,
            SaltoOperation.OpeningNotAllowedDoorInEmergencyState,
            SaltoOperation.OpeningNotAllowedHotelGuestKeyCancelled,
            SaltoOperation.OpeningNotAllowedInvalidPin,
            SaltoOperation.OpeningNotAllowedKeyCancelled,
            SaltoOperation.OpeningNotAllowedKeyDoesNotOverridePrivacy,
            SaltoOperation.OpeningNotAllowedKeyExpired,
            SaltoOperation.OpeningNotAllowedKeyNotActivated,
            SaltoOperation.OpeningNotAllowedKeyOutOfDate,
            SaltoOperation.OpeningNotAllowedKeyWithDataManipulated,
            SaltoOperation.OpeningNotAllowedKeyWithOldRenovationNumber,
            SaltoOperation.OpeningNotAllowedLockerOccupancyTimeout,
            SaltoOperation.OpeningNotAllowedNoAssociatedAuthorization,
            SaltoOperation.OpeningNotAllowedOldHotelGuestKey,
            SaltoOperation.OpeningNotAllowedOutOfTime,
            SaltoOperation.OpeningNotAllowedSecondDoubleCardNotPresented,
            SaltoOperation.OpeningNotAllowedUnableToAuditOnTheKey,
            SaltoOperation.OpeningNotAllowedUniqueOpeningKeyAlreadyUsed,
            SaltoOperation.OpeningNotAllowedKeyNotAllowedInThisDoor
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessCommsLostStr)]
        [Description("Communications lost")]
        public static List<SaltoOperation> CommunicationLostStatusEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.CommunicationWithReaderLost,
            SaltoOperation.CommunicationWithSaltoSoftwareLost
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessCommsReestablishedStr)]
        [Description("Communcations re-established")]
        public static List<SaltoOperation> CommunicationReestablishedStatusEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.CommunicationWithReaderReestablished,
            SaltoOperation.CommunicationWithSaltoSoftwareReestablished
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessDeniedCloseStr)]
        [Description("Denied close of door")]
        public static List<SaltoOperation> DeniedCloseEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.ClosingNotAllowedDoorInEmergencyState
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessHardwareFailStr)]
        [Description("Hardware failure")]
        public static List<SaltoOperation> HardwareFailureEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.UnableToPerformOpenCloseOperationDueToHardwareFailure
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessDoorOpenStr)]
        [Description("Door opened")]
        public static List<SaltoOperation> DoorOpenEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.DoorOpenedAfterSecondCard,
            SaltoOperation.DoorOpenedInsideHandle,
            SaltoOperation.DoorOpenedKey,
            SaltoOperation.DoorOpenedKeyAndKeyboard,
            SaltoOperation.DoorOpenedMechanicalRelay,
            SaltoOperation.DoorOpenedOnlineCommand,
            SaltoOperation.DoorOpenedSpareCardHotel,
            SaltoOperation.DoorOpenedSwitch,
            SaltoOperation.DoorOpenedWithPPD
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessDoorCloseStr)]
        [Description("Door closed")]
        public static List<SaltoOperation> DoorCloseEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.DoorClosedKey,
            SaltoOperation.DoorClosedKeyAndKeyboard,
            SaltoOperation.DoorClosedKeyboard,
            SaltoOperation.DoorClosedSwitch
        };

        [EventGuid(RadiniumSASDefinition.RadiniumAccessLowBatteryStr)]
        [Description("Low battery")]
        public static List<SaltoOperation> LowBatteryEventTypes = new List<SaltoOperation>()
        {
            SaltoOperation.LowBatteryLevel,
            SaltoOperation.OpeningNotAllowedRunOutOfBattery
        };

        private static List<List<SaltoOperation>> AllOperationsSingleton { get; set; }

        public static Dictionary<List<SaltoOperation>, KeyValuePair<Guid, string> > OperationToGuidStringMap = new Dictionary<List<SaltoOperation>, KeyValuePair<Guid, string> >();

        public static List<List<SaltoOperation>> AllOperationsList 
        { 
            get
            {
                if(AllOperationsSingleton == null)
                {
                    AllOperationsSingleton = new List<List<SaltoOperation>>();
                    FieldInfo[] members = typeof(RadiniumSASBackgroundPlugin).GetFields();
                    foreach (FieldInfo member in members)
                    {
                        if (member.CustomAttributes.Any(x => x.AttributeType == typeof(EventGuidAttribute)) && 
                            member.CustomAttributes.Any(x => x.AttributeType == typeof(DescriptionAttribute)) &&
                            member.FieldType == typeof(List<SaltoOperation>))
                        {
                            List<SaltoOperation> operationList = member.GetValue(null) as List<SaltoOperation>;
                            if(operationList != null)
                            {
                                AllOperationsSingleton.Add(operationList);
                                Guid eventGuid = member.GetCustomAttribute<EventGuidAttribute>().EventGuid;
                                string description = member.GetCustomAttribute<DescriptionAttribute>().Description;
                                OperationToGuidStringMap.Add(operationList, new KeyValuePair<Guid, string>(eventGuid, description));
                            }
                        }
                    }
                }
                return AllOperationsSingleton;
            }
        }


        private void SendAnalyticsEvent(Item item, string eventMessage, string eventName, Guid eventTypeGuid)
        {
            //EnvironmentManager.Instance.Log(false, _thread.Name, "Sending analytics event");
            EventSource eventSource = null;
            eventSource = new EventSource()
            {
                FQID = item.FQID,
                Name = item.Name,
                Description = eventMessage
            };
            EventHeader eventHeader = new EventHeader()
            {
                ID = Guid.NewGuid(),
                Class = "SALTO Event",
                Type = "Door",
                Timestamp = DateTime.Now,
                Message = eventMessage,
                MessageId = eventTypeGuid,
                Name = eventName,
                Priority = (ushort)3,
                Source = eventSource,
            };

            EventData data = new EventData()
            {
                EventHeader = eventHeader
            };

            AnalyticsEvent analyticsEvent = new AnalyticsEvent();
            analyticsEvent.EventHeader = eventHeader;
            analyticsEvent.ReferenceList = new ReferenceList();
            if (item.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_CAMERA1) && !String.IsNullOrWhiteSpace(item.Properties[RadiniumSASDefinition.RADINIUM_CAMERA1]))
            {
                try
                {
                    Item camItem = Configuration.Instance.GetItem(new Guid(item.Properties[RadiniumSASDefinition.RADINIUM_CAMERA1]), Kind.Camera);
                    analyticsEvent.ReferenceList.Add(new Reference() { FQID = camItem.FQID });
                }
                catch (Exception)
                { }
            }
            if (item.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_CAMERA2) && !String.IsNullOrWhiteSpace(item.Properties[RadiniumSASDefinition.RADINIUM_CAMERA2]))
            {
                try
                {
                    Item camItem = Configuration.Instance.GetItem(new Guid(item.Properties[RadiniumSASDefinition.RADINIUM_CAMERA2]), Kind.Camera);
                    analyticsEvent.ReferenceList.Add(new Reference() { FQID = camItem.FQID });
                }
                catch (Exception)
                { }
            }
            VideoOS.Platform.Messaging.Message sentAnalytic = new VideoOS.Platform.Messaging.Message(VideoOS.Platform.Messaging.MessageId.Server.NewEventCommand) { Data = data };

            EnvironmentManager.Instance.SendMessage(sentAnalytic);
        }
        private void SendAlarmEvent(SaltoAuditEntry entry, Item door)
        {
            EnvironmentManager.Instance.Log(false, _thread.Name, "Sending access alarm event");
            Alarm alarm = new Alarm();
            alarm.Count = 0;
            alarm.Description = entry.Operation.ToDescriptionString() + "\r\nPlease investigate source of alarm";
            alarm.SnapshotList = new SnapshotList();
            alarm.RuleList = new RuleList();
            alarm.ObjectList = new AnalyticsObjectList();
            alarm.Location = entry.DoorName;
            alarm.ReferenceList = new ReferenceList();
            if (door != null && door.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_CAMERA1))
            {
                string cameraStr = door.Properties[RadiniumSASDefinition.RADINIUM_CAMERA1];
                Item cameraItem = Configuration.Instance.GetItem(Guid.Parse(cameraStr), Kind.Camera);
                if(cameraItem != null)
                {
                    alarm.ReferenceList.Add(new Reference() { FQID = cameraItem.FQID });
                }
            }
            if (door != null && door.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_CAMERA2))
            {
                string cameraStr = door.Properties[RadiniumSASDefinition.RADINIUM_CAMERA2];
                Item cameraItem = Configuration.Instance.GetItem(Guid.Parse(cameraStr), Kind.Camera);
                if (cameraItem != null)
                {
                    alarm.ReferenceList.Add(new Reference() { FQID = cameraItem.FQID });
                }
            }
            alarm.EventHeader = new EventHeader();
            alarm.EventHeader.CustomTag = entry.DoorId; // Unique zone identifier (unique between SecurityAlarmPanel)
            alarm.EventHeader.ID = Guid.NewGuid();
            alarm.EventHeader.MessageId = Guid.Parse(RadiniumSASDefinition.RadiniumAccessAlarmStr);
            alarm.EventHeader.Message = "Intrusion alarm for door " + door.Name;
            alarm.EventHeader.Type = "Access alarm";
            alarm.EventHeader.Timestamp = DateTime.Now;
            alarm.EventHeader.Source = new EventSource() { FQID = door.FQID, Description = entry.Operation.ToDescriptionString(), Name = entry.DoorName };
            alarm.EventHeader.Priority = (ushort)((int)SaltoOperation.LowBatteryLevel == entry.Operation ? 5 : 1);
            alarm.EventHeader.PriorityName = "High";
            alarm.EventHeader.Name = "Access Alarm";
            alarm.EventHeader.ExpireTimestamp = DateTime.MinValue;
            alarm.EventHeader.Class = "AccessAlarm";
            alarm.EventHeader.Version = "1.1";
            alarm.Category = 1;
            alarm.AssignedTo = null;
            alarm.State = 1;
            alarm.StateName = "New";
            alarm.Vendor = new Vendor() { Name = "Radinium (Pty) Ltd" };

            alarm.ReferenceList.Add(new Reference() { FQID = door.FQID });

            if (!String.IsNullOrWhiteSpace(entry.Camera1Id))
            {
                Guid cameraGuid = Guid.Parse(entry.Camera1Id);
                Item cameraItem = Configuration.Instance.GetItem(cameraGuid, Kind.Camera);
                if (cameraItem != null)
                {
                    FQID camFQID = cameraItem.FQID;
                    alarm.ReferenceList.Add(new Reference() { FQID = camFQID });
                }
            }
            if (!String.IsNullOrWhiteSpace(entry.Camera2Id))
            {
                Guid cameraGuid = Guid.Parse(entry.Camera2Id);
                Item cameraItem = Configuration.Instance.GetItem(cameraGuid, Kind.Camera);
                if(cameraItem != null)
                {
                    FQID camFQID = cameraItem.FQID;
                    alarm.ReferenceList.Add(new Reference() { FQID = camFQID });
                }
            }

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(VideoOS.Platform.Messaging.MessageId.Server.NewAlarmCommand, alarm));

            EnvironmentManager.Instance.Log(false, _thread.Name, "Access alarm event sent");
        }

        private object saltoAuditLock = new object();

        private SaltoAuditEntry ProcessSaltoEntry(SaltoAuditEntry entry)
        {
            long maxInternalId = 0;
            if(entryDictionary.Any())
            {
                var maxEntry = entryDictionary.OrderByDescending(x => x.Value.InternalEventId).Skip(MAX_SALTO_EVENTS_COUNT).Take(1).DefaultIfEmpty(new KeyValuePair<long, SaltoAuditEntry>(0, null)).FirstOrDefault();
                maxInternalId = maxEntry.Key;
            }
            List<long> entryIds = entryDictionary.Where(x => x.Value.InternalEventId < maxInternalId).Select(x => x.Key).ToList();
            entryIds.ForEach(x => entryDictionary.Remove(x));

            if (entryDictionary.ContainsKey(entry.InternalEventId) || entry.InternalEventId < maxInternalId)
            {
                return null;
            }

            entryDictionary[entry.InternalEventId] = entry;

            lock(saltoAuditLock)
            {
                List<Item> doorsList = null;
                dynamic currentEntity = null;
                DateTime expiryDate = DateTime.Now.AddMilliseconds(CONFIGURATION_UPDATE_SLEEP_MS * 1.2);
                while (doorsList == null && DateTime.Now < expiryDate)
                {
                    lock (doorsLock)
                    {
                        doorsList = doors;
                    }
                    Item itemDoor = doorsList.FirstOrDefault(y => y.Properties["ExtDoorID"] == entry.DoorId);
                    if (itemDoor != null)
                    {
                        if(!itemDoor.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ITEM_TYPE))
                        {
                            itemDoor.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] = RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR;
                        }
                        switch (itemDoor.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE])
                        {
                            case RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR:
                                currentEntity = SaltoDbDoor.ConvertItemToDoor(itemDoor);
                                break;
                            case RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER:
                                currentEntity = SaltoDbLocker.ConvertItemToLocker(itemDoor);
                                break;
                            case RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM:
                                currentEntity = SaltoDbRoom.ConvertItemToRoom(itemDoor);
                                break;
                        }

                        SaltoOperation op = SaltoOperation.UnknownOperation;
                        if (AllOperationsList.Any(x => x.Any(y => (int)y == entry.Operation)))
                        {
                            op = (SaltoOperation)entry.Operation;
                        }
                        if (AlarmEventTypes.Contains(op))
                        {
                            SendAlarmEvent(entry, itemDoor);
                        }
                        foreach (List<SaltoOperation> operationList in AllOperationsList)
                        {
                            if (operationList.Contains(op))
                            {
                                SendAnalyticsEvent(itemDoor, entry.Operation.ToDescriptionString(), operationList.ToDescriptionString(), operationList.getEventGuid());
                            }
                        }
                    }
                }
                if (currentEntity != null)
                {
                    entry.Camera1Id = currentEntity.Camera1;
                    entry.Camera2Id = currentEntity.Camera2;
                }
            }
            return entry;
        }

        const string AuditTrailRequestXml = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>SaltoDBAuditTrail.Read</RequestName>
    <Params>
        <MaxCount>40</MaxCount>
        <ShowDoorIDAs>1</ShowDoorIDAs>
        <ShowUserIDAs>1</ShowUserIDAs>
        <DescendingOrder>0</DescendingOrder>
        <ShowTimeInUTC>1</ShowTimeInUTC>
        {0}
    </Params>
</RequestCall>";

        /// <summary>
        /// the thread doing the work
        /// </summary>
        private void Run()
        {
            EnvironmentManager.Instance.Log(false, "RadiniumSALTO background thread", "Now starting...", null);
            ThreadPool.QueueUserWorkItem(x => 
            {
                while(!_stop)
                {
                    if (!RadiniumSaltoItemUpdater.SaltoLicenceValid)
                    {
                        Thread.Sleep(10000);
                        continue;
                    }
                    Configuration.Instance.RefreshConfiguration(RadiniumSASDefinition.RadiniumSASKind);
                    List<Item> itemsConfiguration = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind);
                    lock (doorsLock)
                    {
                        doors = itemsConfiguration;
                    }
                    Thread.Sleep(CONFIGURATION_UPDATE_SLEEP_MS);
                }
            });

            long minEventId = 0;

            try
            {
                using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                {
                    using (var context = new SaltoDbContext(conn, contextOwnsConnection: true))
                    {
                        minEventId = context.AuditEntries.Max(x => x.InternalEventId) + 1;
                    }
                }
            }
            catch(Exception)
            {

            }
            DateTime firstLoopTime = DateTime.Now;
            DateTime endLoopTime = DateTime.Now;
            DateTime currentLoopTime = DateTime.Now;
            DateTime endCurrentLoopTime = DateTime.Now;
            while (!_stop)
            {
                if (!RadiniumSaltoItemUpdater.SaltoLicenceValid)
                {
                    Thread.Sleep(10000);
                    continue;
                }
                byte[] data = null;
                try
                {
                    string requestStr;
                    if(minEventId != 0)
                    {
                        requestStr = String.Format(AuditTrailRequestXml, "<StartingFromEventID>" + minEventId + "</StartingFromEventID>");
                    }
                    else
                    {
                        requestStr = String.Format(AuditTrailRequestXml, "");
                    }
                    data = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(requestStr));
                }
                catch(Exception)
                {

                }
                if (data != null)
                {
                    try
                    {
                        string dataStr = UTF8Encoding.UTF8.GetString(data);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(dataStr);
                        XmlNodeList children = doc.SelectSingleNode("//SaltoDBAuditTrail").ChildNodes;
                        List<SaltoAuditEntry> responseEntries = new List<SaltoAuditEntry>();
                        for (int i = 0; i < children.Count; i++)
                        {
                            XmlNode currentChild = children.Item(i);
                            if (currentChild.NodeType == XmlNodeType.Element)
                            {
                                SaltoAuditEntry entry = SaltoAuditEntry.ParseFromXmlNode(currentChild);
                                if (entry == null)
                                {
                                    continue;
                                }

                                SaltoAuditEntry returnEntry = ProcessSaltoEntry(entry);
                                if (returnEntry != null)
                                {
                                    responseEntries.Add(returnEntry);
                                    minEventId = Math.Max(minEventId, returnEntry.InternalEventId + 1);
                                }
                            }
                        }
                        if (responseEntries.Count > 0)
                        {
                            ThreadPool.QueueUserWorkItem((a) =>
                            {
                                currentLoopTime = DateTime.Now;
                                try
                                {
                                    using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                                    {
                                        using (var context = new SaltoDbContext(conn, contextOwnsConnection: true))
                                        {
                                            List<SaltoAuditEntry> entries = a as List<SaltoAuditEntry>;
                                            List<string> doorIds = entries.Select(x => x.DoorId).Distinct().ToList();
                                            List<string> userIds = entries.Where(y => y.SubjectType == SaltoSubjectType.CardHolder).Select(x => x.SubjectId).Distinct().ToList();
                                            IEnumerable<SaltoDbDoor> validDoorsInDb = context.Doors.Where(x => doorIds.Any(y => y == x.ExtDoorID));
                                            IEnumerable<SaltoDbLocker> validLockersInDb = context.Lockers.Where(x => doorIds.Any(y => y == x.ExtDoorID));
                                            IEnumerable<SaltoDbRoom> validRoomsInDb = context.HotelRooms.Where(x => doorIds.Any(y => y == x.ExtDoorID));
                                            IEnumerable<SaltoDbUser> validUsersInDb = context.Users.Where(x => userIds.Any(y => y == x.ExtUserID));
                                            foreach (SaltoAuditEntry entry in entries)
                                            {
                                                if (!String.IsNullOrWhiteSpace(entry.SubjectId) && entry.SubjectType == SaltoSubjectType.CardHolder)
                                                {
                                                    entry.User = validUsersInDb.Where(x => x.ExtUserID == entry.SubjectId).FirstOrDefault();
                                                }
                                                if (!String.IsNullOrWhiteSpace(entry.DoorId))
                                                {
                                                    entry.Door = validDoorsInDb.Where(x => x.ExtDoorID == entry.DoorId).FirstOrDefault();
                                                    entry.Locker = validLockersInDb.Where(x => x.ExtDoorID == entry.DoorId).FirstOrDefault();
                                                    entry.Room = validRoomsInDb.Where(x => x.ExtDoorID == entry.DoorId).FirstOrDefault();
                                                }

                                                if (entry.DoorId != null)
                                                {
                                                    if (entry.Door != null)
                                                    {
                                                        entry.DoorName = entry.Door.Name;
                                                    }
                                                    if (entry.Locker != null)
                                                    {
                                                        entry.DoorName = entry.Locker.Name;
                                                    }
                                                    if (entry.Room != null)
                                                    {
                                                        entry.DoorName = entry.Room.Name;
                                                    }
                                                }
                                                if (entry.SubjectId != null && entry.SubjectType == SaltoSubjectType.CardHolder)
                                                {
                                                    SaltoDbUser user = validUsersInDb.Where(x => x.ExtUserID == entry.SubjectId).FirstOrDefault();
                                                    if (user != null)
                                                    {
                                                        entry.SubjectName = String.Format("{0} {1} {2}", user.Title, user.FirstName, user.LastName);
                                                    }
                                                }
                                                else
                                                {
                                                    entry.SubjectName = "";
                                                }
                                                if (entry.SubjectType == SaltoSubjectType.Door)
                                                {
                                                    entry.SubjectName = "(Door)";
                                                }
                                                if (entry.SubjectType == SaltoSubjectType.SoftwareOperator)
                                                {
                                                    entry.SubjectName = "(Software operator)";
                                                }
                                                context.AuditEntries.Add(entry);
                                            }
                                            context.SaveChanges();

                                            SaltoAuditEntry[] sendEntries = entries.Where(x =>
                                                allSaltoOperations.Keys.Contains(x.Operation) &&
                                                x.Operation != 1000 &&
                                                x.Operation != 1001
                                                ).ToArray();
                                            if(sendEntries.Count() > 0)
                                            {
                                                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_NEW_AUDIT_EVENTS, sendEntries));
                                            }
                                        }
                                    }
                                }
                                catch (Exception)
                                { }
                                endCurrentLoopTime = DateTime.Now;
                            }, responseEntries);
                        }
                        else
                        {
                            endLoopTime = DateTime.Now;
                            endLoopTime = DateTime.Now;
                            Thread.Sleep(AUDIT_TRAIL_UPDATE_SLEEP_MS);
                        }

                    }
                    catch (Exception e)
                    {
                        EnvironmentManager.Instance.Log(false, "RadiniumSALTO background thread", "Cannot parse returned XML");
                        Thread.Sleep(AUDIT_TRAIL_UPDATE_SLEEP_MS);
                    }
                }
                else
                {
                    Thread.Sleep(AUDIT_TRAIL_UPDATE_SLEEP_MS);
                }
            }
            EnvironmentManager.Instance.Log(false, "RadiniumSALTO background thread", "Now stopping...", null);
            _thread = null;
        }
    }
}
