﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RadiniumSAS.Background
{
    public enum DoorType
    {
        [Description("Unknown")]
        UNKNOWN = -1,
        [Description("Control Unit")]
        CONTROL_UNIT = 0,
        [Description("RF Escutcheon")]
        RF_ESCUTCHEON = 1
    }

    public enum CommStatus
    {
        [Description("Unknown")]
        UNKNOWN = -1,
        [Description("Communication lost")]
        NO_COMMS = 0,
        [Description("Communication OK")]
        COMMS_OK = 1
    }

    public enum DoorStatus
    {
        [Description("N/A")]
        UNKNOWN = -1,
        [Description("Open")]
        OPENED = 0,
        [Description("Closed")]
        CLOSED = 1,
        [Description("Warning: Left open")]
        LEFT_OPENED = 2,
        [Description("Alarm: Intrusion")]
        INTRUSION = 3,
        [Description("Emergency open")]
        EMERGENCY_OPEN = 4,
        [Description("Emergency Close")]
        EMERGENCY_CLOSE = 5,
        [Description("Initializing")]
        INITIALIZING = 6
    }

    public enum BatteryStatus
    {
        [Description("Battery Unknown")]
        UNKNOWN = -1,
        [Description("Battery Nominal")]
        NORMAL = 0,
        [Description("Battery Low")]
        LOW = 1,
        [Description("Battery Very low")]
        VERY_LOW = 2,
    }

    public enum TamperStatus
    {
        [Description("Unknown")]
        UNKNOWN = -1,
        [Description("Nominal")]
        NORMAL = 0,
        [Description("Alert: Tamper")]
        ALARMED = 1
    }


    [Serializable]
    [XmlRoot("OnlineDoorStatus")]
    public class SaltoOnlineDoorStatus
    {
        [XmlElement("DoorID")]
        public string DoorID;
        [XmlElement("DoorType")]
        public int DoorTypeInt;
        [XmlElement("CommStatus")]
        public int CommStatusInt;
        [XmlElement("DoorStatus")]
        public int DoorStatusInt;
        [XmlElement("BatteryStatus")]
        public int BatteryStatusInt;
        [XmlElement("TamperStatus")]
        public int TamperStatusInt;

        public bool Online;

        public int BatteryLevel;

        public DoorType DoorType
        {
            get
            {
                return (DoorType)DoorTypeInt;
            }
        }

        public CommStatus CommStatus
        {
            get
            {
                return (CommStatus)CommStatusInt;
            }
        }

        public DoorStatus DoorStatus
        {
            get
            {
                return (DoorStatus)DoorStatusInt;
            }
        }

        public BatteryStatus BatteryStatus
        { 
            get
            {
                return (BatteryStatus)BatteryStatusInt;
            }
        }

        public TamperStatus TamperStatus
        {
            get
            {
                return (TamperStatus)TamperStatusInt;
            }
        }
    }
}
