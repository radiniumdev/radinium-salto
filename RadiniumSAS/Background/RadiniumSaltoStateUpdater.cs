using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Xml;
using System.Linq;
using VideoOS.Platform;
using VideoOS.Platform.Background;
using VideoOS.Platform.Client;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Reflection;
using VideoOS.Platform.Messaging;
using RadiniumSAS.Data;
using RadiniumSAS.Admin;
using System.Data.SqlClient;

namespace RadiniumSAS.Background
{

    /// <summary>
    /// A background plugin will be started during application start and be running until the user logs off or application terminates.<br/>
    /// The Environment will call the methods Init() and Close() when the user login and logout, 
    /// so the background task can flush any cached information.<br/>
    /// The base class implementation of the LoadProperties can get a set of configuration, 
    /// e.g. the configuration saved by the Options Dialog in the Smart Client or a configuration set saved in one of the administrators.  
    /// Identification of which configuration to get is done via the GUID.<br/>
    /// The SaveProperties method can be used if updating of configuration is relevant.
    /// <br/>
    /// The configuration is stored on the server the application is logged into, and should be refreshed when the ApplicationLoggedOn method is called.
    /// Configuration can be user private or shared with all users.<br/>
    /// <br/>
    /// This plugin could be listening to the Message with MessageId == Server.ConfigurationChangedIndication to when when to reload its configuration.  
    /// This event is send by the environment within 60 second after the administrator has changed the configuration.
    /// </summary>
    public class RadiniumSaltoStateUpdater : BackgroundPlugin
    {
        const int UPDATE_PERIOD_SECONDS = 2;

        private bool _stop = false;
        private Thread _thread;

        /// <summary>
        /// Gets the unique id identifying this plugin component
        /// </summary>
        public override Guid Id
        {
            get { return RadiniumSASDefinition.RadinumSaltoStateUpdaterPlugin; }
        }

        /// <summary>
        /// The name of this background plugin
        /// </summary>
        public override String Name
        {
            get { return "RadiniumSAS ItemUpdater BackgroundPlugin"; }
        }

        /// <summary>
        /// Called by the Environment when the user has logged in.
        /// </summary>
        public override void Init()
        {
            _stop = false;
            _thread = new Thread(new ThreadStart(Run));
            _thread.Name = "RadiniumSAS ItemUpdater Background Thread";
            _thread.Start();
        }

        /// <summary>
        /// Called by the Environment when the user log's out.
        /// You should close all remote sessions and flush cache information, as the
        /// user might logon to another server next time.
        /// </summary>
        public override void Close()
        {
            _stop = true;
        }

        /// <summary>
        /// Define in what Environments the current background task should be started.
        /// </summary>
        public override List<EnvironmentType> TargetEnvironments
        {
            get { return new List<EnvironmentType>() { EnvironmentType.Service, EnvironmentType.Standalone }; } // Default will run in the Event Server
        }

        XmlSerializer doorSerializer = new XmlSerializer(typeof(SaltoOnlineDoorStatus));

        const string DoorOnlineStatusRequest = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>OnlineDoor.GetOnlineStatusList</RequestName>
    <Params>
        <ExtDoorIDList />
    </Params>
</RequestCall>
";
        private List<SaltoDbRoom> currentRoomInfos = new List<SaltoDbRoom>();
        private List<SaltoDbLocker> currentLockerInfos = new List<SaltoDbLocker>();
        private List<SaltoDbDoor> currentDoorInfos = new List<SaltoDbDoor>();
        private object currentDoorLock = new object();

        private void UpdateCurrentItems(object obj)
        {
            while(!_stop)
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                    {
                        using (SaltoDbContext context = new SaltoDbContext(conn, true))
                        {
                            List<SaltoDbDoor> currentDoors = context.Doors.ToList();
                            List<SaltoDbLocker> currentLockers = context.Lockers.ToList();
                            List<SaltoDbRoom> currentRooms = context.HotelRooms.ToList();
                            lock(currentDoorLock)
                            {
                                currentDoorInfos = currentDoors;
                                currentLockerInfos = currentLockers;
                                currentRoomInfos = currentRooms;
                            }
                        }
                    }
                }
                catch (Exception)
                { }
                Thread.Sleep(UPDATE_PERIOD_SECONDS * 30000);
            }
        }


        /// <summary>
        /// the thread doing the work
        /// </summary>
        private void Run()
        {
            ThreadPool.QueueUserWorkItem(UpdateCurrentItems);
            while (!_stop)
            {
                string returnXML = null;
                Thread.Sleep(UPDATE_PERIOD_SECONDS * 1000);
                if (!RadiniumSaltoItemUpdater.SaltoLicenceValid)
                {
                    Thread.Sleep(10000);
                    continue;
                }
                try
                {
                    byte[] doorData = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(string.Format(DoorOnlineStatusRequest, "")));
                    if(doorData == null)
                    {
                        continue;
                    }
                    string dataStr = UTF8Encoding.UTF8.GetString(doorData);
                    returnXML = dataStr;
                    XDocument doorDoc = XDocument.Parse(dataStr);
                    doorDoc.Descendants().Where(e => string.IsNullOrEmpty(e.Value)).Remove();
                    IEnumerable<XNode> doorChildren = doorDoc.Descendants("OnlineDoorStatus");
                    List<SaltoOnlineDoorStatus> onlineDoorStatuses = new List<SaltoOnlineDoorStatus>();
                    foreach (XNode node in doorChildren)
                    {
                        string nodeString = node.ToString();
                        SaltoOnlineDoorStatus door = doorSerializer.Deserialize(new StringReader(nodeString)) as SaltoOnlineDoorStatus;
                        if (door == null)
                        {
                            continue;
                        }
                        door.Online = true;
                        lock(currentDoorLock)
                        {
                            SaltoDbDoor saltoDoor = currentDoorInfos.FirstOrDefault(x => x.ExtDoorID == door.DoorID);
                            if(saltoDoor != null)
                            {
                                door.BatteryLevel = saltoDoor.BatteryStatus;
                            }
                            SaltoDbLocker saltoLocker = currentLockerInfos.FirstOrDefault(x => x.ExtDoorID == door.DoorID);
                            if (saltoLocker != null)
                            {
                                door.BatteryLevel = saltoLocker.BatteryStatus;
                            }
                            SaltoDbRoom saltoRoom = currentRoomInfos.FirstOrDefault(x => x.ExtDoorID == door.DoorID);
                            if (saltoRoom != null)
                            {
                                door.BatteryLevel = saltoRoom.BatteryStatus;
                            }
                        }
                        onlineDoorStatuses.Add(door);
                    }
                    lock (currentDoorLock)
                    {
                        foreach(SaltoDbDoor door in currentDoorInfos)
                        {
                            if(!onlineDoorStatuses.Any(x => x.DoorID == door.ExtDoorID))
                            {
                                BatteryStatus status = BatteryStatus.UNKNOWN;
                                if(door.BatteryStatus >= 0 && door.BatteryStatus <= 15)
                                {
                                    status = BatteryStatus.VERY_LOW;
                                }
                                if(door.BatteryStatus > 15 && door.BatteryStatus <= 25)
                                {
                                    status = BatteryStatus.LOW;
                                }
                                if(door.BatteryStatus > 25 && door.BatteryStatus <= 100)
                                {
                                    status = BatteryStatus.NORMAL;
                                }
                                onlineDoorStatuses.Add(new SaltoOnlineDoorStatus()
                                {
                                    BatteryLevel = door.BatteryStatus,
                                    BatteryStatusInt = (int)status,
                                    CommStatusInt = (int)CommStatus.UNKNOWN,
                                    DoorStatusInt = (int)DoorStatus.UNKNOWN,
                                    DoorTypeInt = (int)DoorType.UNKNOWN,
                                    TamperStatusInt = (int)TamperStatus.UNKNOWN,
                                    Online = false,
                                    DoorID = door.ExtDoorID
                                });
                            }
                        }

                        foreach (SaltoDbLocker locker in currentLockerInfos)
                        {
                            if (!onlineDoorStatuses.Any(x => x.DoorID == locker.ExtDoorID))
                            {
                                BatteryStatus status = BatteryStatus.UNKNOWN;
                                if (locker.BatteryStatus >= 0 && locker.BatteryStatus <= 15)
                                {
                                    status = BatteryStatus.VERY_LOW;
                                }
                                if (locker.BatteryStatus > 15 && locker.BatteryStatus <= 25)
                                {
                                    status = BatteryStatus.LOW;
                                }
                                if (locker.BatteryStatus > 25 && locker.BatteryStatus <= 100)
                                {
                                    status = BatteryStatus.NORMAL;
                                }
                                onlineDoorStatuses.Add(new SaltoOnlineDoorStatus()
                                {
                                    BatteryLevel = locker.BatteryStatus,
                                    BatteryStatusInt = (int)status,
                                    CommStatusInt = (int)CommStatus.UNKNOWN,
                                    DoorStatusInt = (int)DoorStatus.UNKNOWN,
                                    DoorTypeInt = (int)DoorType.UNKNOWN,
                                    TamperStatusInt = (int)TamperStatus.UNKNOWN,
                                    Online = false,
                                    DoorID = locker.ExtDoorID
                                });
                            }
                        }

                        foreach (SaltoDbRoom room in currentRoomInfos)
                        {
                            if (!onlineDoorStatuses.Any(x => x.DoorID == room.ExtDoorID))
                            {
                                BatteryStatus status = BatteryStatus.UNKNOWN;
                                if (room.BatteryStatus >= 0 && room.BatteryStatus <= 15)
                                {
                                    status = BatteryStatus.VERY_LOW;
                                }
                                if (room.BatteryStatus > 15 && room.BatteryStatus <= 25)
                                {
                                    status = BatteryStatus.LOW;
                                }
                                if (room.BatteryStatus > 25 && room.BatteryStatus <= 100)
                                {
                                    status = BatteryStatus.NORMAL;
                                }
                                onlineDoorStatuses.Add(new SaltoOnlineDoorStatus()
                                {
                                    BatteryLevel = room.BatteryStatus,
                                    BatteryStatusInt = (int)status,
                                    CommStatusInt = (int)CommStatus.UNKNOWN,
                                    DoorStatusInt = (int)DoorStatus.UNKNOWN,
                                    DoorTypeInt = (int)DoorType.UNKNOWN,
                                    TamperStatusInt = (int)TamperStatus.UNKNOWN,
                                    Online = false,
                                    DoorID = room.ExtDoorID
                                });
                            }
                        }
                    }
                    ThreadPool.QueueUserWorkItem((x) => UpdateDoorStatuses(x), onlineDoorStatuses);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_DOOR_STATUSES, onlineDoorStatuses.ToArray()));
                }
                catch (Exception e)
                {
                    //EnvironmentManager.Instance.Log(false, "RadiniumSALTO State Updater background thread", "Cannot parse returned XML for door status: " + e.Message);
                    //EnvironmentManager.Instance.Log(false, "RadiniumSALTO State Updater background thread", "Cannot parse returned XML for door status: " + e.StackTrace.Replace("\n", "  ").Replace("\r", " "));
                }
            }
            EnvironmentManager.Instance.Log(false, "RadiniumSALTO State Updater background thread", "Now stopping...", null);
            _thread = null;
        }

        private DateTime lastUpdated = DateTime.UtcNow;

        private void UpdateDoorStatuses(object onlineDoorStatuses)
        {
            if(lastUpdated > DateTime.UtcNow)
            {
                return;
            }

            lastUpdated = DateTime.UtcNow.AddSeconds(300);

            List<SaltoOnlineDoorStatus> onlineStatuses = onlineDoorStatuses as List<SaltoOnlineDoorStatus>;
            if(onlineStatuses == null)
            {
                return;
            }
            List<Item> sasItems = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind);
            foreach(SaltoOnlineDoorStatus status in onlineStatuses)
            {
                bool itemUpdated = false;
                Item sasItem = sasItems.Where(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && status.DoorID == x.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]).FirstOrDefault();
                if(!sasItem.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ONLINE_DOOR_TYPE) 
                        ||
                    sasItem.Properties[RadiniumSASDefinition.RADINIUM_ONLINE_DOOR_TYPE] != status.DoorType.ToString()
                    )
                {
                    sasItem.Properties[RadiniumSASDefinition.RADINIUM_ONLINE_DOOR_TYPE] = status.DoorType.ToString();
                    itemUpdated = true;
                }
                if(itemUpdated)
                {
                    Configuration.Instance.SaveItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, sasItem);
                }
            }
        }
    }
}
