﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using VideoOS.Platform;

namespace RadiniumSAS.Background
{

    public enum SaltoSubjectType
    {
        CardHolder = 0,
        Door = 1,
        SoftwareOperator = 2
    }


    [AttributeUsage(AttributeTargets.All)]
    public class SaveInItemAttribute : Attribute
    { 
    }


    //
    // Summary:
    //     Specifies a description for a property or event.
    [AttributeUsage(AttributeTargets.All)]
    public class EventGuidAttribute : Attribute
    {
        public static readonly EventGuidAttribute Default = new EventGuidAttribute(Guid.Empty.ToString());

        public EventGuidAttribute() 
        {
            EventGuidValue = Guid.Empty.ToString();
        }
        public EventGuidAttribute(string eventGuid)
        {
            EventGuidValue = Guid.Empty.ToString();
            if (eventGuid != null)
            {
                Guid eventValue = Guid.Empty;
                if(Guid.TryParse(eventGuid, out eventValue))
                {
                    EventGuidValue = eventValue.ToString();
                }
            }
        }

        public Guid EventGuid 
        { 
            get
            {
                return Guid.Parse(EventGuidValue);
            }
        }

        protected string EventGuidValue { get; set; }
        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }
            if(obj.GetType() == typeof(string))
            {
                Guid dummy = Guid.Empty;
                if(Guid.TryParse((string)obj, out dummy))
                {
                    return EventGuidValue == (string)obj;
                }
            }
            if(obj.GetType() == typeof(EventGuidAttribute))
            {
                return ((EventGuidAttribute)obj).EventGuid.ToString() == EventGuidValue;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return EventGuidValue.GetHashCode();
        }
        public override bool IsDefaultAttribute()
        {
            return EventGuidValue == Guid.Empty.ToString();
        }
    }

    public enum SaltoOperation
    {
        [Description("Unknown operation")]
        UnknownOperation = -10000,
        [Description("Opening or closing")]
        OpeningOrClosing = 1,
        [Description("Door opened: Inside handle")]
        DoorOpenedInsideHandle = 16,
        [Description("Door opened: Key")]
        DoorOpenedKey = 17,
        [Description("Door opened: Key and keyboard")]
        DoorOpenedKeyAndKeyboard = 18,
        [Description("Door opened: Switch")]
        DoorOpenedSwitch = 21,
        [Description("Door opened: Mechanical relay")]
        DoorOpenedMechanicalRelay = 22,
        [Description("First double card read")]
        FirstDoubleCardRead = 23,
        [Description("Door opened after second card presented")]
        DoorOpenedAfterSecondCard = 24,
        [Description("Door opened with PPD")]
        DoorOpenedWithPPD = 25,
        [Description("Door opened: Keyboard")]
        DoorOpenedKeyboard = 26,
        [Description("Door opened: Spare card (hotel)")]
        DoorOpenedSpareCardHotel = 27,
        [Description("Door opened: Online command")]
        DoorOpenedOnlineCommand = 28,
        [Description("Door probably opened: Key and pin")]
        DoorProbablyOpenKeyAndPin = 29,
        [Description("Door closed: Key")]
        DoorClosedKey = 33,
        [Description("Door closed: Key and keyboard")]
        DoorClosedKeyAndKeyboard = 34,
        [Description("Door closed: Keyboard")]
        DoorClosedKeyboard = 35,
        [Description("Door closed: Switch")]
        DoorClosedSwitch = 36,
        [Description("Key inserted (energy saving device)")]
        KeyInsertedEnergySavingDevice = 37,
        [Description("Key removed (energy saving device)")]
        KeyRemovedEnergySavingDevice = 38,
        [Description("Room prepared (energy saving device)")]
        RoomPreparedEnergySavingDevice = 39,
        [Description("Start of privacy")]
        StartOfPrivacy = 40,
        [Description("End of privacy")]
        EndOfPrivacy = 41,
        [Description("End of office mode (keypad)")]
        EndOfOfficeModeKeypad = 32,
        [Description("Communication with the reader lost")]
        CommunicationWithReaderLost = 47,
        [Description("Communication with the reader re-established")]
        CommunicationWithReaderReestablished = 48,
        [Description("Start of office mode")]
        StartOfOfficeMode = 49,
        [Description("End of office mode")]
        EndOfOfficeMode = 50,
        [Description("Hotel guest cancelled")]
        HotelGuestCancelled = 51,
        [Description("Door programmed with spare key")]
        DoorProgrammedWithSpareKey = 54,
        [Description("New hotel guest key")]
        NewHotelGuestKey = 55,
        [Description("Communication with salto software lost")]
        CommunicationWithSaltoSoftwareLost = 79,
        [Description("Communication with salto software re-established")]
        CommunicationWithSaltoSoftwareReestablished = 80,
        [Description("Start of office mode (online)")]
        StartOfOfficeModeOnline = 65,
        [Description("End of office mode (online)")]
        EndOfOfficeModeOnline = 66,
        [Description("Automatic change")]
        AutomaticChange = 68,
        [Description("Start of office mode (online from host)")]
        StartOfOfficeModeOnlineHost = 56,
        [Description("End of office mode (online from host)")]
        EndOfOfficeModeOnlineHost = 57,
        [Description("Start of forced closing (online from host)")]
        StartOfForcedClosingOnlineHost = 58,
        [Description("End of forced closing (online from host)")]
        EndOfForcedClosingOnlineHost = 59,
        [Description("Online peripheral updated")]
        OnlinePeripheralUpdated = 72,
        [Description("RF lock updated")]
        RFLockUpdated = 118,
        [Description("New renovation code for key (online)")]
        NewRenovationCodeForKeyOnline = 8,
        [Description("Key updated in out-of-site mode (online)")]
        KeyUpdatedOutOfSiteOnline = 69,
        [Description("Key updated (online)")]
        KeyUpdatedOnline = 76,
        [Description("Warning: key has not been completely updated (online)")]
        WarningKeyHasNotBeenCompletelyUpdatedOnline = 99,
        [Description("Key deleted (online)")]
        KeyDeletedOnline = 78,
        [Description("Expiration automatically extended (offline)")]
        ExpirationAutomaticallyExtendedOffline = 70,
        [Description("Blacklist key deleted")]
        BlacklistKeyDeleted = 104,
        [Description("Opening not allowed: Key not activated")]
        OpeningNotAllowedKeyNotActivated = 81,
        [Description("Opening not allowed: Key expired")]
        OpeningNotAllowedKeyExpired = 82,
        [Description("Opening not allowed: Key out of date")]
        OpeningNotAllowedKeyOutOfDate = 83,
        [Description("Opening not allowed: Key not allowed in this door")]
        OpeningNotAllowedKeyNotAllowedInThisDoor = 84,
        [Description("Opening not allowed: Out of time")]
        OpeningNotAllowedOutOfTime = 85,
        [Description("Opening not allowed: Key does not override privacy")]
        OpeningNotAllowedKeyDoesNotOverridePrivacy = 87,
        [Description("Opening not allowed: Old hotel guest key")]
        OpeningNotAllowedOldHotelGuestKey = 88,
        [Description("Opening not allowed: Hotel guest key cancelled")]
        OpeningNotAllowedHotelGuestKeyCancelled = 89,
        [Description("Opening not allowed: Antipassback")]
        OpeningNotAllowedAntipassback = 90,
        [Description("Opening not allowed: Second double card not presented")]
        OpeningNotAllowedSecondDoubleCardNotPresented = 91,
        [Description("Opening not allowed: No associated authorization")]
        OpeningNotAllowedNoAssociatedAuthorization = 92,
        [Description("Opening not allowed: Invalid pin")]
        OpeningNotAllowedInvalidPin = 93,
        [Description("Opening not allowed: Door in emergency state")]
        OpeningNotAllowedDoorInEmergencyState = 95,
        [Description("Opening not allowed: Key cancelled")]
        OpeningNotAllowedKeyCancelled = 96,
        [Description("Opening not allowed: Unique opening key already used")]
        OpeningNotAllowedUniqueOpeningKeyAlreadyUsed = 97,
        [Description("Opening not allowed: Key with old renovation number")]
        OpeningNotAllowedKeyWithOldRenovationNumber = 98,
        [Description("Opening not allowed: Run out of battery")]
        OpeningNotAllowedRunOutOfBattery = 100,
        [Description("Opening not allowed: Unable to audit on the key")]
        OpeningNotAllowedUnableToAuditOnTheKey = 101,
        [Description("Opening not allowed: Locker occupancy timeout")]
        OpeningNotAllowedLockerOccupancyTimeout = 102,
        [Description("Opening not allowed: Denied by host")]
        OpeningNotAllowedDeniedByHost = 103,
        [Description("Opening not allowed: Key with data manipulated")]
        OpeningNotAllowedKeyWithDataManipulated = 107,
        [Description("Closing not allowed: Door in emergency state")]
        ClosingNotAllowedDoorInEmergencyState = 111,
        [Description("New renovation code")]
        NewRenovationCode = 112,
        [Description("PPD connection")]
        PPDConnection = 113,
        [Description("Time modified (daylight saving time)")]
        TimeModifiedDaylightSaving = 114,
        [Description("Low battery level")]
        LowBatteryLevel = 115,
        [Description("Incorrect clock value")]
        IncorrectClockValue = 116,
        [Description("RF Lock date and time updated")]
        RFLockDateAndTipeUpdated = 117,
        [Description("Unable to perform open/close operation due to a hardware failure")]
        UnableToPerformOpenCloseOperationDueToHardwareFailure = 119,
        [Description("Duress alarm")]
        DuressAlarm = 42,
        [Description("Alarm: Intrusion (online)")]
        AlarmIntrusionOnline = 60,
        [Description("Alarm: Tamper (online)")]
        AlarmTamperOnline = 61,
        [Description("Door left open")]
        DoorLeftOpen = 62,
        [Description("End of door left open")]
        EndOfDoorLeftOpen = 63,
        [Description("End of intrusion")]
        EndOfIntrusion = 64,
        [Description("End of tamper")]
        EndOfTamper = 67,
        [Description("Unknown (1000)")]
        Unknown1000 = 1000,
        [Description("Unknown (1001)")]
        Unknown1001 = 1001,
        [Description("Software Operator Open")]
        SoftwareOperatorCommandOpen = -1003000,
        [Description("Software Operator Close")]
        SoftwareOperatorCommandClose = -1003001,
        [Description("Software Operator Emergency Evacuation")]
        SoftwareOperatorEmergencyEvacuation = -1003002,
        [Description("Software Operator Emergency Lockdown")]
        SoftwareOperatorEmergencyLockdown = -1003003,
        [Description("Software Operator Emergency End")]
        SoftwareOperatorEmergencyEnd = -1003004,
    }

    public static class SaltoExtensions
    {
        public static Guid getEventGuid(this List<SaltoOperation> val)
        {
            try
            {
                return RadiniumSASBackgroundPlugin.OperationToGuidStringMap[val].Key;
            }
            catch(Exception)
            {
                return Guid.Empty;
            }
        }

        public static string ToDescriptionString(this object val)
        {
            try
            {
                if(val.GetType() == typeof(List<SaltoOperation>) && RadiniumSASBackgroundPlugin.OperationToGuidStringMap.ContainsKey(val as List<SaltoOperation>))
                {
                    return RadiniumSASBackgroundPlugin.OperationToGuidStringMap[val as List<SaltoOperation>].Value;
                }
                DescriptionAttribute[] attributes = (DescriptionAttribute[])val
                   .GetType()
                   .GetField(val.ToString())
                   .GetCustomAttributes(typeof(DescriptionAttribute), false);
                return attributes.Length > 0 ? attributes[0].Description : string.Empty;
            }
            catch(Exception e)
            {
                return string.Empty;
            }
        }
    }


    public static class SaltoUtility
    {
        public volatile static string ServerHost = null;
        public volatile static int ServerPort = 0;
        public volatile static string Protocol = "HTTPS";
        public volatile static string ShipKey = "";

        public static void UpdateConfig(XmlNode config)
        {
            XDocument doc;
            try
            {
                doc = XDocument.Parse(config.OuterXml);
            }
            catch
            {
                return;
            }
            XElement shipHost = doc.Descendants("SaltoShipHost").FirstOrDefault();
            XElement shipPort = doc.Descendants("SaltoShipPort").FirstOrDefault();
            XElement shipProtocol = doc.Descendants("SaltoProtocol").FirstOrDefault();
            XElement shipKey = doc.Descendants("SaltoKey").FirstOrDefault();
            if (shipHost == null || shipPort == null || shipProtocol == null || shipKey == null)
            {
                return;
            }
            string hostStr = shipHost.Value;
            string portStr = shipPort.Value;
            string protocolStr = shipProtocol.Value;
            string shipKeyStr = shipKey.Value;
            int port = int.Parse(portStr);
            if (ServerHost != hostStr)
            {
                ServerHost = hostStr;
            }
            if (ServerPort != port)
            {
                ServerPort = port;
            }
            if (Protocol != protocolStr)
            {
                Protocol = protocolStr;
            }
            if (shipKeyStr != ShipKey)
            {
                ShipKey = shipKeyStr;
            }
        }

        const string protocol = "STP";
        const string delimiter = "/";
        const string protocolVersion = "00";

        public static byte[] BuildSaltoStpPacket(byte[] data)
        {
            int dataLen = data.Length;
            string totalInit = protocol + delimiter + protocolVersion + delimiter + dataLen.ToString() + delimiter;
            byte[] init = UTF8Encoding.UTF8.GetBytes(totalInit);
            return init.Concat(data).ToArray();
        }

        static readonly HttpClient client;

        static SaltoUtility()
        {
            var handler = new HttpClientHandler()
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            };

            client = new HttpClient(handler);
        }


        public static byte[] SendHttpSaltoRequest(string host, int port, byte[] data, bool encrypted)
        {
            HttpContent content = new ByteArrayContent(data);
            content.Headers.Add("Content-Type", "application/vnd.saltosystems.ship.v1+xml");
            string protocol = "http";
            if(encrypted)
            {
                protocol = "https";
            }
            
            try
            {
                using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, protocol + "://" + host + ":" + port))
                {
                    requestMessage.Content = content;
                    requestMessage.Headers.Add("Salto-SHIP-Key", ShipKey);
                    Task<HttpResponseMessage> responseMessage = client.SendAsync(requestMessage);
                    responseMessage.Wait();
                    HttpResponseMessage message = responseMessage.Result;
                    if (message.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        return null;
                    }
                    HttpContent messageContent = message.Content;
                    Task<byte[]> readTask = messageContent.ReadAsByteArrayAsync();
                    readTask.Wait();
                    return readTask.Result;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static byte[] SendSaltoRequest(string host, int port, string protocol, string shipKey, byte[] data)
        {
            byte[] returnData;
            string oldShipKey = ShipKey;
            string oldProtocol = Protocol;
            ShipKey = shipKey;
            if (protocol == "HTTPS")
            {
                Protocol = "HTTPS";
                returnData = SendHttpSaltoRequest(host, port, data, true);
            }
            else if (protocol == "HTTP")
            {
                Protocol = "HTTP";
                returnData = SendHttpSaltoRequest(host, port, data, false);
            }
            else
            {
                Protocol = "STP";
                returnData = SendStpSaltoRequest(host, port, data);
            }
            Protocol = oldProtocol;
            ShipKey = oldShipKey;
            return returnData;
        }

        public static byte[] SendSaltoRequest(string host, int port, byte[] data)
        {
            if(Protocol == "HTTPS")
            {
                return SendHttpSaltoRequest(host, port, data, true);
            }
            else if (Protocol == "HTTP")
            {
                return SendHttpSaltoRequest(host, port, data, false);
            }
            else
            {
                return SendStpSaltoRequest(host, port, data);
            }
        }

        public static byte[] SendStpSaltoRequest(string host, int port, byte[] data)
        {
            if(host == null || port == 0)
            {
                return null;
            }
            byte[] packet = BuildSaltoStpPacket(data);
            try
            {
                TcpClient client = new TcpClient(host, port);
                client.ReceiveTimeout = 10000;
                client.SendTimeout = 10000;
                NetworkStream str = client.GetStream();
                str.Write(packet, 0, packet.Length);
                str.Flush();
                byte[] protocolByte = new byte[3];
                str.Read(protocolByte, 0, 3);
                if (UTF8Encoding.UTF8.GetString(protocolByte) != protocol)
                {
                    EnvironmentManager.Instance.Log(true, "RadiniumSALTO", "Did not receive correct protocol from Salto server!");
                    return null;
                }
                byte[] delimitersAndProtocol = new byte[4];
                str.Read(delimitersAndProtocol, 0, 4);
                if(UTF8Encoding.UTF8.GetString(delimitersAndProtocol) != (delimiter + protocolVersion + delimiter))
                {
                    EnvironmentManager.Instance.Log(true, "RadiniumSALTO", "Did not receive correct protocol version from Salto server!");
                    return null;
                }
                List<byte> numberBytes = new List<byte>();
                bool continueBool = false;
                do
                {
                    continueBool = false;
                    int readByte = str.ReadByte();
                    if (readByte >= '0' && readByte <= '9')
                    {
                        numberBytes.Add((byte)readByte);
                        continueBool = true;
                    }
                }
                while (continueBool);
                if(numberBytes.Count <= 0)
                {
                    EnvironmentManager.Instance.Log(true, "RadiniumSALTO", "Invalid length received for STP protocol!");
                    return null;
                }
                byte[] lenBytes = numberBytes.ToArray();
                string lengthStr = UTF8Encoding.UTF8.GetString(lenBytes);
                int length = int.Parse(lengthStr);
                if(length <= 0)
                {
                    EnvironmentManager.Instance.Log(true, "RadiniumSALTO", "Invalid length received for STP protocol!");
                    return null;
                }
                byte[] dataResult = new byte[length];
                int currentPosition = 0, readBytes = 1;
                while(currentPosition < length && readBytes > 0)
                {
                    readBytes = str.Read(dataResult, currentPosition, length - currentPosition);
                    currentPosition += readBytes;
                }
                if(currentPosition != length)
                {
                    EnvironmentManager.Instance.Log(true, "RadiniumSALTO", "Invalid amount of data received for the length returned!");
                    return null;
                }
                return dataResult;
            }
            catch(Exception e)
            {
                EnvironmentManager.Instance.Log(true, "RadiniumSALTO", "Exception in sending Salto request: " + e.Message);
            }
            return null;
        }

    }
}
