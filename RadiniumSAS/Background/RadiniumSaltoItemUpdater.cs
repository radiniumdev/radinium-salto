using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Xml;
using System.Linq;
using VideoOS.Platform;
using VideoOS.Platform.Background;
using VideoOS.Platform.Client;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Reflection;
using RadiniumSAS.Data;
using System.Data.SqlClient;
using RadiniumSAS.Admin;
using VideoOS.Platform.Messaging;
using PdfRpt.FluentInterface;
using PdfRpt.Core.Contracts;
using static RadiniumSAS.Client.RadiniumSASReportForm;
using Cygnetic.MIPPluginCommons.Licence;
using VideoOS.Platform.Util;

namespace RadiniumSAS.Background
{
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).AddHours(-dt.Hour).AddMinutes(-dt.Minute).AddSeconds(-dt.Second).AddMilliseconds(-dt.Millisecond).AddMilliseconds(-1);
        }

        public static DateTime EndOfDay(this DateTime dt)
        {
            DateTime date = dt.AddDays(1).AddHours(-dt.Hour).AddMinutes(-dt.Minute).AddSeconds(-dt.Second);
            return date.AddMilliseconds(-date.Millisecond).AddMilliseconds(-1);
        }

        public static DateTime StartOfDay(this DateTime dt)
        {
            DateTime date = dt.AddHours(-dt.Hour).AddMinutes(-dt.Minute).AddSeconds(-dt.Second);
            return date.AddMilliseconds(-date.Millisecond);
        }
    }

    [Serializable]
    public class ReportRequestObject
    {
        public bool CardholderFilterIsChecked;
        public bool DoorFilterIsChecked;
        public bool EventTypeFilterIsChecked;
        public DateTime FromDate;
        public DateTime ToDate;
        public List<SaltoDbUser> selectedUsers;
        public List<SaltoDbDoor> selectedDoors;
        public List<SaltoDbLocker> selectedLockers;
        public List<SaltoDbRoom> selectedRooms;
        public List<int> selectedEventTypes;
    }
    
    [Serializable]
    public class AuditRequest
    {
        public string CardholderName;
        public string DoorId;
        public SaltoOperation? EventType;
    }

    public enum CurrentEmergencyState
    {
        NO_EMERGENCY,
        LOCKDOWN,
        EVACUATION,
        CANCEL
    }
    /// <summary>
    /// A background plugin will be started during application start and be running until the user logs off or application terminates.<br/>
    /// The Environment will call the methods Init() and Close() when the user login and logout, 
    /// so the background task can flush any cached information.<br/>
    /// The base class implementation of the LoadProperties can get a set of configuration, 
    /// e.g. the configuration saved by the Options Dialog in the Smart Client or a configuration set saved in one of the administrators.  
    /// Identification of which configuration to get is done via the GUID.<br/>
    /// The SaveProperties method can be used if updating of configuration is relevant.
    /// <br/>
    /// The configuration is stored on the server the application is logged into, and should be refreshed when the ApplicationLoggedOn method is called.
    /// Configuration can be user private or shared with all users.<br/>
    /// <br/>
    /// This plugin could be listening to the Message with MessageId == Server.ConfigurationChangedIndication to when when to reload its configuration.  
    /// This event is send by the environment within 60 second after the administrator has changed the configuration.
    /// </summary>
    public class RadiniumSaltoItemUpdater : BackgroundPlugin
    {
        const int UPDATE_PERIOD_SECONDS = 60;

        private bool _stop = false;
        private Thread _thread;

        XmlSerializer doorSerializer = new XmlSerializer(typeof(SaltoDbDoor));
        XmlSerializer lockerSerializer = new XmlSerializer(typeof(SaltoDbLocker));
        XmlSerializer roomSerializer = new XmlSerializer(typeof(SaltoDbRoom));
        XmlSerializer userSerializer = new XmlSerializer(typeof(SaltoDbUser));
        /// <summary>
        /// Gets the unique id identifying this plugin component
        /// </summary>
        public override Guid Id
        {
            get { return RadiniumSASDefinition.RadinumSaltoItemUpdaterPlugin; }
        }

        /// <summary>
        /// The name of this background plugin
        /// </summary>
        public override String Name
        {
            get { return "RadiniumSAS ItemUpdater BackgroundPlugin"; }
        }

        const string LockdownOpenCommand = "OnlineDoor.EmergencyOpen";
        const string LockdownCloseCommand = "OnlineDoor.EmergencyClose";
        const string LockdownEndCommand = "OnlineDoor.EndOfEmergency";
        const string ExtDoorId = "<ExtDoorID>{0}</ExtDoorID>";
        const string LockdownCommand = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>{0}</RequestName>
    <Params>
        <ExtDoorIDList>{1}
        </ExtDoorIDList>
    </Params>
</RequestCall>";



        private static volatile CurrentEmergencyState currentEmergencyState = CurrentEmergencyState.NO_EMERGENCY;
        private static volatile string[] emergencyDoors = new string[0];


        private object ReceiveLockdownCommand(Message msg, FQID destination, FQID sender)
        {
            if (!RadiniumSaltoItemUpdater.SaltoLicenceValid)
            {
                return null;
            }

            if (msg.Data == null || msg.Data as string[] == null)
            {
                return null;
            }
            string userEmergencyPerson = (msg.Data as string[])[0];
            string[] doorIds = (msg.Data as string[]).Skip(1).ToArray();

            string extDoorIds = String.Join("", doorIds.Select(x => String.Format(ExtDoorId, x)));
            string lockdowncmd = "";
            if (msg.MessageId == RadiniumSASDefinition.RADINIUM_MESSAGE_EMERGENCY_OPEN)
            {
                currentEmergencyState = CurrentEmergencyState.EVACUATION;
                lockdowncmd = LockdownOpenCommand;
                emergencyDoors = emergencyDoors.Concat(doorIds).ToArray();
            }

            if (msg.MessageId == RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_CLOSE)
            {
                currentEmergencyState = CurrentEmergencyState.LOCKDOWN;
                lockdowncmd = LockdownCloseCommand;
                emergencyDoors = emergencyDoors.Concat(doorIds).ToArray();
            }

            if (msg.MessageId == RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_END)
            {
                currentEmergencyState = CurrentEmergencyState.NO_EMERGENCY;
                lockdowncmd = LockdownEndCommand;
                emergencyDoors = emergencyDoors.Where(x => !doorIds.Contains(x)).ToArray();
            }
            if(lockdowncmd == "")
            {
                return null;
            }

            string request = String.Format(LockdownCommand, lockdowncmd, extDoorIds);
            byte[] results = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(request));

            using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
            {
                using (var context = new SaltoDbContext(conn, true))
                {
                    SaltoAuditEntry entry = new SaltoAuditEntry();
                    string extDoorId = "";
                    entry.DoorId = extDoorId;
                    if(currentEmergencyState == CurrentEmergencyState.EVACUATION)
                    {
                        entry.DoorName = "Emergency evacuation";
                    }
                    if(currentEmergencyState == CurrentEmergencyState.LOCKDOWN)
                    {
                        entry.DoorName = "Emergency lockdown";
                    }
                    if(currentEmergencyState == CurrentEmergencyState.NO_EMERGENCY)
                    {
                        entry.DoorName = "End Emergency";
                    }
                    entry.EventDate = DateTime.UtcNow;
                    entry.InternalEventId = 0;
                    entry.IsExit = false;

                    if (msg.MessageId == RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_END)
                        entry.Operation = (int)SaltoOperation.SoftwareOperatorEmergencyEnd;
                    if (msg.MessageId == RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_CLOSE)
                        entry.Operation = (int)SaltoOperation.SoftwareOperatorEmergencyLockdown;
                    if (msg.MessageId == RadiniumSASDefinition.RADINIUM_MESSAGE_EMERGENCY_OPEN)
                        entry.Operation = (int)SaltoOperation.SoftwareOperatorEmergencyEvacuation;
                    entry.SubjectId = "";
                    entry.SubjectName = userEmergencyPerson;
                    entry.SubjectType = SaltoSubjectType.SoftwareOperator;
                    entry.User = null;
                    context.AuditEntries.Add(entry);
                    context.SaveChanges();
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_NEW_AUDIT_EVENTS, new SaltoAuditEntry[] { entry }));
                }
            }


            return null;
        }


        const string DoorOpenCommand = "OnlineDoor.Open";
        const string DoorCloseCommand = "OnlineDoor.Close";
        const string DoorOpenCloseRequest = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>{0}</RequestName>
    <Params>
        <ExtDoorIDList>
            <ExtDoorId>{1}</ExtDoorId>
        </ExtDoorIDList>
    </Params>
</RequestCall>
        ";
        private object ReceiveCommand(Message msg, FQID destination, FQID sender)
        {
            if (!RadiniumSaltoItemUpdater.SaltoLicenceValid)
            {
                return null;
            }
            Item item = Configuration.Instance.GetItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, msg.RelatedFQID.Kind, msg.RelatedFQID.ObjectId);
            if (item == null)
            {
                return null;
            }
            if (item.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && !String.IsNullOrWhiteSpace(item.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]))
            {
                if (msg.Data == null)
                {
                    return null;
                }
                string[] messageStrings = msg.Data as string[];
                if (messageStrings[0] == RadiniumSASDefinition.RADINIUM_MESSAGE_OPEN_DOOR)
                {
                    string command = String.Format(DoorOpenCloseRequest, DoorOpenCommand, item.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]);
                    byte[] results = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(command));
                    using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                    {
                        using (var context = new SaltoDbContext(conn, true))
                        {
                            SaltoAuditEntry entry = new SaltoAuditEntry();
                            string extDoorId = item.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID];
                            switch (item.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE])
                            {
                                case RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM:
                                    entry.Room = context.HotelRooms.FirstOrDefault(x => x.ExtDoorID == extDoorId);
                                    break;
                                case RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER:
                                    entry.Locker = context.Lockers.FirstOrDefault(x => x.ExtDoorID == extDoorId);
                                    break;
                                default:
                                    entry.Door = context.Doors.FirstOrDefault(x => x.ExtDoorID == extDoorId);
                                    break;
                            }

                            entry.Camera1Id = item.Properties[RadiniumSASDefinition.RADINIUM_CAMERA1];
                            entry.Camera2Id = item.Properties[RadiniumSASDefinition.RADINIUM_CAMERA2];
                            entry.DoorId = extDoorId;
                            entry.DoorName = item.Name.Substring(item.Name.IndexOf(" ") + 1);
                            entry.EventDate = DateTime.UtcNow;
                            entry.InternalEventId = 0;
                            entry.IsExit = false;
                            entry.Operation = (int)SaltoOperation.SoftwareOperatorCommandOpen;
                            entry.SubjectId = "";
                            entry.SubjectName = messageStrings[1];
                            entry.SubjectType = SaltoSubjectType.SoftwareOperator;
                            entry.User = null;
                            context.AuditEntries.Add(entry);
                            context.SaveChanges();
                            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_NEW_AUDIT_EVENTS, new SaltoAuditEntry[] { entry }));
                        }
                    }
                }
                if (msg.Data.ToString() == RadiniumSASDefinition.RADINIUM_MESSAGE_CLOSE_DOOR)
                {
                    string command = String.Format(DoorOpenCloseRequest, DoorCloseCommand, item.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]);
                    byte[] results = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(command));
                    using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                    {
                        using (var context = new SaltoDbContext(conn, true))
                        {
                            SaltoAuditEntry entry = new SaltoAuditEntry();
                            switch (item.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE])
                            {
                                case RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM:
                                    entry.Room = context.HotelRooms.FirstOrDefault(x => x.ExtDoorID == item.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]);
                                    break;
                                case RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER:
                                    entry.Locker = context.Lockers.FirstOrDefault(x => x.ExtDoorID == item.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]);
                                    break;
                                default:
                                    entry.Door = context.Doors.FirstOrDefault(x => x.ExtDoorID == item.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]);
                                    break;
                            }

                            entry.Camera1Id = item.Properties[RadiniumSASDefinition.RADINIUM_CAMERA1];
                            entry.Camera2Id = item.Properties[RadiniumSASDefinition.RADINIUM_CAMERA2];
                            entry.DoorId = item.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID];
                            entry.DoorName = item.Name.Substring(item.Name.IndexOf(" ") + 1);
                            entry.EventDate = DateTime.UtcNow;
                            entry.InternalEventId = 0;
                            entry.IsExit = false;
                            entry.Operation = (int)SaltoOperation.SoftwareOperatorCommandClose;
                            entry.SubjectId = "";
                            entry.SubjectName = messageStrings[1];
                            entry.SubjectType = SaltoSubjectType.SoftwareOperator;
                            entry.User = null;
                            context.AuditEntries.Add(entry);
                            context.SaveChanges();
                            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_NEW_AUDIT_EVENTS, new SaltoAuditEntry[] { entry }));
                        }
                    }
                }
            }
            return null;
        }

        private object stateReceiver = null, reportReceiver = null, auditRequestReceiver = null;
        private object receiveCommandReceiverOpen = null, receiveCommandReceiverClose = null;
        private object receiveCommandLockdownOpen = null, receiveCommandLockdownClose = null, receiveCommandLockdownEnd = null;
        private MessageCommunication comms = null;
        /// <summary>
        /// Called by the Environment when the user has logged in.
        /// </summary>
        public override void Init()
        {

            Array arr = Enum.GetValues(typeof(SaltoOperation));
            for (int i = 0; i < arr.Length; i++)
            {
                allSaltoOperations[(int)arr.GetValue(i)] = ((SaltoOperation)arr.GetValue(i)).ToDescriptionString();
            }
            _stop = false;
            _thread = new Thread(new ThreadStart(Run));
            _thread.Name = "RadiniumSAS ItemUpdater Background Thread";
            _thread.Start();
            ThreadPool.QueueUserWorkItem((x) => { BroadcastEmergencyState(); });

        }


        Dictionary<int, string> allSaltoOperations = new Dictionary<int, string>();

        private void BroadcastEmergencyState()
        {
            while(!_stop)
            {
                ThreadPool.QueueUserWorkItem((x) => {
                    if(comms != null)
                    {
                        comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_EMERGENCY_STATE, new string[] { ((int)currentEmergencyState).ToString() }.Concat(emergencyDoors).ToArray()), null, null, null);
                    }
                });
                Thread.Sleep(1000);
            }
        }

        private object AuditRequest(Message message, FQID destination, FQID source)
        {
            if (!SaltoLicenceValid)
            {
                return null;
            }

            AuditRequest request = message.Data as AuditRequest;
            if(request == null)
            {
                return null;
            }

            List<SaltoAuditEntry> results = new List<SaltoAuditEntry>();
            try
            {
                using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                {
                    using (var context = new SaltoDbContext(conn, true))
                    {
                        IEnumerable<SaltoAuditEntry> entries = context.AuditEntries;
                        if (request.CardholderName != null && !string.IsNullOrWhiteSpace(request.CardholderName))
                        {
                            entries = entries.Where(x => x.SubjectType == SaltoSubjectType.CardHolder && x.SubjectName != null && x.SubjectName.ToLowerInvariant().Contains(request.CardholderName.ToLowerInvariant()));
                        }
                        if (request.DoorId != null)
                        {
                            entries = entries.Where(x => x.DoorId == request.DoorId);
                        }
                        if (request.EventType != null)
                        {
                            entries = entries.Where(x => x.Operation == (int)request.EventType);
                        }
                        List<SaltoAuditEntry> auditEntries = entries.Where(x => allSaltoOperations.Keys.Contains(x.Operation) && 
                        x.Operation != 1000 && 
                        x.Operation != 1001).OrderByDescending(x => x.EventDate).Take(50).ToList();

                        auditEntries.ForEach(x => {
                            x.Room = context.HotelRooms.FirstOrDefault(y => y.ExtDoorID == x.DoorId);
                            x.Locker = context.Lockers.FirstOrDefault(y => y.ExtDoorID == x.DoorId);
                            x.Door = context.Doors.FirstOrDefault(y => y.ExtDoorID == x.DoorId);
                            x.User = context.Users.FirstOrDefault(y => y.ExtUserID == x.SubjectId && x.SubjectType == SaltoSubjectType.CardHolder);
                            if(x.User != null)
                            {
                                x.User.Photo = null;
                            }
                        });

                        SaltoAuditEntrySerialize[] entryArray = auditEntries.Select(x => new SaltoAuditEntrySerialize(x)).ToArray();

                        comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_RESPONSE_AUDIT, entryArray), message.ExternalMessageSourceEndPoint, null, null);
                    }
                }
            }
            catch(Exception e)
            {
                EnvironmentManager.Instance.Log(true, "Radinium Access Item Updater", "Could not retrieve audit list from SQL database! Error: " + e.Message);
            }

            return null;
        }


        private object ItemStateRequest(Message message, FQID destination, FQID source)
        {
            if (!SaltoLicenceValid)
            {
                return null;
            }

            List<List<string> > returnList = new List<List<string> >();
            try
            {
                using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                {
                    using (var context = new SaltoDbContext(conn, true))
                    {
                        List<string> userStrings = new List<string>();
                        List<string> doorStrings = new List<string>();
                        List<string> roomStrings = new List<string>();
                        List<string> lockerStrings = new List<string>();

                        context.Users.ToList().ForEach(x =>
                        {
                            StringBuilder xmlStringBuilder = new StringBuilder();
                            StringWriter writer = new StringWriter(xmlStringBuilder);
                            userSerializer.Serialize(writer, x);
                            writer.Flush();
                            userStrings.Add(xmlStringBuilder.ToString());
                        });

                        context.Doors.ToList().ForEach(x =>
                        {
                            StringBuilder xmlStringBuilder = new StringBuilder();
                            StringWriter writer = new StringWriter(xmlStringBuilder);
                            doorSerializer.Serialize(writer, x);
                            writer.Flush();
                            doorStrings.Add(xmlStringBuilder.ToString());
                        });

                        context.Lockers.ToList().ForEach(x =>
                        {
                            StringBuilder xmlStringBuilder = new StringBuilder();
                            StringWriter writer = new StringWriter(xmlStringBuilder);
                            lockerSerializer.Serialize(writer, x);
                            writer.Flush();
                            lockerStrings.Add(xmlStringBuilder.ToString());
                        });

                        context.HotelRooms.ToList().ForEach(x =>
                        {
                            StringBuilder xmlStringBuilder = new StringBuilder();
                            StringWriter writer = new StringWriter(xmlStringBuilder);
                            roomSerializer.Serialize(writer, x);
                            writer.Flush();
                            roomStrings.Add(xmlStringBuilder.ToString());
                        });

                        returnList.Add(userStrings);
                        returnList.Add(doorStrings);
                        returnList.Add(lockerStrings);
                        returnList.Add(roomStrings);
                    }
                }
            }
            catch(Exception e)
            {
                EnvironmentManager.Instance.Log(true, "Radinium Access", "Could not retrieve item state from SQL database! Error: " + e.Message);
            }
            comms.TransmitMessage(new Message(RadiniumSASDefinition.RADINIUM_MESSAGE_RESPONSE_ITEMS, returnList.ToArray()), message.ExternalMessageSourceEndPoint, null, null);
            return null;
        }



        private object ReportRequest(Message message, FQID destination, FQID source)
        {
            if (!SaltoLicenceValid)
            {
                return null;
            }

            Dictionary<int, string> enumPairs = new Dictionary<int, string>();
            ReportRequestObject requestObj = message.Data as ReportRequestObject;

            Array arr = Enum.GetValues(typeof(SaltoOperation));
            for (int i = 0; i < arr.Length; i++)
            {
                enumPairs[(int)arr.GetValue(i)] = ((SaltoOperation)arr.GetValue(i)).ToDescriptionString();
            }

            try
            {
                PdfReport report = new PdfReport();
                byte[] reportBytes = report.DocumentPreferences(doc =>
                {
                    doc.Orientation(PageOrientation.Portrait);
                    doc.RunDirection(PdfRunDirection.LeftToRight);
                    doc.PageSize(PdfPageSize.A4);
                    doc.DocumentMetadata(new DocumentMetadata
                    {
                        Author = "Radinium (Pty) Ltd",
                        Application = "Radinium Access",
                        Keywords = "",
                        Subject = "Access Audit",
                        Title = "Access Audit"
                    });
                })
                    .DefaultFonts(fonts =>
                    {
                        fonts.Path(Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\arial.ttf",
                                   Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\verdana.ttf");
                    })
                    .PagesFooter(footer =>
                    {
                        footer.DefaultFooter(DateTime.Now.ToString("yyyy/MM/dd"));
                    })
                    .PagesHeader(header =>
                    {
                        header.HtmlHeader(htmlHeader => {
                            htmlHeader.PageHeaderProperties(new HeaderBasicProperties
                            {
                                RunDirection = PdfRunDirection.LeftToRight,
                                ShowBorder = false,
                                PdfFont = header.PdfFont
                            });
                            htmlHeader.AddPageHeader((headerData) => {
                                var msg = "SALTO Access Audit";
                                var photo = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Resources\\Radinium.png";
                                var image = string.Format("<img height='64px' src='{0}' />", photo);
                                var auditSubheader = "Selected Date: " + requestObj.FromDate.ToString("yyyy-MM-dd") + " to " + requestObj.ToDate.ToString("yyyy-MM-dd");
                                return string.Format(@"<table style='width: 100%;font-size:16pt;font-family:tahoma;font-weight: bold; height: 70px;'>
													<tr>
														<td align='right'>{0}</td>
													</tr>
													<tr>
														<td align='left'>{1}</td>
													</tr>
												</table><h4>{2}</h4>", image, msg, auditSubheader);
                            });
                        });
                    })
                    .MainTableTemplate(template =>
                    {
                        template.BasicTemplate(BasicTemplate.ClassicTemplate);
                    })
                    .MainTablePreferences(table =>
                    {
                        table.ColumnsWidthsType(TableColumnWidthType.Relative);
                    })
                    .MainTableColumns(columns =>
                    {
                        columns.AddColumn(column =>
                        {
                            column.HeaderCell("Event Date");
                            column.IsVisible(true);
                            column.PropertyName("EventDate");
                            column.Order(0);
                            column.CellsHorizontalAlignment(PdfRpt.Core.Contracts.HorizontalAlignment.Center);
                            column.Width(1.2f);
                        });
                        columns.AddColumn(column =>
                        {
                            column.HeaderCell("Event");
                            column.IsVisible(true);
                            column.PropertyName("AuditEvent");
                            column.Order(1);
                            column.CellsHorizontalAlignment(PdfRpt.Core.Contracts.HorizontalAlignment.Center);
                            column.Width(3);
                        });
                        columns.AddColumn(column =>
                        {
                            column.HeaderCell("Cardholder");
                            column.IsVisible(true);
                            column.PropertyName("UserName");
                            column.Order(2);
                            column.CellsHorizontalAlignment(PdfRpt.Core.Contracts.HorizontalAlignment.Center);
                            column.Width(3);
                        });
                        columns.AddColumn(column =>
                        {
                            column.HeaderCell("Door");
                            column.IsVisible(true);
                            column.PropertyName("DoorName");
                            column.Order(3);
                            column.CellsHorizontalAlignment(PdfRpt.Core.Contracts.HorizontalAlignment.Center);
                            column.Width(3);
                        });
                        columns.AddColumn(column =>
                        {
                            column.HeaderCell("Access");
                            column.IsVisible(true);
                            column.PropertyName("Exit");
                            column.Order(4);
                            column.CellsHorizontalAlignment(PdfRpt.Core.Contracts.HorizontalAlignment.Center);
                            column.Width(1);
                        });
                    })
                    .MainTableDataSource(dataSource =>
                    {
                        List<AuditTraceItem> auditList = new List<AuditTraceItem>();
                        using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                        {
                            using (var context = new SaltoDbContext(conn, true))
                            {
                                bool filterByUser = true == requestObj.CardholderFilterIsChecked;
                                bool filterByDoor = true == requestObj.DoorFilterIsChecked;
                                bool filterByEvent = true == requestObj.EventTypeFilterIsChecked;
                                DateTime fromDate = requestObj.FromDate.StartOfDay();
                                DateTime toDate = requestObj.ToDate.EndOfDay();
                                IEnumerable<SaltoAuditEntry> auditFiltered = context.AuditEntries.Where(x => x.EventDate > fromDate && x.EventDate < toDate).ToList();
                                if (filterByUser)
                                {
                                    auditFiltered = auditFiltered.Where(x => x.SubjectType == SaltoSubjectType.CardHolder && requestObj.selectedUsers.Any(y => y.ExtUserID == x.SubjectId));
                                }
                                if (filterByDoor)
                                {
                                    auditFiltered = auditFiltered.Where(x => requestObj.selectedLockers.Any(y => y.ExtDoorID == x.DoorId) || requestObj.selectedRooms.Any(y => y.ExtDoorID == x.DoorId) || requestObj.selectedDoors.Any(y => y.ExtDoorID == x.DoorId));
                                }
                                if (filterByEvent)
                                {
                                    auditFiltered = auditFiltered.Where(x => requestObj.selectedEventTypes.Any(y => y == (int)x.Operation));
                                }
                                auditFiltered.ToList().ForEach(x =>
                                {
                                    AuditTraceItem item = new AuditTraceItem();
                                    item.AuditEvent = enumPairs.ContainsKey((int)x.Operation) ? enumPairs[(int)x.Operation] : ("Unknown (" + x.Operation.ToString() + ")");
                                    item.DoorName = x.DoorName;
                                    item.EventDate = x.EventDate.ToLocalTime().ToString();
                                    item.Exit = x.IsExit ? "Exit" : "Entry";
                                    item.UserName = x.SubjectName;
                                    auditList.Add(item);
                                });
                            }
                        }
                        dataSource.StronglyTypedList(auditList.OrderByDescending(x => x.EventDate).ToList());
                    }).GenerateAsByteArray();
                comms.TransmitMessage(new Message(RadiniumSASDefinition.RADINIUM_MESSAGE_RESPONSE_REPORT, reportBytes), message.ExternalMessageSourceEndPoint, null, null);
            }
            catch(Exception e)
            {
                EnvironmentManager.Instance.Log(true, "Radinium Access Item Updater", "Could not generate report! Error: " + e.Message);
                comms.TransmitMessage(new Message(RadiniumSASDefinition.RADINIUM_MESSAGE_RESPONSE_REPORT, e.Message), message.ExternalMessageSourceEndPoint, null, null);
            }

            return null;
        }

        /// <summary>
        /// Called by the Environment when the user log's out.
        /// You should close all remote sessions and flush cache information, as the
        /// user might logon to another server next time.
        /// </summary>
        public override void Close()
        {
            _stop = true;
        }

        /// <summary>
        /// Define in what Environments the current background task should be started.
        /// </summary>
        public override List<EnvironmentType> TargetEnvironments
        {
            get { return new List<EnvironmentType>() { EnvironmentType.Service }; } // Default will run in the Event Server
        }

        public Dictionary<int, SaltoAuditEntry> entryDictionary = new Dictionary<int, SaltoAuditEntry>();

        const string LockerDetailsRequest = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>SaltoDBLockerList.Read</RequestName>
    <Params>
        <MaxCount>100</MaxCount>
        <ReturnMembership_Door_Zone>0</ReturnMembership_Door_Zone>
        {0}
    </Params>
</RequestCall>
";
        const string RoomDetailsRequest = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>SaltoDBHotelRoomList.Read</RequestName>
    <Params>
        <MaxCount>100</MaxCount>
        {0}
    </Params>
</RequestCall>
";

        const string DoorDetailsRequest = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>SaltoDBDoorList.Read</RequestName>
    <Params>
        <MaxCount>100</MaxCount>
        <ReturnMembership_Door_Zone>0</ReturnMembership_Door_Zone>
        {0}
    </Params>
</RequestCall>
";
        const string UserDetailsRequest = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>SaltoDBUserList.Read</RequestName>
    <Params>
        <MaxCount>100</MaxCount>
        {0}
    </Params>
</RequestCall>
";

        private bool ProcessEntity(dynamic door, List<dynamic> saltoDoors)
        {
            bool toSave = false;
            if(saltoDoors.Any(x => x.ExtDoorID == door.ExtDoorID))
            {
                dynamic oldDoor = saltoDoors.First(x => x.ExtDoorID == door.ExtDoorID);
                if(oldDoor.Name != door.Name)
                {
                    toSave = true;
                    oldDoor.Name = door.Name;
                }
                PropertyInfo[] fields = oldDoor.GetType().GetProperties();
                foreach (PropertyInfo field in fields)
                {
                    IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                    if (attrArr.Count() > 0)
                    {
                        XmlElementAttribute a = attrArr.First();
                        if(a == null)
                        {
                            continue;
                        }
                        object oldValue = field.GetValue(oldDoor);
                        object newValue = field.GetValue(door);
                        if((oldValue == null && newValue != null && field.GetCustomAttributes<SaveInItemAttribute>().Count() > 0) || (oldValue != null && newValue != null && oldValue.ToString() != newValue.ToString() && field.GetCustomAttributes<SaveInItemAttribute>().Count() > 0))
                        {
                            toSave = true;
                        }
                        field.SetValue(oldDoor, field.GetValue(door));
                    }
                }
            }
            else
            {
                toSave = true;
            }
            return toSave;
        }

        private void ProcessUser(SaltoDbUser user, XDocument outerConfigNode)
        {
            List<XElement> userChildren = outerConfigNode.Descendants("SaltoDbUser").ToList();
            List<SaltoDbUser> userList = new List<SaltoDbUser>();
            foreach (XElement node in userChildren)
            {
                SaltoDbUser listedUser = userSerializer.Deserialize(new StringReader(node.ToString())) as SaltoDbUser;
                userList.Add(listedUser);
            }
            if(userList.Any(x => x.ExtUserID == user.ExtUserID))
            {
                return;
            }

            StringBuilder xml = new StringBuilder();
            StringWriter writer = new StringWriter(xml);
            userSerializer.Serialize(writer, user);
            writer.Flush();
            XDocument doc = XDocument.Parse(writer.ToString());
            outerConfigNode.Descendants("SaltoUsers").First().Add(doc.Root);
        }

        public static volatile bool SaltoLicenceValid = false;
        public static volatile string SaltoLicenceMessage = "Licence not loaded yet";

        void LicenceValidateThread()
        {
            while(!_stop)
            {
                Configuration.Instance.RefreshConfiguration(RadiniumSASDefinition.RadiniumSASKind);
                int numDoors = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind).Count;
                RadiniumSASLicenceValidator validator = new RadiniumSASLicenceValidator();
                if (!validator.ValidateLicence(new object[] { }))
                {
                    EnvironmentManager.Instance.Log(false, "Radinium SALTO background thread", "Invalid licence");
                    SaltoLicenceValid = false;
                    Thread.Sleep(10000);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_LICENCE_INVALID, "No valid licence available for SALTO!"));
                    continue;
                }
                Base64Licence licence = validator.LoadLicence();
                if (licence == null)
                {
                    EnvironmentManager.Instance.Log(false, "Radinium SALTO background thread", "No active licences for SALTO", null);
                    SaltoLicenceValid = false;
                    Thread.Sleep(10000);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_LICENCE_INVALID, "No active licences for SALTO!"));
                    continue;
                }
                if (licence.ExpiryDateDateTime < DateTime.Now && licence.LicenceType == LicenceType.Grace)
                {
                    EnvironmentManager.Instance.Log(false, "Radinium SALTO background thread", "SALTO Licences expired", null);
                    SaltoLicenceValid = false;
                    Thread.Sleep(10000);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_LICENCE_INVALID, "Grace licence for SALTO expired!"));
                    continue;
                }
                if (licence.NumLicences < numDoors)
                {
                    EnvironmentManager.Instance.Log(false, "Radinium SALTO background thread", "Insufficient active licences for SALTO", null);
                    SaltoLicenceValid = false;
                    Thread.Sleep(10000);
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_LICENCE_INVALID, "Insufficient active licences for SALTO!"));
                    continue;
                }
                SaltoLicenceValid = true;
                if(SaltoLicenceValid)
                {
                    Thread.Sleep(300000);
                }
                else
                {
                    Thread.Sleep(10000);
                }
            }
        }

        private void ReadAndUpdateEntities(Type doorType, List<Item> doors)
        {
            string request = DoorDetailsRequest;
            if(doorType == typeof(SaltoDbLocker))
            {
                request = LockerDetailsRequest;
            }
            if(doorType == typeof(SaltoDbRoom))
            {
                request = RoomDetailsRequest;
            }
            string nodeString = "";
            byte[] entityData = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(string.Format(request, "")));
            bool eof = false;
            IEnumerable<SaltoDbDoor> saltoDoors = new List<SaltoDbDoor>();
            IEnumerable<SaltoDbLocker> saltoLockers = new List<SaltoDbLocker>();
            IEnumerable<SaltoDbRoom> saltoRooms = new List<SaltoDbRoom>();
            if(doorType == typeof(SaltoDbDoor))
            {
                saltoDoors = doors.Where(x => !x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ITEM_TYPE) || x.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] == RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR).Select(x => SaltoDbDoor.ConvertItemToDoor(x));
            }
            if(doorType == typeof(SaltoDbLocker))
            {
                saltoLockers = doors.Where(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ITEM_TYPE) && x.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] == RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER).Select(x => SaltoDbLocker.ConvertItemToLocker(x));
            }
            if (doorType == typeof(SaltoDbRoom))
            {
                saltoRooms = doors.Where(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ITEM_TYPE) && x.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] == RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM).Select(x => SaltoDbRoom.ConvertItemToRoom(x));
            }
            while (!eof && entityData != null)
            {
                string dataStr = UTF8Encoding.UTF8.GetString(entityData);
                XDocument doorDoc = XDocument.Parse(dataStr);
                doorDoc.Descendants().Where(e => string.IsNullOrEmpty(e.Value)).Remove();
                IEnumerable<XNode> doorChildren = null;
                if (doorType == typeof(SaltoDbDoor))
                {
                    doorChildren = doorDoc.Descendants("SaltoDBDoor");
                }
                if(doorType == typeof(SaltoDbLocker))
                {
                    doorChildren = doorDoc.Descendants("SaltoDBLocker");
                }
                if(doorType == typeof(SaltoDbRoom))
                {
                    doorChildren = doorDoc.Descendants("SaltoDBHotelRoom");
                }

                string finalDoorUid = "";
                List<dynamic> newDoorList = new List<dynamic>();
                List<dynamic> toSaveDoorList = new List<dynamic>();
                List<dynamic> saltoEntities = null;
                foreach (XNode node in doorChildren)
                {
                    nodeString = node.ToString();
                    dynamic door = null;
                    switch (doorType.Name)
                    {
                        case "SaltoDbDoor":
                            door = doorSerializer.Deserialize(new StringReader(nodeString)) as SaltoDbDoor;
                            saltoEntities = saltoDoors.Select(x => (dynamic)x).ToList();
                            break;
                        case "SaltoDbLocker":
                            door = lockerSerializer.Deserialize(new StringReader(nodeString)) as SaltoDbLocker;
                            saltoEntities = saltoLockers.Select(x => (dynamic)x).ToList();
                            break;
                        case "SaltoDbRoom":
                            door = roomSerializer.Deserialize(new StringReader(nodeString)) as SaltoDbRoom;
                            saltoEntities = saltoRooms.Select(x => (dynamic)x).ToList();
                            break;
                        default:
                            continue;
                    }

                    if (door == null)
                    {
                        continue;
                    }

                    bool needToSave = ProcessEntity(door, saltoEntities);
                    if (needToSave)
                    {
                        toSaveDoorList.Add(door);
                    }
                    finalDoorUid = door.ExtDoorID;
                    newDoorList.Add(door);
                }


                try
                {
                    using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                    {
                        using (var context = new SaltoDbContext(conn, contextOwnsConnection: true))
                        {
                            foreach (dynamic dr in newDoorList)
                            {
                                switch (doorType.Name)
                                {
                                    case "SaltoDbDoor":
                                        SaltoDbDoor door = (SaltoDbDoor)dr;
                                        if (!context.Doors.Any(x => x.ExtDoorID == door.ExtDoorID))
                                        {
                                            context.Doors.Add(door);
                                        }
                                        else
                                        {
                                            SaltoDbDoor existingDoor = context.Doors.FirstOrDefault(x => x.ExtDoorID == door.ExtDoorID);
                                            door.CopyFieldsTo(existingDoor);
                                        }
                                        break;
                                    case "SaltoDbLocker":
                                        SaltoDbLocker locker = (SaltoDbLocker)dr;
                                        if (!context.Lockers.Any(x => x.ExtDoorID == locker.ExtDoorID))
                                        {
                                            context.Lockers.Add(locker);
                                        }
                                        else
                                        {
                                            SaltoDbLocker existingDoor = context.Lockers.FirstOrDefault(x => x.ExtDoorID == locker.ExtDoorID);
                                            locker.CopyFieldsTo(existingDoor);
                                        }
                                        break;
                                    case "SaltoDbRoom":
                                        SaltoDbRoom room = (SaltoDbRoom)dr;
                                        if (!context.HotelRooms.Any(x => x.ExtDoorID == room.ExtDoorID))
                                        {
                                            context.HotelRooms.Add(room);
                                        }
                                        else
                                        {
                                            SaltoDbRoom existingDoor = context.HotelRooms.FirstOrDefault(x => x.ExtDoorID == room.ExtDoorID);
                                            room.CopyFieldsTo(existingDoor);
                                        }
                                        break;
                                }
                            }
                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception e)
                {
                    EnvironmentManager.Instance.Log(true, "Radinium Access Item Updater Thread", "Unable to save doors and users to database: Exception " + e.Message);
                }

                IEnumerable<XNode> eofNode = doorDoc.Descendants("EOF");
                eof = true;
                if (eofNode.Count() > 0 && finalDoorUid != "")
                {
                    if (eofNode.First().ToString().ToUpper() == "<EOF>0</EOF>")
                    {
                        eof = false;
                    }
                }
                entityData = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(string.Format(request, "<StartingFromExtDoorID>" + finalDoorUid + "</StartingFromExtDoorID>")));
            }
        }


        /// <summary>
        /// the thread doing the work
        /// </summary>
        private void Run()
        {
            ThreadPool.QueueUserWorkItem((a) =>
            {
                LicenceValidateThread();
            });

            MessageCommunicationManager.Start(EnvironmentManager.Instance.CurrentSite.ServerId);
            MessageCommunicationManager.Security = MessageCommunicationManager.SecurityLevel.Low;
            comms = MessageCommunicationManager.Get(EnvironmentManager.Instance.CurrentSite.ServerId);
            Thread.Sleep(10000);
            receiveCommandReceiverOpen = comms.RegisterCommunicationFilter(ReceiveCommand, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_OPEN_DOOR));
            receiveCommandReceiverClose = comms.RegisterCommunicationFilter(ReceiveCommand, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_CLOSE_DOOR));
            receiveCommandLockdownOpen = comms.RegisterCommunicationFilter(ReceiveLockdownCommand, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_EMERGENCY_OPEN));
            receiveCommandLockdownClose = comms.RegisterCommunicationFilter(ReceiveLockdownCommand, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_CLOSE));
            receiveCommandLockdownEnd = comms.RegisterCommunicationFilter(ReceiveLockdownCommand, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_END));
            stateReceiver = comms.RegisterCommunicationFilter(ItemStateRequest, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_ITEMS));
            reportReceiver = comms.RegisterCommunicationFilter(ReportRequest, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_REPORT));
            auditRequestReceiver = comms.RegisterCommunicationFilter(AuditRequest, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_AUDIT));
            EnvironmentManager.Instance.Log(false, "RadiniumSALTO Itemupdater background thread", "Now starting...", null);
            while (!_stop)
            {
                if(!SaltoLicenceValid)
                {
                    Thread.Sleep(10000);
                    continue;
                }
                Configuration.Instance.RefreshConfiguration(RadiniumSASDefinition.RadiniumSASKind);
                string nodeString = "";
                SaltoUtility.UpdateConfig(Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.RadiniumSASServerSettings, false));
                List<Item> doors = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind);
                doors.Where(x => !x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) || String.IsNullOrWhiteSpace(x.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID])).ToList().ForEach(x =>
                    {
                        Configuration.Instance.DeleteItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, x);
                    });

                XmlNode configNode = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.UserSettingsGuid, false);
                XDocument outerConfigNode;
                if (configNode.OuterXml == "")
                {
                    outerConfigNode = XDocument.Parse("<Root></Root>");
                }
                else
                {
                    outerConfigNode = XDocument.Parse(configNode.OuterXml);
                }
                if (outerConfigNode.Element("Root").Element("SaltoUsers") == null)
                {
                    XElement node = new XElement(XName.Get("SaltoUsers"));
                    outerConfigNode.Element("Root").Add(node);
                }
                try
                {
                    ReadAndUpdateEntities(typeof(SaltoDbDoor), doors);
                }
                catch (Exception e)
                {
                    EnvironmentManager.Instance.Log(false, "RadiniumSALTO Itemupdater background thread", "Cannot parse returned XML for doors");
                }
                try
                {
                    ReadAndUpdateEntities(typeof(SaltoDbLocker), doors);
                }
                catch (Exception e)
                {
                    EnvironmentManager.Instance.Log(false, "RadiniumSALTO Itemupdater background thread", "Cannot parse returned XML for lockers");
                }
                try
                {
                    ReadAndUpdateEntities(typeof(SaltoDbRoom), doors);
                }
                catch (Exception e)
                {
                    EnvironmentManager.Instance.Log(false, "RadiniumSALTO Itemupdater background thread", "Cannot parse returned XML for rooms");
                }

                try
                {
                    byte[] userData = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(string.Format(UserDetailsRequest, "")));
                    bool eof = false;
                    while (!eof && userData != null)
                    {
                        if (userData == null)
                        {
                            break;
                        }
                        string dataStr = UTF8Encoding.UTF8.GetString(userData);
                        XDocument userDoc = XDocument.Parse(dataStr);
                        userDoc.Descendants().Where(e => string.IsNullOrEmpty(e.Value)).Remove();
                        IEnumerable<XNode> userChildren = userDoc.Descendants("SaltoDBUser");
                        string finalUserUid = "";
                        foreach (XNode node in userChildren)
                        {
                            nodeString = node.ToString();
                            SaltoDbUser user = userSerializer.Deserialize(new StringReader(nodeString)) as SaltoDbUser;
                            if (user == null)
                            {
                                continue;
                            }
                            ProcessUser(user, outerConfigNode);
                            finalUserUid = user.ExtUserID;
                        }

                        IEnumerable<XNode> eofNode = userDoc.Descendants("EOF");
                        eof = true;
                        if (eofNode.Count() > 0 && finalUserUid != "" && userData != null)
                        {
                            if (eofNode.First().ToString().ToUpper() == "<EOF>0</EOF>")
                            {
                                eof = false;
                                userData = SaltoUtility.SendSaltoRequest(SaltoUtility.ServerHost, SaltoUtility.ServerPort, UTF8Encoding.UTF8.GetBytes(string.Format(UserDetailsRequest, "<StartingFromExtUserID>" + finalUserUid + "</StartingFromExtUserID>")));
                            }
                        }
                    }
                    List<SaltoDbUser> saltoUsers = outerConfigNode.Descendants("SaltoDBUser").Select(x => userSerializer.Deserialize(new StringReader(x.ToString())) as SaltoDbUser).Where(x => x != null).ToList();
                    List<string> ExtUserIdList = new List<string>();
                    using (var conn = new SqlConnection(Utility.SQL_CONNECTION_STRING()))
                    {
                        using (var context = new SaltoDbContext(conn, contextOwnsConnection: true))
                        {
                            foreach (SaltoDbUser user in saltoUsers)
                            {
                                if (!context.Users.Any(x => x.ExtUserID == user.ExtUserID) && !ExtUserIdList.Contains(user.ExtUserID))
                                {
                                    ExtUserIdList.Add(user.ExtUserID);
                                    user.Photo = new byte[0];
                                    context.Users.Add(user);
                                }
                                else
                                {
                                    if(!ExtUserIdList.Contains(user.ExtUserID))
                                    {
                                        SaltoDbUser existingUser = context.Users.FirstOrDefault(x => x.ExtUserID == user.ExtUserID);
                                        user.CopyFieldsTo(existingUser);
                                    }
                                }
                            }
                            context.SaveChanges();
                        }
                    }

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(outerConfigNode.ToString());
                    Configuration.Instance.SaveOptionsConfiguration(RadiniumSASDefinition.UserSettingsGuid, false, doc.SelectSingleNode("Root"));
                }
                catch (Exception e)
                {
                    EnvironmentManager.Instance.Log(false, "RadiniumSALTO Itemupdater background thread", "Cannot parse returned XML for user: Message " + e.Message + " - " + e.StackTrace);
                }

                //Now make sure the users and doors are added to the database


                Thread.Sleep(UPDATE_PERIOD_SECONDS * 1000);
            }
            comms.UnRegisterCommunicationFilter(receiveCommandReceiverOpen);
            comms.UnRegisterCommunicationFilter(receiveCommandReceiverClose);
            comms.UnRegisterCommunicationFilter(stateReceiver);
            comms.UnRegisterCommunicationFilter(reportReceiver);
            comms.UnRegisterCommunicationFilter(auditRequestReceiver);
            comms.UnRegisterCommunicationFilter(receiveCommandLockdownOpen);
            comms.UnRegisterCommunicationFilter(receiveCommandLockdownClose);
            comms.UnRegisterCommunicationFilter(receiveCommandLockdownEnd);
            MessageCommunicationManager.Stop(EnvironmentManager.Instance.CurrentSite.ServerId);

            EnvironmentManager.Instance.Log(false, "RadiniumSALTO Itemupdater background thread", "Now stopping...", null);
            _thread = null;
        }
    }
}
