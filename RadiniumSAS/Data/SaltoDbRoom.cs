﻿using RadiniumSAS.Background;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using VideoOS.Platform;

namespace RadiniumSAS.Data
{

    [Serializable]
    [XmlRoot("SaltoDBHotelRoom")]
    public class SaltoDbRoom
    {

        public void CopyFieldsTo(SaltoDbRoom otherRoom)
        {
            if (otherRoom == null)
            {
                return;
            }
            PropertyInfo[] fields = GetType().GetProperties();
            foreach (PropertyInfo field in fields)
            {
                if (field.GetCustomAttributes<XmlElementAttribute>().Count() > 0)
                {
                    if (field.GetCustomAttributes<KeyAttribute>().Count() > 0)
                    {
                        continue;
                    }
                    object newValue = field.GetValue(this);
                    if (newValue != null)
                    {
                        field.SetValue(otherRoom, newValue);
                    }
                }
            }
        }

        public static SaltoDbRoom ConvertItemToRoom(VideoOS.Platform.Item room)
        {
            SaltoDbRoom ReturnRoom = new SaltoDbRoom();
            ReturnRoom.Name = "(Room) " + room.Name;
            PropertyInfo[] fields = ReturnRoom.GetType().GetProperties();
            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    if (!room.Properties.ContainsKey(a.ElementName))
                    {
                        continue;
                    }
                    string val = room.Properties[a.ElementName];
                    IEnumerable<MethodInfo> methods = field.PropertyType.GetRuntimeMethods();
                    try
                    {
                        if (field.PropertyType == typeof(string))
                        {
                            field.SetValue(ReturnRoom, val);
                        }
                        if (field.PropertyType == typeof(int?))
                        {
                            field.SetValue(ReturnRoom, int.Parse(val));
                        }
                        if (field.PropertyType == typeof(bool?))
                        {
                            field.SetValue(ReturnRoom, bool.Parse(val));
                        }
                        if (field.PropertyType == typeof(DateTime?))
                        {
                            field.SetValue(ReturnRoom, DateTime.Parse(val));
                        }
                        methods.Where(x => x.Name == "Parse" && x.GetParameters().Count() == 1 && x.GetParameters()[1].ParameterType == typeof(string)).ToList().ForEach(x =>
                        {
                            field.SetValue(ReturnRoom, x.Invoke(field.PropertyType, new object[] { val }));
                        });
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return ReturnRoom;
        }

        public static void CopyPropertiesToItem(SaltoDbRoom room, Item item)
        {
            PropertyInfo[] fields = item.GetType().GetProperties();

            if (item.Name != room.Name && room.Name != null)
            {
                item.Name = room.Name;
            }

            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    object roomValVal = field.GetValue(room);
                    if (roomValVal != null)
                    {
                        item.Properties[a.ElementName] = roomValVal.ToString();
                    }
                }
            }
        }

        public static VideoOS.Platform.Item ConvertRoomToItem(SaltoDbRoom room)
        {
            VideoOS.Platform.Item ReturnRoom = new VideoOS.Platform.Item();
            PropertyInfo[] fields = room.GetType().GetProperties();

            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    object roomVal = field.GetValue(room);
                    if (roomVal != null && a.ElementName != "Name")
                    {
                        ReturnRoom.Properties[a.ElementName] = roomVal.ToString();
                    }
                }
            }
            ReturnRoom.HasChildren = HasChildren.No;
            ReturnRoom.HasRelated = HasRelated.No;
            ReturnRoom.Name = "(Room) " + room.Name;
            ReturnRoom.FQID = new VideoOS.Platform.FQID(EnvironmentManager.Instance.CurrentSite.ServerId, FolderType.No, RadiniumSASDefinition.RadiniumSASKind);
            ReturnRoom.FQID.ObjectId = Guid.NewGuid();
            return ReturnRoom;
        }

        public override string ToString()
        {
            return Name;
        }

        [SaveInItem]
        [Key]
        [XmlElement("ExtDoorID")]
        public string ExtDoorID { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Name")]
        public string Name { get; set; }
        [Column]
        [XmlElement("BatteryStatus")]
        public int BatteryStatus { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Description")]
        public string Description { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("GPF1")]
        public string GPF1 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("GPF2")]
        public string GPF2 { get; set; }
        [Column]
        [XmlElement("OpenTime")]
        public int? OpenTime { get; set; }
        [Column]
        [XmlElement("OpenTimeADA")]
        public int? OpenTimeADA { get; set; }
        [Column]
        [XmlElement("AuditOnKeys")]
        public bool? AuditOnKeys { get; set; }
        [Column]
        [XmlElement("UpdateRequired")]
        public bool? UpdateRequired { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera1")]
        public string Camera1 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera2")]
        public string Camera2 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera3")]
        public string Camera3 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera4")]
        public string Camera4 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("LinkedMap")]
        public string LinkedMap { get; set; }
        [Column]
        [XmlElement("ExtParentHotelSuiteID")]
        public string ExtParentHotelSuiteID { get; set; }
    }
}
