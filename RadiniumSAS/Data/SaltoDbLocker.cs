﻿using RadiniumSAS.Background;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using VideoOS.Platform;

namespace RadiniumSAS.Data
{

    [Serializable]
    [XmlRoot("SaltoDBLocker")]
    public class SaltoDbLocker
    {

        public void CopyFieldsTo(SaltoDbLocker otherLocker)
        {
            if (otherLocker == null)
            {
                return;
            }
            PropertyInfo[] fields = GetType().GetProperties();
            foreach (PropertyInfo field in fields)
            {
                if (field.GetCustomAttributes<XmlElementAttribute>().Count() > 0)
                {
                    if (field.GetCustomAttributes<KeyAttribute>().Count() > 0)
                    {
                        continue;
                    }
                    object newValue = field.GetValue(this);
                    if (newValue != null)
                    {
                        field.SetValue(otherLocker, newValue);
                    }
                }
            }
        }

        public static SaltoDbLocker ConvertItemToLocker(VideoOS.Platform.Item locker)
        {
            SaltoDbLocker ReturnLocker = new SaltoDbLocker();
            ReturnLocker.Name = locker.Name;
            PropertyInfo[] fields = ReturnLocker.GetType().GetProperties();
            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    if (!locker.Properties.ContainsKey(a.ElementName))
                    {
                        continue;
                    }
                    string val = locker.Properties[a.ElementName];
                    IEnumerable<MethodInfo> methods = field.PropertyType.GetRuntimeMethods();
                    try
                    {
                        if (field.PropertyType == typeof(string))
                        {
                            field.SetValue(ReturnLocker, val);
                        }
                        if (field.PropertyType == typeof(int?))
                        {
                            field.SetValue(ReturnLocker, int.Parse(val));
                        }
                        if (field.PropertyType == typeof(bool?))
                        {
                            field.SetValue(ReturnLocker, bool.Parse(val));
                        }
                        if (field.PropertyType == typeof(DateTime?))
                        {
                            field.SetValue(ReturnLocker, DateTime.Parse(val));
                        }
                        methods.Where(x => x.Name == "Parse" && x.GetParameters().Count() == 1 && x.GetParameters()[1].ParameterType == typeof(string)).ToList().ForEach(x =>
                        {
                            field.SetValue(ReturnLocker, x.Invoke(field.PropertyType, new object[] { val }));
                        });
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return ReturnLocker;
        }

        public static void CopyPropertiesToItem(SaltoDbLocker locker, Item item)
        {
            PropertyInfo[] fields = item.GetType().GetProperties();

            if (item.Name != locker.Name && locker.Name != null)
            {
                item.Name = locker.Name;
            }

            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    object lockerVal = field.GetValue(locker);
                    if (lockerVal != null)
                    {
                        item.Properties[a.ElementName] = lockerVal.ToString();
                    }
                }
            }
        }

        public static VideoOS.Platform.Item ConvertLockerToItem(SaltoDbLocker locker)
        {
            VideoOS.Platform.Item ReturnLocker = new VideoOS.Platform.Item();
            PropertyInfo[] fields = locker.GetType().GetProperties();

            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    object lockerVal = field.GetValue(locker);
                    if (lockerVal != null && a.ElementName != "Name")
                    {
                        ReturnLocker.Properties[a.ElementName] = lockerVal.ToString();
                    }
                }
            }
            ReturnLocker.HasChildren = HasChildren.No;
            ReturnLocker.HasRelated = HasRelated.No;
            ReturnLocker.Name = "(Locker) " + locker.Name;
            ReturnLocker.FQID = new VideoOS.Platform.FQID(EnvironmentManager.Instance.CurrentSite.ServerId, FolderType.No, RadiniumSASDefinition.RadiniumSASKind);
            ReturnLocker.FQID.ObjectId = Guid.NewGuid();
            return ReturnLocker;
        }

        public override string ToString()
        {
            return Name;
        }

        [SaveInItem]
        [Key]
        [XmlElement("ExtDoorID")]
        public string ExtDoorID { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Name")]
        public string Name { get; set; }
        [Column]
        [XmlElement("BatteryStatus")]
        public int BatteryStatus { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Description")]
        public string Description { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("GPF1")]
        public string GPF1 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("GPF2")]
        public string GPF2 { get; set; }
        [Column]
        [XmlElement("OpenTime")]
        public int? OpenTime { get; set; }
        [Column]
        [XmlElement("OpenTimeADA")]
        public int? OpenTimeADA { get; set; }
        [Column]
        [XmlElement("OpeningMode")]
        public int? OpeningMode { get; set; }
        [Column]
        [XmlElement("TimedPeriodsTableId")]
        public int? TimedPeriodsTableId { get; set; }
        [Column]
        [XmlElement("AutomaticChangesTableId")]
        public int? AutomaticChangesTableId { get; set; }
        [Column]
        [XmlElement("KeypadCode")]
        public string KeypadCode { get; set; }
        [Column]
        [XmlElement("AuditOnKeys")]
        public bool? AuditOnKeys { get; set; }
        [Column]
        [XmlElement("UpdateRequired")]
        public bool? UpdateRequired { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera1")]
        public string Camera1 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera2")]
        public string Camera2 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera3")]
        public string Camera3 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera4")]
        public string Camera4 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("LinkedMap")]
        public string LinkedMap { get; set; }
        [Column]
        [XmlElement("IsFreeAssignment")]
        public bool? IsFreeAssignment { get; set; }
    }
}
