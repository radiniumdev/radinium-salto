﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RadiniumSAS.Data
{


    public class KeyPeriod
    {
        public DateTime StartPeriod { get; set; }
        public DateTime EndPeriod { get; set; }
    }

    public static class SaltoDbUserExtension
    {
        public static void CopyFieldsTo(this SaltoDbUser me, SaltoDbUser otherUser)
        {
            if (otherUser == null)
            {
                return;
            }
            PropertyInfo[] fields = me.GetType().GetProperties();
            foreach (PropertyInfo field in fields)
            {
                if (field.GetCustomAttributes<XmlElementAttribute>().Count() > 0)
                {
                    if (field.GetCustomAttributes<KeyAttribute>().Count() > 0)
                    {
                        continue;
                    }
                    object newValue = field.GetValue(me);
                    if (newValue != null)
                    {
                        field.SetValue(otherUser, newValue);
                    }
                }
            }
        }
    }

    [Serializable]
    [XmlRoot("SaltoDBUser")]
    public class SaltoDbUser
    {

        public override string ToString()
        {
            return Title + " " + LastName + ", " + FirstName;
        }
        public override int GetHashCode()
        {
            if (ExtUserID != null)
            {
                return ExtUserID.GetHashCode();
            }
            else return 0;
        }

        public override bool Equals(object obj)
        {
            SaltoDbUser target = obj as SaltoDbUser;
            if (target == null)
            {
                return false;
            }
            if (ExtUserID != null && target.ExtUserID != null && ExtUserID == target.ExtUserID)
            {
                return true;
            }
            return false;
        }

        [Key]
        [XmlElement("ExtUserID")]
        public string ExtUserID { get; set; }
        [Column]
        [XmlElement("FirstName")]
        public string FirstName { get; set; }
        [Column]
        [XmlElement("LastName")]
        public string LastName { get; set; }
        [Column]
        [XmlElement("Title")]
        public string Title { get; set; }
        [Column]
        [XmlElement("Office")]
        public bool? Office { get; set; }
        [Column]
        [XmlElement("AuditOpenings")]
        public bool? AuditOpenings { get; set; }
        [Column]
        [XmlElement("UseADA")]
        public bool? UseADA { get; set; }
        [Column]
        [XmlElement("UseAntipassback")]
        public bool? UseAntipassback { get; set; }
        [Column]
        [XmlElement("LockdownEnabled")]
        public bool? LockdownEnabled { get; set; }
        [Column]
        [XmlElement("OverrideLockdown")]
        public bool? OverrideLockdown { get; set; }
        [Column]
        [XmlElement("OverridePrivacy")]
        public bool? OverridePrivacy { get; set; }
        [Column]
        [XmlElement("UseLockCalendar")]
        public bool? UseLockCalendar { get; set; }
        [Column]
        [XmlElement("CalendarId")]
        public int? CalendarId { get; set; }
        [Column]
        public string GPF1 { get; set; }
        [Column]
        public string GPF2 { get; set; }
        [Column]
        [XmlElement("GPF3")]
        public string GPF3 { get; set; }
        [Column]
        [XmlElement("GPF4")]
        public string GPF4 { get; set; }
        [Column]
        [XmlElement("GPF5")]
        public string GPF5 { get; set; }
        [Column]
        [XmlElement("AutoKeyEdit.ROMCode")]
        public string AutoKeyEditROMCode { get; set; }
        [Column]
        [XmlElement("CurrentROMCode")]
        public string CurrentROMCode { get; set; }
        [Column]
        [XmlElement("UserActivationDate")]
        public DateTime? UserActivationDate { get; set; }
        [Column]
        [XmlElement("UserExpiration.Enabled")]
        public bool? UserExpirationEnabled { get; set; }
        [Column]
        [XmlElement("UserExpiration")]
        public DateTime? UserExpiration { get; set; }
        [Column]
        [XmlElement("KeyExpirationDifferentFromUserExpiration")]
        public bool? KeyExpirationDifferentFromUserExpiration { get; set; }
        [Column]
        [XmlElement("KeyRevalidation.UnitOfUpdatePeriod")]
        public int? KeyRevalidationUnitOfUpdatePeriod { get; set; }
        [Column]
        [XmlElement("CurrentKeyExists")]
        public bool? CurrentKeyExists { get; set; }
        //[XmlElement("CurrentKeyPeriod")]
        //public KeyPeriod CurrentKeyPeriod;
        [Column]
        [XmlElement("CurrentKeyStatus")]
        public int? CurrentKeyStatus { get; set; }
        [Column]
        [XmlElement("PINEnabled")]
        public bool? PINEnabled { get; set; }
        [Column]
        [XmlElement("PINCode")]
        public string PINCode { get; set; }
        [Column]
        [XmlElement("WiegandCode")]
        public string WiegandCode { get; set; }
        [Column]
        [XmlElement("IsBanned")]
        public bool? IsBanned { get; set; }
        [Column]
        [XmlElement("KeyIsCancellableThroughBlacklist")]
        public bool? KeyIsCancellableThroughBlacklist { get; set; }
        [Column]
        [XmlElement("LocationFunctionTimezoneTableID")]
        public int? LocationFunctionTimezoneTableID { get; set; }
        [Column]
        [XmlElement("MobileAppType")]
        public int? MobileAppType { get; set; }
        [Column]
        [XmlElement("PhoneNumber")]
        public string PhoneNumber { get; set; }
        [Column]
        [XmlElement("ExtLimitedEntryGroupID")]
        public string ExtLimitedEntryGroupID { get; set; }
        [Column]
        public byte[] Photo { get; set; }
    }
}
