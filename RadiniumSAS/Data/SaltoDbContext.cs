﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Common;
using RadiniumSAS.Migrations;
using System.Data.SqlClient;

namespace RadiniumSAS.Data
{
    public class SaltoDbContext : DbContext
    {
        public DbSet<SaltoDbDoor> Doors { get; set; }
        public DbSet<SaltoDbLocker> Lockers { get; set; }
        public DbSet<SaltoDbRoom> HotelRooms { get; set; }
        public DbSet<SaltoDbUser> Users { get; set; }
        public DbSet<SaltoAuditEntry> AuditEntries { get; set; }


        public SaltoDbContext()
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

        public SaltoDbContext(DbConnection existingConnection, bool contextOwnsConnection) : base(existingConnection, contextOwnsConnection)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SaltoDbContext, Configuration>(true));
        }
    }
}
