﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RadiniumSAS.Background;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RadiniumSAS.Data
{
    [Serializable]
    public class SaltoAuditEntrySerialize
    {
        public SaltoAuditEntrySerialize(SaltoAuditEntry sourceEntry)
        {
            this.EventDate = sourceEntry.EventDate;
            this.EventId = sourceEntry.EventId;
            this.InternalEventId = sourceEntry.InternalEventId;
            this.Operation = (int)sourceEntry.Operation;
            this.IsExit = sourceEntry.IsExit;
            this.DoorId = sourceEntry.DoorId;
            this.SubjectType = (int)sourceEntry.SubjectType;
            this.SubjectId = sourceEntry.SubjectId;
            this.SubjectName = sourceEntry.SubjectName;
            this.DoorName = sourceEntry.DoorName;
            this.Camera1Id = sourceEntry.Camera1Id;
            this.Camera2Id = sourceEntry.Camera2Id;
            this.Door = sourceEntry.Door;
            this.Locker = sourceEntry.Locker;
            this.Room = sourceEntry.Room;
            this.User = sourceEntry.User;
        }

        public SaltoAuditEntry SaltoEntry
        {
            get
            {
                SaltoAuditEntry myEntry = new SaltoAuditEntry();
                myEntry.EventDate = EventDate;
                myEntry.EventId = EventId;
                myEntry.InternalEventId = InternalEventId;
                myEntry.IsExit = IsExit;
                myEntry.DoorId = DoorId;
                myEntry.SubjectType = (SaltoSubjectType)SubjectType;
                myEntry.SubjectId = SubjectId;
                myEntry.SubjectName = SubjectName;
                myEntry.DoorName = DoorName;
                myEntry.Camera1Id = Camera1Id;
                myEntry.Camera2Id = Camera2Id;
                myEntry.Door = Door;
                myEntry.Locker = Locker;
                myEntry.Room = Room;
                myEntry.User = User;
                myEntry.Operation = Operation;
                return myEntry;
            }
        }

        public DateTime EventDate { get; set; }
        public int EventId { get; set; }
        public long InternalEventId { get; set; }
        public int Operation { get; set; }
        public bool IsExit { get; set; }
        public string DoorId { get; set; }
        public int SubjectType { get; set; }
        public string SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string DoorName { get; set; }
        public string Camera1Id { get; set; }
        public string Camera2Id { get; set; }
        public SaltoDbDoor Door { get; set; }
        public SaltoDbLocker Locker { get; set; }
        public SaltoDbRoom Room { get; set; }
        public SaltoDbUser User { get; set; }
    }


    [Serializable]
    public class SaltoAuditEntry
    {
        [Column]
        public long InternalEventId { get; set; }
        [Column] 
        public DateTime EventDate { get; set; }
        [Key]
        public int EventId { get; set; }
        [Column] 
        public int Operation { get; set; }
        [Column] 
        public bool IsExit { get; set; }
        [Column] 
        public string DoorId { get; set; }
        [Column] 
        public SaltoSubjectType SubjectType { get; set; }
        [Column] 
        public string SubjectId { get; set; }
        [Column] 
        public string SubjectName { get; set; }
        [Column] 
        public string DoorName { get; set; }
        [Column] 
        public string Camera1Id { get; set; }
        [Column] 
        public string Camera2Id { get; set; }
        public SaltoDbRoom Room { get; set; }
        public SaltoDbLocker Locker { get; set; }
        public SaltoDbDoor Door { get; set; }
        public SaltoDbUser User { get; set; }
        
        public static SaltoAuditEntry ParseFromXmlNode(XmlNode node)
        {
            SaltoAuditEntry entry = new SaltoAuditEntry();
            string eventDate = node.SelectSingleNode("EventDateTime").InnerText;
            int eventId = int.Parse(node.SelectSingleNode("EventID").InnerText);
            int operation = int.Parse(node.SelectSingleNode("Operation").InnerText);
            bool isExit = node.SelectSingleNode("IsExit").InnerText == "1";
            string doorId = node.SelectSingleNode("DoorID").InnerText;
            SaltoSubjectType subjectType = (SaltoSubjectType)Enum.Parse(typeof(SaltoSubjectType), node.SelectSingleNode("SubjectType").InnerText);
            string subjectId = node.SelectSingleNode("SubjectID").InnerText;

            entry.EventDate = DateTime.Parse(eventDate);
            entry.InternalEventId = eventId;
            entry.Operation = operation;
            entry.IsExit = isExit;
            entry.DoorId = doorId;
            entry.SubjectType = subjectType;
            entry.SubjectId = subjectId;

            return entry;
        }
    }
}
