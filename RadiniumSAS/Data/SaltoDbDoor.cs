﻿using RadiniumSAS.Background;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using VideoOS.Platform;

namespace RadiniumSAS.Data
{

    [Serializable]
    [XmlRoot("SaltoDBDoor")]
    public class SaltoDbDoor
    {

        public void CopyFieldsTo(SaltoDbDoor otherDoor)
        {
            if(otherDoor == null)
            {
                return;
            }
            PropertyInfo[] fields = GetType().GetProperties();
            foreach (PropertyInfo field in fields)
            {
                if (field.GetCustomAttributes<XmlElementAttribute>().Count() > 0)
                {
                    if(field.GetCustomAttributes<KeyAttribute>().Count() > 0)
                    {
                        continue;
                    }
                    object newValue = field.GetValue(this);
                    if(newValue != null)
                    {
                        field.SetValue(otherDoor, newValue);
                    }
                }
            }
        }

        public static SaltoDbDoor ConvertItemToDoor(VideoOS.Platform.Item door)
        {
            SaltoDbDoor ReturnDoor = new SaltoDbDoor();
            ReturnDoor.Name = door.Name;
            PropertyInfo[] fields = ReturnDoor.GetType().GetProperties();
            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    if(!door.Properties.ContainsKey(a.ElementName))
                    {
                        continue;
                    }
                    string val = door.Properties[a.ElementName];
                    IEnumerable<MethodInfo> methods = field.PropertyType.GetRuntimeMethods();
                    try
                    {
                        if(field.PropertyType == typeof(string))
                        {
                            field.SetValue(ReturnDoor, val);
                        }
                        if(field.PropertyType == typeof(int?))
                        {
                            field.SetValue(ReturnDoor, int.Parse(val));
                        }
                        if(field.PropertyType == typeof(bool?))
                        {
                            field.SetValue(ReturnDoor, bool.Parse(val));
                        }
                        if(field.PropertyType == typeof(DateTime?))
                        {
                            field.SetValue(ReturnDoor, DateTime.Parse(val));
                        }
                        methods.Where(x => x.Name == "Parse" && x.GetParameters().Count() == 1 && x.GetParameters()[1].ParameterType == typeof(string)).ToList().ForEach(x =>
                        {
                            field.SetValue(ReturnDoor, x.Invoke(field.PropertyType, new object[] { val }));
                        });
                    }
                    catch(Exception)
                    {
                    }
                }
            }

            return ReturnDoor;
        }

        public static void CopyPropertiesToItem(SaltoDbDoor door, Item item)
        {
            PropertyInfo[] fields = item.GetType().GetProperties();

            if(item.Name != door.Name && door.Name != null)
            {
                item.Name = door.Name;
            }

            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    object doorVal = field.GetValue(door);
                    if (doorVal != null)
                    {
                        item.Properties[a.ElementName] = doorVal.ToString();
                    }
                }
            }
        }

        public static VideoOS.Platform.Item ConvertDoorToItem(SaltoDbDoor door)
        {
            VideoOS.Platform.Item ReturnDoor = new VideoOS.Platform.Item();
            PropertyInfo[] fields = door.GetType().GetProperties();

            foreach (PropertyInfo field in fields)
            {
                IEnumerable<XmlElementAttribute> attrArr = field.GetCustomAttributes<XmlElementAttribute>();
                if (attrArr.Count() > 0)
                {
                    XmlElementAttribute a = attrArr.First();
                    object doorVal = field.GetValue(door);
                    if(doorVal != null && a.ElementName != "Name")
                    {
                        ReturnDoor.Properties[a.ElementName] = doorVal.ToString();
                    }
                }
            }
            ReturnDoor.HasChildren = HasChildren.No;
            ReturnDoor.HasRelated = HasRelated.No;
            ReturnDoor.Name = "(Door) " + door.Name;
            ReturnDoor.FQID = new VideoOS.Platform.FQID(EnvironmentManager.Instance.CurrentSite.ServerId, FolderType.No, RadiniumSASDefinition.RadiniumSASKind);
            ReturnDoor.FQID.ObjectId = Guid.NewGuid();
            return ReturnDoor;
        }

        public override string ToString()
        {
            return Name;
        }

        [SaveInItem]
        [Key]
        [XmlElement("ExtDoorID")]
        public string ExtDoorID { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Name")]
        public string Name { get; set; }
        [Column]
        [XmlElement("BatteryStatus")]
        public int BatteryStatus { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Description")]
        public string Description { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("GPF1")]
        public string GPF1 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("GPF2")]
        public string GPF2 { get; set; }
        [Column]
        [XmlElement("OpenTime")]
        public int? OpenTime { get; set; }
        [Column]
        [XmlElement("OpenTimeADA")]
        public int? OpenTimeADA { get; set; }
        [Column]
        [XmlElement("OpeningMode")]
        public int? OpeningMode { get; set; }
        [Column]
        [XmlElement("TimedPeriodsTableId")]
        public int? TimedPeriodsTableId { get; set; }
        [Column]
        [XmlElement("AutomaticChangesTableId")]
        public int? AutomaticChangesTableId { get; set; }
        [Column]
        [XmlElement("KeypadCode")]
        public string KeypadCode { get; set; }
        [Column]
        [XmlElement("AuditOnKeys")]
        public bool? AuditOnKeys { get; set; }
        [Column]
        [XmlElement("AntipassbackEnabled")]
        public bool? AntipassbackEnabled { get; set; }
        [Column]
        [XmlElement("OutwardAntipassback")]
        public bool? OutwardAntipassback { get; set; }
        [Column]
        [XmlElement("UpdateRequired")]
        public bool? UpdateRequired { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera1")]
        public string Camera1 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera2")]
        public string Camera2 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera3")]
        public string Camera3 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("Camera4")]
        public string Camera4 { get; set; }
        [SaveInItem]
        [Column]
        [XmlElement("LinkedMap")]
        public string LinkedMap { get; set; }
    }
}
