﻿using Cygnetic.MIPPluginCommons.Licence;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoOS.Platform;
using VideoOS.Platform.Data;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.RuleAction;
using VideoOS.Platform.Util;

namespace RadiniumSAS
{
    public class RadiniumSASActionManager : ActionManager
    {
        static ActionDefinition openDoor = new ActionDefinition()
        {
            ActionItemKind = new ActionElement() 
            { 
                ItemKinds = new Collection<Guid>() { RadiniumSASDefinition.RadiniumSASKind }, DefaultText = "Door" 
            },
            DescriptionText = "Open SALTO door {0}",
            Id = new Guid("dc79f90f-f32e-448a-9419-3948eecbdf47"),
            Name = "Open door",
            SelectionText = "Open SALTO door"
        };

        static ActionDefinition closeDoor = new ActionDefinition()
        {
            ActionItemKind = new ActionElement() 
            { 
                ItemKinds = new Collection<Guid>() { RadiniumSASDefinition.RadiniumSASKind }, DefaultText = "Door" 
            },
            DescriptionText = "Close SALTO door {0}",
            Id = new Guid("50d9ac6c-509b-4000-a7af-be142bdddc9f"),
            Name = "Close door",
            SelectionText = "Close SALTO door"
        };

        static ActionDefinition evacuate = new ActionDefinition()
        {
            ActionItemKind = new ActionElement()
            {
                ItemKinds = new Collection<Guid>() { RadiniumSASDefinition.EvacuationItemKind }, DefaultText = "Evacuation Zone"
            },
            DescriptionText = "Engage evacuation for zone {0}",
            Id = new Guid("752c424b-98f5-4215-b1c4-45600059bb89"),
            Name = "Evacuate zone",
            SelectionText = "Evacuate zone"
        };

        static ActionDefinition lockdown = new ActionDefinition()
        {
            ActionItemKind = new ActionElement()
            {
                ItemKinds = new Collection<Guid>() { RadiniumSASDefinition.LockdownItemKind },
                DefaultText = "Lockdown Zone"
            },
            DescriptionText = "Engage lockdown for zone {0}",
            Id = new Guid("629003dd-e3ea-4144-a537-1ce7296b4dda"),
            Name = "Lockdown zone",
            SelectionText = "Lockdown zone"
        };

        static ActionDefinition endemergency = new ActionDefinition()
        {
            ActionItemKind = new ActionElement()
            {
                ItemKinds = new Collection<Guid>() { RadiniumSASDefinition.LockdownItemKind, RadiniumSASDefinition.EvacuationItemKind },
                DefaultText = "End emergency mode"
            },
            DescriptionText = "End emergency mode for zone {0}",
            Id = new Guid("bea21fc1-065d-4cee-8a91-e65e38208cd0"),
            Name = "End lockdown/evacuation",
            SelectionText = "End lockdown/evacuation"
        };
        

        private static Collection<ActionDefinition> _actions = new Collection<ActionDefinition>(){ };
        static RadiniumSASActionManager()
        {
            _actions.Add(openDoor);
            _actions.Add(closeDoor);
            _actions.Add(evacuate);
            _actions.Add(lockdown);
            _actions.Add(endemergency);
        }

        private string[] getDoorIds(FQID objectId)
        {
            Item x = Configuration.Instance.GetItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, objectId.Kind, objectId.ObjectId);
            string[] doorObjectIds = x.Properties[RadiniumSASDefinition.RADINIUM_LOCKDOWN_GROUP].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            List<Item> allDoors = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind);

            List<Item> doorList = allDoors.Where(y => doorObjectIds.Contains(y.FQID.ObjectId.ToString())).ToList();

            return doorList.Where(y => y.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID)).Select(y => y.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]).ToArray();
        }

        public override void ExecuteAction(Guid actionId, Collection<FQID> actionItems, BaseEvent sourceEvent)
        {
            int numDoors = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind).Count;
            RadiniumSASLicenceValidator validator = new RadiniumSASLicenceValidator();
            if (!validator.ValidateLicence(new object[] { }))
            {
                EnvironmentManager.Instance.Log(false, "Radinium Intrusion background thread", "Invalid licence");
                return;
            }
            Base64Licence licence = validator.LoadLicence();
            if (licence == null)
            {
                EnvironmentManager.Instance.Log(false, "Radinium Intrusion background thread", "No active licences for intrusion", null);
                return;
            }
            if (licence.ExpiryDateDateTime < DateTime.Now && licence.LicenceType == LicenceType.Grace)
            {
                EnvironmentManager.Instance.Log(false, "Radinium Intrusion background thread", "Licences expired", null);
                return;
            }
            if (licence.NumLicences < numDoors)
            {
                EnvironmentManager.Instance.Log(false, "Radinium Intrusion background thread", "Insufficient active licences for intrusion", null);
                return;
            }
            foreach (FQID actionItem in actionItems)
            {
                string actionCommand = "";
                string userName = "(Rule triggered)";
                if (actionId == openDoor.Id)
                {
                    actionCommand = RadiniumSASDefinition.RADINIUM_MESSAGE_OPEN_DOOR;
                    EnvironmentManager.Instance.PostMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_OPEN_DOOR, actionItem, new string[] { actionCommand, userName }), actionItem, sourceEvent.EventHeader.Source.FQID);
                }
                if (actionId == closeDoor.Id)
                {
                    actionCommand = RadiniumSASDefinition.RADINIUM_MESSAGE_CLOSE_DOOR;
                    EnvironmentManager.Instance.PostMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_CLOSE_DOOR, actionItem, new string[] { actionCommand, userName }), actionItem, sourceEvent.EventHeader.Source.FQID);
                }
                if (actionId == evacuate.Id)
                {
                    string[] doorObjectIds = getDoorIds(actionItem);
                    actionCommand = RadiniumSASDefinition.RADINIUM_MESSAGE_EMERGENCY_OPEN;
                    EnvironmentManager.Instance.PostMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_EMERGENCY_OPEN, actionItem, new string[] { userName }.Concat(doorObjectIds).ToArray()), actionItem, sourceEvent.EventHeader.Source.FQID);
                }
                if (actionId == lockdown.Id)
                {
                    string[] doorObjectIds = getDoorIds(actionItem);
                    actionCommand = RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_CLOSE;
                    EnvironmentManager.Instance.PostMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_CLOSE, actionItem, new string[] { userName }.Concat(doorObjectIds).ToArray()), actionItem, sourceEvent.EventHeader.Source.FQID);
                }
                if (actionId == endemergency.Id)
                {
                    string[] doorObjectIds = getDoorIds(actionItem);
                    actionCommand = RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_END;
                    EnvironmentManager.Instance.PostMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_END, actionItem, new string[] { userName }.Concat(doorObjectIds).ToArray()), actionItem, sourceEvent.EventHeader.Source.FQID);
                }

                EnvironmentManager.Instance.PostMessage(new VideoOS.Platform.Messaging.Message(MessageId.Control.TriggerCommand, actionItem, actionCommand), actionItem, sourceEvent.EventHeader.Source.FQID);
            }
        }

        public override Collection<ActionDefinition> GetActionDefinitions()
        {
            return _actions;
        }
    }
}
