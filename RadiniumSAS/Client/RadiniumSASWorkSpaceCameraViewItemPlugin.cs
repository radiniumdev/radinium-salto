using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using VideoOS.Platform;
using VideoOS.Platform.Client;

namespace RadiniumSAS.Client
{
    public class RadiniumSASWorkSpaceCameraViewItemPlugin : ViewItemPlugin
    {
        private static System.Drawing.Image _treeNodeImage;

        private RadiniumSASWorkSpacePlugin myPlugin;
        public RadiniumSASWorkSpaceCameraViewItemPlugin(RadiniumSASWorkSpacePlugin plugin)
        {
            this.myPlugin = plugin;
            string assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            _treeNodeImage = Properties.Resources.WorkSpaceIcon;
        }

        public override Guid Id
        {
            get { return RadiniumSASDefinition.RadiniumSASWorkSpaceCameraViewItemPluginId; }
        }

        public override System.Drawing.Image Icon
        {
            get { return _treeNodeImage; }
        }

        public override string Name
        {
            get { return "WorkSpace Plugin View Item"; }
        }

        public override bool HideSetupItem
        {
            get
            {
                return false;
            }
        }

        private static RadiniumSASWorkSpaceCameraViewItemManager newestItem = null;

        public override ViewItemManager GenerateViewItemManager()
        {
            newestItem = new RadiniumSASWorkSpaceCameraViewItemManager(myPlugin);
            return newestItem;
        }

        public void SwitchToLiveCameras(FQID camera1, FQID camera2)
        {
            newestItem.SetLiveCameras(camera1, camera2);
        }

        public void SwitchToCamerasOnTime(DateTime timestamp, FQID camera1, FQID camera2)
        {
            newestItem.SetCamerasAtTime(timestamp, camera1, camera2);
        }

        public override void Init()
        {
        }

        public override void Close()
        {
        }


    }
}
