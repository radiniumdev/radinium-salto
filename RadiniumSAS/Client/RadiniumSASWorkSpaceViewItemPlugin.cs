using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using VideoOS.Platform.Client;

namespace RadiniumSAS.Client
{
    public class RadiniumSASWorkSpaceViewItemPlugin : ViewItemPlugin
    {
        private static System.Drawing.Image _treeNodeImage;

        private RadiniumSASWorkSpacePlugin myPlugin;
        public RadiniumSASWorkSpaceViewItemPlugin(RadiniumSASWorkSpacePlugin plugin)
        {
            this.myPlugin = plugin;
            string assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            _treeNodeImage = Properties.Resources.WorkSpaceIcon;
        }

        public override Guid Id
        {
            get { return RadiniumSASDefinition.RadiniumSASWorkSpaceViewItemPluginId; }
        }

        public override System.Drawing.Image Icon
        {
            get { return _treeNodeImage; }
        }

        public override string Name
        {
            get { return "SALTO"; }
        }

        public override bool HideSetupItem
        {
            get
            {
                return false;
            }
        }

        public override ViewItemManager GenerateViewItemManager()
        {
            return new RadiniumSASWorkSpaceViewItemManager(myPlugin);
        }

        public override void Init()
        {
        }

        public override void Close()
        {
        }


    }
}
