using VideoOS.Platform.Client;

namespace RadiniumSAS.Client
{
    public class RadiniumSASWorkSpaceViewItemManager : ViewItemManager
    {
        public RadiniumSASWorkSpaceViewItemManager(RadiniumSASWorkSpacePlugin parentPlugin) : base("RadiniumSASWorkSpaceViewItemManager")
        {
            this.parentPlugin = parentPlugin;
        }

        RadiniumSASWorkSpacePlugin parentPlugin = null;

        public override ViewItemWpfUserControl GenerateViewItemWpfUserControl()
        {
            RadiniumSASWorkSpaceViewItemWpfUserControl myControl = new RadiniumSASWorkSpaceViewItemWpfUserControl();
            myControl.SetParentPlugin(parentPlugin);
            return myControl;
        }
    }
}
