﻿using RadiniumSAS.Background;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;

namespace RadiniumSAS.Client
{
    /// <summary>
    /// Interaction logic for RadiniumSASEmergencyForm.xaml
    /// </summary>
    public partial class RadiniumSASEmergencyForm : UserControl
    {

        public class ItemCheck
        {
            public string ItemName { get; set; }
            public string ItemId { get; set; }
            public bool ItemChecked { get; set; }
            public override string ToString()
            {
                return ItemName;
            }
        }
        private MessageCommunication comms;


        List<ItemCheck> ListOfAllLockdownsEvacuate, ListOfAllLockdownsLockdown;
        List<Item> allDoors = null;
        Dictionary<string, string[]> LockdownToItemMap = new Dictionary<string, string[]>();
        public CurrentEmergencyState target = CurrentEmergencyState.NO_EMERGENCY;

        public RadiniumSASEmergencyForm(MessageCommunication comms, CurrentEmergencyState emergencyActive, string[] doorIds, CurrentEmergencyState targetState)
        {
            this.doorIds = doorIds;
            InitializeComponent();
            LockdownImage.Visibility = Visibility.Hidden;
            EvacuationImage.Visibility = Visibility.Hidden;
            target = targetState;
            if (targetState == CurrentEmergencyState.EVACUATION)
            {
                EvacuationImage.Visibility = Visibility.Visible;
                EvacuationTextBlock.Text = "Select Emergency Evacuation Zones:";
                EmergencyWarningTextBlock.Content = "You are about to activate an emergency evacuation.\r\nWhilst active, all indicated doors in the evacuation zone will be in an\r\nemergency open state.";
            }
            if (targetState == CurrentEmergencyState.LOCKDOWN)
            {
                LockdownImage.Visibility = Visibility.Visible;
                EvacuationTextBlock.Text = "Select Emergency Lockdown Zones:";
                EmergencyWarningTextBlock.Content = "You are about to activate a security lockdown.\r\nWhilst active, all doors will be locked and will only be accessible with a\r\nhigh clearance security card.";
            }
            if(targetState == CurrentEmergencyState.NO_EMERGENCY)
            {
                this.Height = 50 + 80 + 70;
                RowRemoving.Height = new GridLength(0);
                EmergencyEvacuationButton.Visibility = Visibility.Hidden;
                EmergencyWarningTextBlock.Content = "You are about to cancel the emergency state.";
                EmergencyWarningTextBlock.Height = 80;
            }
            List<Item> allLockdowns = new List<Item>();
            if (targetState == CurrentEmergencyState.LOCKDOWN)
            {
                allLockdowns = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.LockdownItemKind);
            }
            if(targetState == CurrentEmergencyState.EVACUATION)
            {
                allLockdowns = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.EvacuationItemKind);
            }
            allDoors = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind);
            List<ItemCheck> ListOfAllLockdowns = new List<ItemCheck>();

            allLockdowns.ForEach(x =>
            {
                if (!x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_LOCKDOWN_GROUP))
                {
                    return;
                }
                string[] doorObjectIds = x.Properties[RadiniumSASDefinition.RADINIUM_LOCKDOWN_GROUP].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                List<Item> doorList = allDoors.Where(y => doorObjectIds.Contains(y.FQID.ObjectId.ToString())).ToList();
                string[] extDoorIds = doorList.Where(y => y.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID)).Select(y => y.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]).ToArray();
                LockdownToItemMap.Add(x.FQID.ObjectId.ToString(), extDoorIds);
            });

            ListOfAllLockdownsEvacuate = ListOfAllLockdowns.Concat(allLockdowns.Select(x => new ItemCheck() { ItemName = x.Name, ItemId = x.FQID.ObjectId.ToString(), ItemChecked = false }).OrderBy(x => x.ToString())).ToList();
            ListOfAllLockdownsLockdown = ListOfAllLockdowns.Concat(allLockdowns.Select(x => new ItemCheck() { ItemName = x.Name, ItemId = x.FQID.ObjectId.ToString(), ItemChecked = false }).OrderBy(x => x.ToString())).ToList();

            if(target == CurrentEmergencyState.EVACUATION)
            {
                EvacuationSelection.ItemsSource = ListOfAllLockdownsEvacuate;
            }
            else
            {
                EvacuationSelection.ItemsSource = ListOfAllLockdownsLockdown;
            }

            this.EmergencyActive = emergencyActive;
            if(emergencyActive != CurrentEmergencyState.NO_EMERGENCY)
            {
                EmergencyEvacuationButton.IsEnabled = false;
                EmergencyActivateButton.IsEnabled = false;
            }
            else
            {
                EmergencyEvacuationButton.IsEnabled = true;
                EmergencyActivateButton.IsEnabled = true;
            }
            this.comms = comms;
        }

        public CurrentEmergencyState EmergencyActive = CurrentEmergencyState.NO_EMERGENCY;
        public string[] doorIds = new string[0];

        private void EmergencyDeactivateButton_Click(object sender, RoutedEventArgs e)
        {
            string[] userName = new string[] { VideoOS.Platform.EnvironmentManager.Instance.LoginNetworkCredential.UserName };
            comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_END, userName.Concat(doorIds).ToArray()), EnvironmentManager.Instance.CurrentSite, null, null);
            EmergencyActive = CurrentEmergencyState.NO_EMERGENCY;

            Window.GetWindow(this).Close();
        }

        private void EmergencyActivateButton_Click(object sender, RoutedEventArgs e)
        {
            string[] userName = new string[] { VideoOS.Platform.EnvironmentManager.Instance.LoginNetworkCredential.UserName };
            if(target == CurrentEmergencyState.EVACUATION)
            {
                doorIds = ListOfAllLockdownsEvacuate.Where(x => x.ItemChecked).Select(y => LockdownToItemMap[y.ItemId]).SelectMany(y => y).Distinct().ToArray();
            }
            else
            {
                doorIds = ListOfAllLockdownsLockdown.Where(x => x.ItemChecked).Select(y => LockdownToItemMap[y.ItemId]).SelectMany(y => y).Distinct().ToArray();
            }
            if(target == CurrentEmergencyState.EVACUATION)
            {
                comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_EMERGENCY_OPEN, userName.Concat(doorIds).ToArray()), EnvironmentManager.Instance.CurrentSite, null, null);
                EmergencyActive = CurrentEmergencyState.EVACUATION;
            }
            if(target == CurrentEmergencyState.LOCKDOWN)
            {
                comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_LOCKDOWN_CLOSE, userName.Concat(doorIds).ToArray()), EnvironmentManager.Instance.CurrentSite, null, null);
                EmergencyActive = CurrentEmergencyState.LOCKDOWN;
            }
            Window.GetWindow(this).Close();
        }

        private Brush foregroundSelected = new SolidColorBrush(Color.FromRgb(255,0,0));
        private Brush foregroundUnselected = null;
        private Brush backgroundSelected = new SolidColorBrush(Color.FromRgb(70, 0, 0));
        private Brush backgroundUnselected = null;

        private void lstBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ListBox senderBox = sender as System.Windows.Controls.ListBox;
            foreach (object item in e.AddedItems)
            {
                if (item as ItemCheck == null)
                {
                    continue;
                }
                (item as ItemCheck).ItemChecked = true;
            }
            foreach (object item in e.RemovedItems)
            {
                if (item as ItemCheck == null)
                {
                    continue;
                }
                (item as ItemCheck).ItemChecked = false;
            }
            senderBox.Items.Refresh();
        }

        private void EmergencyEvacuationButton_Checked(object sender, RoutedEventArgs e)
        {
            if (foregroundUnselected == null)
            {
                foregroundUnselected = EvacuationTextBlock.Foreground;
                backgroundUnselected = EmergencyEvacuationButton.Background;
            }
            EvacuationTextBlock.Foreground = foregroundSelected;
            EvacuationSelection.Foreground = foregroundSelected;
            EmergencyEvacuationButton.Background = backgroundSelected;
        }

        private void chkDoorsSelected_Click(object sender, RoutedEventArgs e)
        {

            if (sender as System.Windows.Controls.CheckBox == null)
            {
                return;
            }
            System.Windows.Controls.CheckBox box = sender as System.Windows.Controls.CheckBox;
            System.Windows.Controls.ListBoxItem list = (box.TemplatedParent as ContentPresenter).TemplatedParent as ListBoxItem;
            if (list == null)
            {
                return;
            }
            string id = box.CommandParameter as string;
            if (id == null)
            {
                return;
            }
            ItemCheck item = box.DataContext as ItemCheck;
            ItemCheck otherItem = null;
            if(target == CurrentEmergencyState.EVACUATION)
            {
                otherItem = ListOfAllLockdownsEvacuate.FirstOrDefault(x => x == item);
            }
            else
            {
                otherItem = ListOfAllLockdownsLockdown.FirstOrDefault(x => x == item);
            }
            if(target == CurrentEmergencyState.NO_EMERGENCY)
            {
                return;
            }
            
            if (otherItem != null)
            {
                if (item.ItemChecked)
                {
                    (EvacuationSelection.Template.FindName("lstBox", EvacuationSelection) as System.Windows.Controls.ListBox).SelectedItems.Add(otherItem);
                }
                else
                {
                    (EvacuationSelection.Template.FindName("lstBox", EvacuationSelection) as System.Windows.Controls.ListBox).SelectedItems.Remove(otherItem);
                }
                return;
            }
        }

        private void lstBox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void EmergencyEvacuationButton_Unchecked(object sender, RoutedEventArgs e)
        {

            if(ListOfAllLockdownsEvacuate.Any(y => y.ItemChecked))
            {
                EmergencyEvacuationButton.IsChecked = true;
            }

            if (foregroundUnselected != null)
            {
                EvacuationTextBlock.Foreground = foregroundUnselected;
                EvacuationSelection.Foreground = foregroundUnselected;
                EmergencyEvacuationButton.Background = backgroundUnselected;
            }
        }

    }
}
