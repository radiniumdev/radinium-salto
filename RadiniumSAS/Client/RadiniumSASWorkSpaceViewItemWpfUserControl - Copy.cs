using RadiniumSAS.Background;
using RadiniumSAS.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.Messaging;

namespace RadiniumSAS.Client
{
    /// <summary>
    /// Interaction logic for RadiniumSASWorkSpaceViewItemWpfUserControl.xaml
    /// </summary>

    public partial class RadiniumSASWorkSpaceViewItemWpfCameraUserControl : ViewItemWpfUserControl
    {
        public RadiniumSASWorkSpaceViewItemWpfCameraUserControl()
        {
        }

        RadiniumSASWorkSpacePlugin parentPlugin = null;
        PlaybackWpfUserControl playbackControl = null;
        ImageViewerWpfControl imageViewer = null, imageViewer2 = null;
        VideoControl vc1 = null, vc2 = null;
        
        public void SetParentPlugin(RadiniumSASWorkSpacePlugin plugin)
        {
            parentPlugin = plugin;
        }

        public override void Init()
        {
            InitializeComponent();
            myPlayback = ClientControl.Instance.GeneratePlaybackController();
            imageViewer = new ImageViewerWpfControl();
            imageViewer2 = new ImageViewerWpfControl();
            imageViewer.PlaybackControllerFQID = myPlayback;
            imageViewer2.PlaybackControllerFQID = myPlayback;
            playbackControl = new PlaybackWpfUserControl();
            playbackControl.Init(myPlayback);
            MyGridPlayback.Children.Add(playbackControl);
            vc1 = ClientControl.Instance.GenerateVideoControl();
            vc1.Live = true;
            vc2 = ClientControl.Instance.GenerateVideoControl();
            vc2.Live = true;
        }

        public override void Close()
        {
        }

        private FQID myPlayback;

        public void SwitchToLiveCameras(FQID camera1, FQID camera2)
        {
            MyGrid1.Children.Clear();
            MyGrid2.Children.Clear();
            try
            {
                imageViewer.Disconnect();
            }
            catch (Exception)
            {

            }

            try
            {
                imageViewer2.Disconnect();
            }
            catch (Exception)
            {

            }
            if(camera1 != null && vc1 != null)
            {
                vc1.Cameras = new List<FQID>() { camera1 };
                vc1.SelectedCamera = camera1;
            }
            else if (vc1 != null)
            {
                vc1.Cameras = null;
            }
            if (camera2 != null && vc2 != null)
            {
                vc2.Cameras = new List<FQID>() { camera2 };
                vc2.SelectedCamera = camera2;
            }
            else if(vc2 != null)
            {
                vc2.Cameras = null;
            }


            MyGrid1.Children.Add(vc1);
            MyGrid2.Children.Add(vc2);

            playbackControl.Visibility = Visibility.Hidden;
            //playbackControl.SetCameras(cameraList);
        }
        
        public void SwitchToCamerasOnTime(DateTime timestamp, FQID camera1, FQID camera2)
        {
            if(MyGrid1 == null || MyGrid2 == null || playbackControl == null || imageViewer == null || imageViewer2 == null)
            {
                return;
            }
            MyGrid1.Children.Clear();
            MyGrid2.Children.Clear();
            MyGrid1.Children.Add(imageViewer);
            MyGrid2.Children.Add(imageViewer2);
            playbackControl.Visibility = Visibility.Visible;
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(MessageId.SmartClient.PlaybackCommand, new PlaybackCommandData() { Command = PlaybackData.PlayStop }), myPlayback);
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(MessageId.SmartClient.PlaybackCommand, new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = timestamp.ToLocalTime() }), myPlayback);
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(MessageId.SmartClient.PlaybackCommand, new PlaybackCommandData() { Command = PlaybackData.Play, DateTime = timestamp.ToLocalTime() }), myPlayback);

            List<FQID> cameraList = new List<FQID>();
            try
            {
                imageViewer.Disconnect();
            }
            catch (Exception)
            {

            }

            imageViewer.SuppressUpdateOnMotionOnly = false;
            imageViewer2.SuppressUpdateOnMotionOnly = false;
            imageViewer.PlaybackControllerFQID = myPlayback;

            if (camera1 != null)
            {
                imageViewer.CameraFQID = camera1;
                cameraList.Add(camera1);
                imageViewer.Initialize();
                imageViewer.Connect();
                imageViewer.StartBrowse();
            }
            try
            {
                imageViewer2.Disconnect();
            }
            catch(Exception)
            {

            }

            imageViewer2.PlaybackControllerFQID = myPlayback;

            if (camera2 != null)
            {
                cameraList.Add(camera2);
                imageViewer2.CameraFQID = camera2;
                imageViewer2.Initialize();
                imageViewer2.Connect();
                imageViewer2.StartBrowse();
            }
            playbackControl.SetCameras(cameraList);
        }

        /// <summary>
        /// Do not show the sliding toolbar!
        /// </summary>
        /// 
        public override bool ShowToolbar
        {
            get { return false; }
        }
    }
}
