using System;
using VideoOS.Platform;
using VideoOS.Platform.Client;

namespace RadiniumSAS.Client
{
    public class RadiniumSASWorkSpaceCameraViewItemManager : ViewItemManager
    {
        public RadiniumSASWorkSpaceCameraViewItemManager(RadiniumSASWorkSpacePlugin parentPlugin) : base("RadiniumSASWorkSpaceCameraViewItemManager")
        {
            this.parentPlugin = parentPlugin;
        }

        RadiniumSASWorkSpacePlugin parentPlugin = null;
        RadiniumSASWorkSpaceViewItemWpfCameraUserControl latestControl = null;

        public void SetLiveCameras(FQID camera1, FQID camera2)
        {
            latestControl.SwitchToLiveCameras(camera1, camera2);
        }

        public void SetCamerasAtTime(DateTime timestamp, FQID camera1, FQID camera2)
        {
            latestControl.SwitchToCamerasOnTime(timestamp, camera1, camera2);
        }

        public override ViewItemWpfUserControl GenerateViewItemWpfUserControl()
        {
            latestControl = new RadiniumSASWorkSpaceViewItemWpfCameraUserControl();
            latestControl.SetParentPlugin(parentPlugin);
            return latestControl;
        }
    }
}
