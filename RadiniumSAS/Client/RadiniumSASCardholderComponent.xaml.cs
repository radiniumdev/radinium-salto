﻿using RadiniumSAS.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using VideoOS.Platform;

namespace RadiniumSAS.Client
{
    /// <summary>
    /// Interaction logic for RadiniumSASCardholderComponent.xaml
    /// </summary>
    public partial class RadiniumSASCardholderComponent : UserControl
    {
        XmlSerializer userSerializer = new XmlSerializer(typeof(SaltoDbUser));
        class Cardholder
        {
            public BitmapSource picture { get; set; }
            public string status { get; set; }
            public string date { get; set; }
            public string name { get; set; }
            public string number { get; set; }
        }
        public RadiniumSASCardholderComponent()
        {
            InitializeComponent();

            try
            {

                XmlNode rootNode = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.UserSettingsGuid, false);

                if (rootNode != null)
                {
                    XDocument newDoc = XDocument.Parse(rootNode.OuterXml);
                    List<SaltoDbUser> parsedUsers;
                    List<Cardholder> cardholders = new List<Cardholder>();
                    parsedUsers = newDoc.Descendants("SaltoDbUser").Select(x => userSerializer.Deserialize(new StringReader(x.ToString())) as SaltoDbUser).ToList();

                    foreach (var user in parsedUsers)
                    {
                        var source = BitmapSource.Create(2, 2, 300, 300, PixelFormats.Indexed8, BitmapPalettes.Gray256, user.Photo, 2);
                        cardholders.Add(new Cardholder { picture = source , status = (user.CurrentKeyStatus == 0 ? "ACCESS DENIED" : "ACCESS GRANTED"), date = DateTime.Now.ToString(), name = user.FirstName + " " + user.LastName, number = user.PhoneNumber });
                    }

                    cardholderListView.ItemsSource = cardholders;
                }

                Configuration.Instance.RefreshConfiguration(RadiniumSASDefinition.RadiniumSASKind);
                Thread.Sleep(60000);
            }
            catch (Exception)
            {
                Thread.Sleep(10000);
            }
        }

        private void CardholderCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
