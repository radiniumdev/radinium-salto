﻿using PdfRpt.Core.Contracts;
using PdfRpt.FluentInterface;
using RadiniumSAS.Admin;
using RadiniumSAS.Background;
using RadiniumSAS.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;

namespace RadiniumSAS.Client
{

    /// <summary>
    /// Interaction logic for RadiniumSASReportForm.xaml
    /// </summary>
    public partial class RadiniumSASReportForm : System.Windows.Controls.UserControl
    {
        static List<SaltoDbUser> currentUsers = null;
        static List<SaltoDbDoor> currentDoors = null;
        static List<SaltoDbLocker> currentLockers = null;
        static List<SaltoDbRoom> currentRooms = null;

        public enum CurrentListSelection
        {
            None,
            Cardholders,
            Doors,
            EventTypes
        }

        public static CurrentListSelection CurrentSelection = CurrentListSelection.None;

        private MessageCommunication comms;

        private volatile object commsFilterItems = null, commsFilterReport = null;

        XmlSerializer doorSerializer = new XmlSerializer(typeof(SaltoDbDoor));
        XmlSerializer lockerSerializer = new XmlSerializer(typeof(SaltoDbLocker));
        XmlSerializer roomSerializer = new XmlSerializer(typeof(SaltoDbRoom));
        XmlSerializer userSerializer = new XmlSerializer(typeof(SaltoDbUser));

        private List<ItemCheck> ListOfAllCardholders = new List<ItemCheck>();
        private List<ItemCheck> ListOfAllDoors = new List<ItemCheck>();
        private List<ItemCheck> ListOfAllEvents = new List<ItemCheck>();

        public RadiniumSASReportForm(MessageCommunication comms)
        {
            CurrentSelection = CurrentListSelection.None;
            currentUsers = null;
            currentDoors = null;
            currentLockers = null;
            currentRooms = null;
            InitializeComponent();
            this.comms = comms;
            Array arr = Enum.GetValues(typeof(SaltoOperation));
            for (int i = 0; i < arr.Length; i++)
            {
                enumPairs[(int)arr.GetValue(i)] = ((SaltoOperation)arr.GetValue(i)).ToDescriptionString();
            }

            ListOfAllEvents = enumPairs.Select(x => new ItemCheck() { ItemChecked = false, ItemId = x.Key.ToString(), ItemName = x.Value }).OrderBy(x => x.ItemName.ToLowerInvariant()).ToList();
            ReportEventsCombo.ItemsSource = ListOfAllEvents;

            if (commsFilterItems == null)
            {
                commsFilterItems = comms.RegisterCommunicationFilter(MessageReceiver, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_RESPONSE_ITEMS));
            }
            if (commsFilterReport == null)
            { 
                commsFilterReport = comms.RegisterCommunicationFilter(ReportReceiver, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_RESPONSE_REPORT));
            }
            Thread.Sleep(4000);
            comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_ITEMS), EnvironmentManager.Instance.CurrentSite, null, null);
            //Clone the two lists
            FromDate.SelectedDate = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
            ToDate.SelectedDate = DateTime.Now.EndOfDay();
        }

        ~RadiniumSASReportForm()
        {
            if(commsFilterItems != null)
            {
                comms.UnRegisterCommunicationFilter(commsFilterItems);
            }
            if(commsFilterReport != null)
            {
                comms.UnRegisterCommunicationFilter(commsFilterReport);
            }
            commsFilterItems = null;
            commsFilterReport = null;
        }



        public object MessageReceiver(VideoOS.Platform.Messaging.Message message, FQID destination, FQID sender)
        {
            object firstResponse = message.Data;
            if (firstResponse != null)
            {
                object[] objects = firstResponse as object[];
                if (objects != null)
                {
                    List<string> users = objects[0] as List<string>;
                    List<string> doors = objects[1] as List<string>;
                    List<string> lockers = objects[2] as List<string>;
                    List<string> rooms = objects[3] as List<string>;
                    List<SaltoDbUser> newUsers = new List<SaltoDbUser>();
                    List<SaltoDbDoor> newDoors = new List<SaltoDbDoor>();
                    List<SaltoDbLocker> newLockers = new List<SaltoDbLocker>();
                    List<SaltoDbRoom> newRooms = new List<SaltoDbRoom>();
                    users.ForEach(x => {
                        newUsers.Add(userSerializer.Deserialize(new StringReader(x)) as SaltoDbUser);
                    });
                    doors.ForEach(x => {
                        newDoors.Add(doorSerializer.Deserialize(new StringReader(x)) as SaltoDbDoor);
                    });
                    lockers.ForEach(x => {
                        newLockers.Add(lockerSerializer.Deserialize(new StringReader(x)) as SaltoDbLocker);
                    });
                    rooms.ForEach(x => {
                        newRooms.Add(roomSerializer.Deserialize(new StringReader(x)) as SaltoDbRoom);
                    });
                    currentUsers = newUsers;
                    currentDoors = newDoors;
                    currentLockers = newLockers;
                    currentRooms = newRooms;
                    ListOfAllCardholders = currentUsers.OrderBy(x => x.ToString().ToLowerInvariant()).Select(x => new ItemCheck() { ItemChecked = false, ItemId = x.ExtUserID, ItemName = x.ToString() }).ToList();
                    IEnumerable<ItemCheck> doorItems = currentDoors.OrderBy(x => x.Name.ToLowerInvariant()).Select(x => new ItemCheck() { ItemChecked = false, ItemId = x.ExtDoorID, ItemName = "(Door) " + x.ToString() });
                    IEnumerable<ItemCheck> lockerItems = currentLockers.OrderBy(x => x.Name.ToLowerInvariant()).Select(x => new ItemCheck() { ItemChecked = false, ItemId = x.ExtDoorID, ItemName = "(Locker) " + x.ToString() });
                    IEnumerable<ItemCheck> roomItems = currentRooms.OrderBy(x => x.Name.ToLowerInvariant()).Select(x => new ItemCheck() { ItemChecked = false, ItemId = x.ExtDoorID, ItemName = "(Room) " + x.ToString() });
                    ListOfAllDoors = doorItems.Concat(lockerItems).Concat(roomItems).OrderBy(x => x.ItemName.ToLowerInvariant()).ToList();

                    Dispatcher.Invoke(() => {
                        ReportDoorCombo.ItemsSource = ListOfAllDoors;
                        ReportCardholderCombo.ItemsSource = ListOfAllCardholders;
                    });
                    if (currentUsers.Count == 0 && currentDoors.Count == 0)
                    {
                        Thread.Sleep(1000);
                        comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_ITEMS), EnvironmentManager.Instance.CurrentSite, null, null);
                    }
                }
            }

            Dispatcher.Invoke(() =>
            {
                //LoadingImage.Visibility = Visibility.Hidden;
            });
            return null;
        }
        System.Timers.Timer timer = null;

        public object ReportReceiver(VideoOS.Platform.Messaging.Message message, FQID destination, FQID sender)
        {
            byte[] bytesData = message.Data as byte[]; 
            Dispatcher.Invoke(() => {
                if (!GenerateReport.IsEnabled)
                {
                    if (timer != null)
                    {
                        timer.Enabled = false;
                    }
                    GenerateReport.IsEnabled = true;
                    if (bytesData != null && bytesData.Length > 0)
                    {
                        SaveFileDialog saveFileDialog = new SaveFileDialog();
                        saveFileDialog.Filter = "PDF document file (*.pdf)|*.pdf";
                        saveFileDialog.Title = "Save report";
                        saveFileDialog.DefaultExt = "pdf";
                        saveFileDialog.CheckPathExists = true;
                        if (saveFileDialog.ShowDialog() == DialogResult.OK)
                        {
                            File.WriteAllBytes(saveFileDialog.FileName, bytesData);
                            System.Windows.Forms.MessageBox.Show("Report generated and saved");
                        }
                        Process.Start(saveFileDialog.FileName);
                    }
                    else
                    {
                        string messageString = message.Data as string;
                        if (messageString != null)
                        {
                            System.Windows.MessageBox.Show("Could not retrieve report! Error: " + messageString);
                        }
                        else
                        {
                            System.Windows.MessageBox.Show("Could not retrieve report!");
                        }
                    }
                    Window.GetWindow(this).Close();
                }
            });
            comms.UnRegisterCommunicationFilter(commsFilterItems);
            comms.UnRegisterCommunicationFilter(commsFilterReport);
            commsFilterItems = null;
            commsFilterReport = null;
            return null;
        }

        public class AuditTraceItem
        {
            public string EventDate { get; set; }
            public string AuditEvent { get; set; }
            public string UserName { get; set; }
            public string DoorName { get; set; }
            public string Exit { get; set; }
        }


        Dictionary<int, string> enumPairs = new Dictionary<int, string>();

        private void CancelReportButton_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        private void ClearReportFilterButton_Click(object sender, RoutedEventArgs e)
        {
            ListOfAllCardholders.ForEach(x => { x.ItemChecked = false; });
            ListOfAllDoors.ForEach(x => { x.ItemChecked = false; });
            ListOfAllEvents.ForEach(x => { x.ItemChecked = false; });
            (ReportCardholderCombo.Template.FindName("lstBox", ReportCardholderCombo) as System.Windows.Controls.ListBox).UnselectAll();
            (ReportDoorCombo.Template.FindName("lstBox", ReportDoorCombo) as System.Windows.Controls.ListBox).UnselectAll();
            (ReportEventsCombo.Template.FindName("lstBox", ReportEventsCombo) as System.Windows.Controls.ListBox).UnselectAll();
            (ReportCardholderCombo.Template.FindName("lstBox", ReportCardholderCombo) as System.Windows.Controls.ListBox).Items.Refresh();
            (ReportDoorCombo.Template.FindName("lstBox", ReportDoorCombo) as System.Windows.Controls.ListBox).Items.Refresh();
            (ReportEventsCombo.Template.FindName("lstBox", ReportEventsCombo) as System.Windows.Controls.ListBox).Items.Refresh();
        }

        private void AllCardholder_CheckedAndUnchecked(object sender, RoutedEventArgs e)
        {
            ReportCardholderCombo.Text = string.Join(", ", ListOfAllCardholders.Where(x => x.ItemChecked).Select(x => x.ItemName));
        }

        private void AllDoor_CheckedAndUnchecked(object sender, RoutedEventArgs e)
        {
            ReportDoorCombo.Text = string.Join(", ", ListOfAllDoors.Where(x => x.ItemChecked).Select(x => x.ItemName));
        }

        private void AllEvents_CheckedAndUnchecked(object sender, RoutedEventArgs e)
        {
            ReportEventsCombo.Text = string.Join(", ", ListOfAllEvents.Where(x => x.ItemChecked).Select(x => x.ItemName));
        }

        private void GenerateReport_Click(object sender, RoutedEventArgs e)
        {
            //LoadingImage.Visibility = Visibility.Visible;
            ReportRequestObject reportRequest = new ReportRequestObject();
            reportRequest.FromDate = (DateTime)(FromDate.SelectedDate != null ? FromDate.SelectedDate.GetValueOrDefault().StartOfDay() : DateTime.UtcNow.StartOfWeek(DayOfWeek.Monday));
            reportRequest.ToDate = (DateTime)(ToDate.SelectedDate != null ? ToDate.SelectedDate.GetValueOrDefault() : DateTime.UtcNow).EndOfDay();
            reportRequest.selectedDoors = ListOfAllDoors.Where(x => x.ItemChecked).Select(x => currentDoors.FirstOrDefault(y => y.ExtDoorID == x.ItemId)).Where(x => x != null).ToList();
            reportRequest.selectedLockers = ListOfAllDoors.Where(x => x.ItemChecked).Select(x => currentLockers.FirstOrDefault(y => y.ExtDoorID == x.ItemId)).Where(x => x != null).ToList();
            reportRequest.selectedRooms = ListOfAllDoors.Where(x => x.ItemChecked).Select(x => currentRooms.FirstOrDefault(y => y.ExtDoorID == x.ItemId)).Where(x => x != null).ToList();
            reportRequest.selectedEventTypes = ListOfAllEvents.Where(x => x.ItemChecked).Select(x => int.Parse(x.ItemId)).ToList();
            reportRequest.selectedUsers = ListOfAllCardholders.Where(x => x.ItemChecked).Select(x => currentUsers.FirstOrDefault(y => y.ExtUserID == x.ItemId)).Where(x => x != null).ToList();
            if(reportRequest.selectedUsers.Count > 0)
            {
                reportRequest.CardholderFilterIsChecked = true;
            }
            if(reportRequest.selectedDoors.Count > 0 || reportRequest.selectedLockers.Count > 0 || reportRequest.selectedRooms.Count > 0)
            {
                reportRequest.DoorFilterIsChecked = true;
            }
            if(reportRequest.selectedEventTypes.Count > 0)
            {
                reportRequest.EventTypeFilterIsChecked = true;
            }

            comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_REPORT, reportRequest), EnvironmentManager.Instance.CurrentSite, null, null);
            GenerateReport.IsEnabled = false;
            timer = new System.Timers.Timer() {  AutoReset = false, Enabled = false, Interval = 60000 };
            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;
        }

        private void lstBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ListBox senderBox = sender as System.Windows.Controls.ListBox;
            foreach(object item in e.AddedItems)
            {
                if(item as ItemCheck == null)
                {
                    continue;
                }
                (item as ItemCheck).ItemChecked = true;
            }
            foreach(object item in e.RemovedItems)
            {
                if (item as ItemCheck == null)
                {
                    continue;
                }
                (item as ItemCheck).ItemChecked = false;
            }
            senderBox.Items.Refresh();
        }

        private void chkDoorsSelected_Click(object sender, RoutedEventArgs e)
        {
            if(sender as System.Windows.Controls.CheckBox == null)
            {
                return;
            }
            System.Windows.Controls.CheckBox box = sender as System.Windows.Controls.CheckBox;
            System.Windows.Controls.ListBoxItem list = (box.TemplatedParent as ContentPresenter).TemplatedParent as ListBoxItem;
            if(list == null)
            {
                return;
            }
            string id = box.CommandParameter as string;
            if(id == null)
            {
                return;
            }
            ItemCheck item = box.DataContext as ItemCheck;
            ItemCheck otherItem = ListOfAllCardholders.FirstOrDefault(x => x == item);
            if (otherItem != null)
            {
                if(item.ItemChecked)
                {
                    (ReportCardholderCombo.Template.FindName("lstBox", ReportCardholderCombo) as System.Windows.Controls.ListBox).SelectedItems.Add(otherItem);
                }
                else
                {
                    (ReportCardholderCombo.Template.FindName("lstBox", ReportCardholderCombo) as System.Windows.Controls.ListBox).SelectedItems.Remove(otherItem);
                }
                return;
            }
            if (otherItem == null)
            {
                otherItem = ListOfAllDoors.FirstOrDefault(x => x == item);
            }
            if (otherItem != null)
            {
                if(item.ItemChecked)
                {
                    (ReportDoorCombo.Template.FindName("lstBox", ReportDoorCombo) as System.Windows.Controls.ListBox).SelectedItems.Add(otherItem);
                }
                else
                {
                    (ReportDoorCombo.Template.FindName("lstBox", ReportDoorCombo) as System.Windows.Controls.ListBox).SelectedItems.Remove(otherItem);
                }
                return;
            }
            if (otherItem == null)
            {
                otherItem = ListOfAllEvents.FirstOrDefault(x => x == item);
            }
            if(otherItem != null)
            {
                if(item.ItemChecked)
                {
                    (ReportEventsCombo.Template.FindName("lstBox", ReportEventsCombo) as System.Windows.Controls.ListBox).SelectedItems.Add(otherItem);
                }
                else
                {
                    (ReportEventsCombo.Template.FindName("lstBox", ReportEventsCombo) as System.Windows.Controls.ListBox).SelectedItems.Remove(otherItem);
                }
                return;
            }
        }

        private void lstBox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Dispatcher.Invoke(() => {
                System.Windows.MessageBox.Show("Could not retrieve report, the server took too long.", "Report retrieval error!");
                Window.GetWindow(this).Close();
            });
        }
    }

    public class ItemCheck
    {
        public string ItemName { get; set; }
        public string ItemId { get; set; }
        public bool ItemChecked { get; set; }
        public override string ToString()
        {
            return ItemName;
        }
    }

}
