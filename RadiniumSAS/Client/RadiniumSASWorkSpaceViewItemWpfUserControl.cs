using Cygnetic.MIPPluginCommons.Licence;
using RadiniumSAS.Background;
using RadiniumSAS.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.Util;

namespace RadiniumSAS.Client
{
    /// <summary>
    /// Interaction logic for RadiniumSASWorkSpaceViewItemWpfUserControl.xaml
    /// </summary>
    public class DoorStatusObject
    {
        private string DoorId;

        public void SetDoorID(string newId)
        {
            DoorId = newId;
        }

        public string GetExtDoorID()
        {
            return DoorId;
        }

        public override string ToString()
        {
            return Name;
        }

        public bool CommsError;
        public bool BatteryError;
        public bool TamperError;

        public string Name { get; set; }
        public string Opened { get; set; }
        public string Errors { get; set; }
        public string BatteryPercent { get; set; }

        public string StrType { get; set; }
    }


    public class ComboItem
    {
        private static Dictionary<int, string> enumPairs = new Dictionary<int, string>();
        public int Key { get; set; }
        public string Value { get; set; }

        static ComboItem()
        {
            Array arr = Enum.GetValues(typeof(SaltoOperation));
            for (int i = 0; i<arr.Length; i++)
            {
                enumPairs[(int)arr.GetValue(i)] = ((SaltoOperation) arr.GetValue(i)).ToDescriptionString();
            }
        }

        public SaltoOperation GetKey()
        {
            if (enumPairs.Any(x => x.Value == Value))
            {
                return enumPairs.Where(x => x.Value == Value).Select(x => (SaltoOperation)x.Key).First();
            }
            return SaltoOperation.UnknownOperation;
        }

        public override string ToString()
        {
            return Value;
        }
    }

    public struct DoorWithStatus
    {
        public string ExtDoorId;
        public string Name;
        public DoorType Type;
        public CommStatus Comms;
        public DoorStatus Opened;
        public BatteryStatus Battery;
        public TamperStatus Tamper;
        public int BatteryPercent;
        public bool Online;

        public string StatusString()
        {
            return String.Format("{0} {1} {2} {3}", Comms.ToDescriptionString(), Opened.ToDescriptionString(), Battery.ToDescriptionString(), Tamper.ToDescriptionString());
        }

        public string ErrorsStatus()
        {
            List<string> totalErrors = new List<string>();
            if(Comms != CommStatus.COMMS_OK && Comms != CommStatus.UNKNOWN)
            {
                totalErrors.Add(Comms.ToDescriptionString());
            }
            if(Opened != DoorStatus.CLOSED && Opened != DoorStatus.INITIALIZING && Opened != DoorStatus.OPENED && Opened != DoorStatus.UNKNOWN)
            {
                totalErrors.Add(Opened.ToDescriptionString());
            }
            if(Battery != BatteryStatus.NORMAL && Battery != BatteryStatus.UNKNOWN)
            {
                totalErrors.Add(Battery.ToDescriptionString());
            }
            if(Tamper != TamperStatus.NORMAL && Tamper != TamperStatus.UNKNOWN)
            {
                totalErrors.Add(Tamper.ToDescriptionString());
            }
            if(totalErrors.Count == 0)
            {
                return "-";
            }
            return string.Join(", ", totalErrors);
        }
    }


    public partial class RadiniumSASWorkSpaceViewItemWpfUserControl : ViewItemWpfUserControl
    {
        private string CurrentDoorTextFilter = "";
        private string CurrentUserTextFilter = "";
        private string CurrentDoorIdFilter = "";
        private int CurrentOperationFilter = -9999;

        PlaybackWpfUserControl playbackControl = null;
        ImageViewerWpfControl imageViewer = null, imageViewer2 = null;
        VideoControl vc1 = null, vc2 = null;
        private FQID myPlayback;

        public RadiniumSASWorkSpaceViewItemWpfUserControl()
        {
            InitializeComponent();
            DoorDataGrid.ItemsSource = currentDoorObjects;
            IEnumerable<Item> allOnlineLocks = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind)
                .Where(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ONLINE_DOOR_TYPE)
                &&
                x.Properties[RadiniumSASDefinition.RADINIUM_ONLINE_DOOR_TYPE] != DoorType.UNKNOWN.ToString()
                );
            LiveViewDoorsCombo.ItemsSource = allOnlineLocks;
        }

        RadiniumSASWorkSpacePlugin parentPlugin = null;

        public void SetParentPlugin(RadiniumSASWorkSpacePlugin plugin)
        {
            parentPlugin = plugin;
        }
        List<Item> currentDoorList = new List<Item>();
        object doorsListLock = new object();

        object auditEventsLock = new object();

        List<SaltoAuditEntry> auditEvents = new List<SaltoAuditEntry>();
        List<SaltoOnlineDoorStatus> doorsStatusList = new List<SaltoOnlineDoorStatus>();
        List<SaltoDbUser> usersList = new List<SaltoDbUser>();
        object usersLock = new object();

        MessageCommunication comms = null;
        bool stopping = false;
        object commsRegisterEvent = null;
        object commsRegisterOnlineState = null;
        object commsAuditResponse = null;
        object photoResponse = null;
        object emergencyStateResponse = null;
        object licenceResponse = null;

        XmlSerializer userSerializer = new XmlSerializer(typeof(SaltoDbUser));

        public void UpdateUsers(List<SaltoDbUser> newUsersList)
        {
            lock (usersLock)
            {
                usersList = newUsersList;
            }
        }

        List<DoorStatusObject> currentDoorObjects = new List<DoorStatusObject>();
        private object doorLock = new object();

        public void FilterAndDisplayDoorStates(bool addedOrDeleted)
        {
            if (DoorStatusCombo == null || DoorListOpenedCombo == null || DoorDataGrid == null || currentDoorObjects == null || currentDoorObjects.Count == 0)
            {
                return;
            }
            Dispatcher.Invoke(() =>
            {
                lock (doorLock)
                {
                    IEnumerable<DoorStatusObject> filteredDoorObjects = new List<DoorStatusObject>();
                    filteredDoorObjects = currentDoorObjects.ToList();
                    if (addedOrDeleted)
                    {
                        List<DoorStatusObject> oneMoreFilteredObject = new List<DoorStatusObject>();
                        DoorStatusObject status = new DoorStatusObject();
                        status.Name = "All locks";
                        status.Errors = "";
                        status.Opened = "";
                        status.SetDoorID("");
                        oneMoreFilteredObject.Add(status);
                        DoorsCombo.ItemsSource = oneMoreFilteredObject.Concat(filteredDoorObjects.OrderBy(y => y.Name)).ToList();
                        status = new DoorStatusObject();
                        status.Name = "Select a lock...";
                        status.Errors = "";
                        status.Opened = "";
                        status.SetDoorID("");
                        List<DoorStatusObject> selectADoorList = new List<DoorStatusObject>();
                        selectADoorList.Add(status);
                        DoorsCombo.SelectedIndex = 0;
                    }
                    if (!String.IsNullOrWhiteSpace(CurrentDoorTextFilter))
                    {
                        filteredDoorObjects = filteredDoorObjects.Where(x => x.Name.ToLowerInvariant().Contains(CurrentDoorTextFilter));
                    }
                    string doorStatusSelection = (DoorStatusCombo.Items[DoorStatusCombo.SelectedIndex] as ComboBoxItem).Content.ToString();
                    if (doorStatusSelection != "All error states")
                    {
                        switch (doorStatusSelection)
                        {
                            case "No errors":
                                filteredDoorObjects = filteredDoorObjects.Where(x => !(x.BatteryError || x.CommsError || x.TamperError));
                                break;
                            case "Low battery":
                                filteredDoorObjects = filteredDoorObjects.Where(x => x.BatteryError);
                                break;
                            case "Comms":
                                filteredDoorObjects = filteredDoorObjects.Where(x => x.CommsError);
                                break;
                            case "Tamper":
                                filteredDoorObjects = filteredDoorObjects.Where(x => x.TamperError);
                                break;
                        }
                    }
                    string doorOpenedSelection = (DoorListOpenedCombo.Items[DoorListOpenedCombo.SelectedIndex] as ComboBoxItem).Content.ToString();
                    if (doorOpenedSelection != "Open or closed")
                    {
                        switch (doorOpenedSelection)
                        {
                            case "Open":
                                filteredDoorObjects = filteredDoorObjects.Where(x => x.Opened == "Open");
                                break;
                            case "Closed":
                                filteredDoorObjects = filteredDoorObjects.Where(x => x.Opened == "Closed");
                                break;
                            case "N/A":
                                filteredDoorObjects = filteredDoorObjects.Where(x => x.Opened == "N/A");
                                break;
                        }
                    }
                    DoorDataGrid.ItemsSource = filteredDoorObjects.OrderBy(x => x.Name).ToList();
                    DoorDataGrid.Items.Refresh();
                }
            });
        }

        public void UpdateDoorStates(List<SaltoOnlineDoorStatus> doorStatuses)
        {
            foreach (SaltoOnlineDoorStatus doorStatus in doorStatuses)
            {
                lock (doorsListLock)
                {
                    Item currentDoor = currentDoorList.FirstOrDefault(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && (x.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID] == doorStatus.DoorID));
                    if (currentDoor != null)
                    {
                        currentDoor.Properties["BatteryStatus"] = doorStatus.BatteryLevel.ToString();
                    }
                    else
                    {

                    }
                }
            }
            lock (doorsListLock)
            {
                doorsStatusList = doorStatuses;
            }
            List<DoorWithStatus> currentDoorStatuses = new List<DoorWithStatus>();
            foreach (Item door in currentDoorList)
            {
                if(!door.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID))
                {
                    continue;
                }
                SaltoOnlineDoorStatus thisDoorStatus = doorStatuses.FirstOrDefault(x => x.DoorID == door.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]);

                DoorWithStatus saltoDoor = new DoorWithStatus();
                if (thisDoorStatus == null)
                {
                    saltoDoor.Battery = BatteryStatus.UNKNOWN;
                    saltoDoor.Comms = CommStatus.UNKNOWN;
                    saltoDoor.Type = DoorType.UNKNOWN;
                    saltoDoor.Opened = DoorStatus.UNKNOWN;
                    saltoDoor.Tamper = TamperStatus.UNKNOWN;
                    saltoDoor.Online = false;
                }
                else
                {
                    saltoDoor.Battery = thisDoorStatus.BatteryStatus;
                    saltoDoor.Comms = thisDoorStatus.CommStatus;
                    saltoDoor.Type = thisDoorStatus.DoorType;
                    saltoDoor.Opened = thisDoorStatus.DoorStatus;
                    saltoDoor.Tamper = thisDoorStatus.TamperStatus;
                    saltoDoor.BatteryPercent = (thisDoorStatus.DoorType != DoorType.CONTROL_UNIT) ? thisDoorStatus.BatteryLevel : -1;
                    saltoDoor.Online = thisDoorStatus.Online;
                }
                string prefix = "";
                if(saltoDoor.Type == DoorType.CONTROL_UNIT)
                {
                    prefix = "(Online) ";
                }
                if(saltoDoor.Type == DoorType.RF_ESCUTCHEON)
                {
                    prefix = "(RF Online) ";
                }
                if(saltoDoor.Type == DoorType.UNKNOWN)
                {
                    prefix = "(Offline) ";
                }
                saltoDoor.Name = prefix + door.Name;
                
                saltoDoor.ExtDoorId = door.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID];
                currentDoorStatuses.Add(saltoDoor);
            }

            List<DoorStatusObject> doorObjects = new List<DoorStatusObject>();

            currentDoorStatuses.OrderBy(x => x.Name).ToList().ForEach(x =>
            {
                DoorStatusObject newObj = new DoorStatusObject() { Name = x.Name, Opened = x.Opened.ToDescriptionString(), Errors = x.ErrorsStatus() };
                newObj.StrType = x.Online ? "Online" : "Offline";
                newObj.BatteryError = x.Battery == BatteryStatus.LOW || x.Battery == BatteryStatus.VERY_LOW || (x.BatteryPercent < 25 && x.BatteryPercent >= 0);
                newObj.CommsError = x.Comms == CommStatus.NO_COMMS;
                newObj.TamperError = x.Tamper == TamperStatus.ALARMED;
                newObj.BatteryPercent = x.BatteryPercent >= 0 ? x.BatteryPercent.ToString() : "Unknown";
                newObj.SetDoorID(x.ExtDoorId);
                if (x.Name.ToLower().Contains(CurrentDoorTextFilter.ToLower()))
                {
                    doorObjects.Add(newObj);
                }
            });

            List<DoorStatusObject> newObjects;
            List<DoorStatusObject> deletedObjects;

            Dispatcher.Invoke(() =>
            {
                bool modified = false;
                bool addedOrDeleted = false;
                lock (doorLock)
                {
                    doorObjects.ForEach(x =>
                    {
                        DoorStatusObject possiblyModified = currentDoorObjects.FirstOrDefault(y => y.GetExtDoorID() == x.GetExtDoorID());
                        if (possiblyModified == null)
                        {
                            return;
                        }
                        if (x.Errors != possiblyModified.Errors)
                        {
                            modified = true;
                            possiblyModified.Errors = x.Errors;
                        }
                        if (x.Name != possiblyModified.Name)
                        {
                            modified = true;
                            possiblyModified.Name = x.Name;
                        }
                        if (x.Opened != possiblyModified.Opened)
                        {
                            modified = true;
                            possiblyModified.Opened = x.Opened;
                        }
                        if (x.BatteryPercent != possiblyModified.BatteryPercent)
                        {
                            modified = true;
                            possiblyModified.BatteryPercent = x.BatteryPercent;
                        }
                    });
                    newObjects = doorObjects.Where(x => !currentDoorObjects.Any(y => y.GetExtDoorID() == x.GetExtDoorID())).ToList();
                    deletedObjects = currentDoorObjects.Where(y => !doorObjects.Any(z => z.GetExtDoorID() == y.GetExtDoorID())).ToList();

                    if (newObjects.Count > 0)
                    {
                        modified = true;
                        currentDoorObjects.AddRange(newObjects);
                    }
                    if (deletedObjects.Count > 0)
                    {
                        modified = true;
                        deletedObjects.ForEach(x => { currentDoorObjects.Remove(x); });
                    }
                    if (newObjects.Count > 0 || deletedObjects.Count > 0)
                    {
                        addedOrDeleted = true;
                        modified = true;
                        currentDoorObjects = currentDoorObjects.OrderBy(x => x.Name).ToList();
                    }
                }
                if (modified)
                {
                    FilterAndDisplayDoorStates(addedOrDeleted);
                    LoadingImageDoors.Visibility = Visibility.Hidden;
                }
            }
            );
        }

        public void UpdateDoorsItemList(List<Item> newDoorsList)
        {
            lock (doorsListLock)
            {
                foreach (Item newDoorItem in newDoorsList)
                {
                    Item matchingDoor = currentDoorList.Where(x => newDoorItem.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && x.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID] == newDoorItem.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID]).FirstOrDefault();
                    if (matchingDoor != null && matchingDoor.Properties.ContainsKey("BatteryStatus"))
                    {
                        newDoorItem.Properties["BatteryStatus"] = matchingDoor.Properties["BatteryStatus"];
                    }
                }
                currentDoorList = newDoorsList;
            }
        }
        Dictionary<int, string> enumPairs = new Dictionary<int, string>();


        public void UpdateOption(object parameter)
        {
            MessageCommunicationManager.Start(EnvironmentManager.Instance.CurrentSite.ServerId);
            while (comms == null)
            {
                Thread.Sleep(1000);
                try
                {
                    comms = MessageCommunicationManager.Get(EnvironmentManager.Instance.CurrentSite.ServerId);
                }
                catch (Exception) { }
            }
            if (commsRegisterEvent == null)
            {
                commsRegisterEvent = comms.RegisterCommunicationFilter(ReceiveLiveEvents, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_NEW_AUDIT_EVENTS));
                commsRegisterOnlineState = comms.RegisterCommunicationFilter(ReceiveDoorStatusesEvent, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_DOOR_STATUSES));
                commsAuditResponse = comms.RegisterCommunicationFilter(ReceiveAuditResponse, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_RESPONSE_AUDIT));
                photoResponse = comms.RegisterCommunicationFilter(ReceivePhoto, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_API_RESPONSE_PHOTO));
                emergencyStateResponse = comms.RegisterCommunicationFilter(ReceiveEmergencyState, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_EMERGENCY_STATE));
                licenceResponse = comms.RegisterCommunicationFilter(ReceiveLicenceIssue, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_LICENCE_INVALID));
            }
            Array arr = Enum.GetValues(typeof(SaltoOperation));
            enumPairs[-9999] = " All events ";
            for (int i = 0; i < arr.Length; i++)
            {
                enumPairs[(int)arr.GetValue(i)] = ((SaltoOperation)arr.GetValue(i)).ToDescriptionString();
            }
            Dispatcher.Invoke(() =>
            {
                EventsCombo.ItemsSource = enumPairs.Where(x => x.Value != null).Select(x => new ComboItem() { Key = x.Key, Value = x.Value }).OrderBy(x => x.Value).ToList();
                EventsCombo.SelectedIndex = 0;
            }
            );
            Thread.Sleep(6000);

            try
            {
                comms.TransmitMessage(new Message(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_AUDIT, new AuditRequest()), EnvironmentManager.Instance.CurrentSite, null, null);
            }
            catch (Exception e)
            {

            }

            while (!stopping)
            {
                try
                {
                    List<Item> items = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind);
                    XmlNode rootNode = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.UserSettingsGuid, false);
                    UpdateDoorsItemList(items);
                    if (rootNode != null)
                    {
                        XDocument newDoc = XDocument.Parse(rootNode.OuterXml);
                        List<SaltoDbUser> parsedUsers;
                        parsedUsers = newDoc.Descendants("SaltoDbUser").Select(x => userSerializer.Deserialize(new StringReader(x.ToString())) as SaltoDbUser).ToList();
                        UpdateUsers(parsedUsers);
                    }

                    Configuration.Instance.RefreshConfiguration(RadiniumSASDefinition.RadiniumSASKind);
                    Thread.Sleep(300000);
                }
                catch (Exception)
                {
                    Thread.Sleep(30000);
                }
            }
        }

        private bool nagScreenShown = false;
        private object ReceiveLicenceIssue(Message msg, FQID destination, FQID source)
        {
            if (!nagScreenShown)
            {
                nagScreenShown = true;
                MessageBox.Show(msg.Data.ToString());
            }
            return null;
        }

        System.Windows.Media.Brush emergencyDefaultBackground = null;
        System.Windows.Media.Brush lockdownAttentionBackground = System.Windows.Media.Brushes.OrangeRed;
        System.Windows.Media.Brush evacuateAttentionBackground = System.Windows.Media.Brushes.Orange;
        private object ReceiveEmergencyState(Message msg, FQID destination, FQID source)
        {

            Dispatcher.Invoke(() =>
            {
                if (emergencyDefaultBackground == null)
                {
                    emergencyDefaultBackground = EmergencyButton.Background;
                }
                string[] msgData = msg.Data as string[];
                EmergencyState = (CurrentEmergencyState)int.Parse(msgData[0]);
                if (EmergencyState == CurrentEmergencyState.NO_EMERGENCY)
                {
                    EmergencyButton.Tag = "Emergency";
                    EmergencyButton.Background = emergencyDefaultBackground;
                }
                else
                {
                    EmergencyDoors = msgData.Skip(1).ToArray();
                    EmergencyButton.Tag = "Emergency Active";
                    if (EmergencyButton.Background == emergencyDefaultBackground)
                    {
                        EmergencyButton.Background = (EmergencyState == CurrentEmergencyState.LOCKDOWN ? lockdownAttentionBackground : evacuateAttentionBackground);
                    }
                    else
                    {
                        EmergencyButton.Background = emergencyDefaultBackground;
                    }
                }
            });
            return null;
        }

        public delegate void ParameterDelegate(byte[] param, string extId);

        private object ReceivePhoto(Message msg, FQID destination, FQID source)
        {
            byte[][] byteParams = msg.Data as byte[][];
            if (byteParams == null)
            {
                return null;
            }
            if (byteParams.Length != 2)
            {
                return null;
            }
            string extUserId = UTF8Encoding.UTF8.GetString(byteParams[0]);

            Dispatcher.Invoke((ParameterDelegate)((bytes, extId) =>
           {
               if (EventDataGrid.SelectedIndex < 0)
               {
                   return;
               }
               dynamic dynamicObj = ToDynamic(EventDataGrid.Items.GetItemAt(EventDataGrid.SelectedIndex));
               if (dynamicObj == null)
               {
                   return;
               }
               object dynId = dynamicObj.Id;
               if (dynId == null)
               {
                   return;
               }
               int temp;
               if (!int.TryParse(dynId.ToString(), out temp))
               {
                   return;
               }
               int dynamicId = temp;

               SaltoAuditEntry entry = null;
               lock (auditEventsLock)
               {
                   entry = auditEvents.FirstOrDefault(x => x.EventId == dynamicId);
               }
               if (entry == null)
               {
                   return;
               }
               if (entry.SubjectType != SaltoSubjectType.CardHolder || entry.SubjectId == null || entry.SubjectId != extId)
               {
                   return;
               }
               byte[] photo = bytes;
               auditEvents.Where(x => x.SubjectId == entry.SubjectId && x.SubjectType == SaltoSubjectType.CardHolder && x.User != null).ToList().ForEach(x => {
                   x.User.Photo = bytes;
               });
               try
               {
                   BitmapImage bitmap = new BitmapImage();
                   bitmap.BeginInit();
                   bitmap.StreamSource = new MemoryStream(photo);
                   bitmap.CacheOption = BitmapCacheOption.OnLoad;
                   bitmap.EndInit();
                   bitmap.Freeze();
                   UserPhotoImage.Source = bitmap;
                   if (entry.User != null)
                   {
                       entry.User.Photo = photo;
                   }
               }
               catch (Exception)
               { }
           }), byteParams[1], extUserId);
            return null;
        }

        ContextMenu menu = new ContextMenu();

        public override void Init()
        {
            ThreadPool.QueueUserWorkItem((x) =>
            {
                UpdateOption(x);
            });

            try
            {
                SecurityAccess.CheckPermission(RadiniumSASDefinition.RadiniumSASPluginId, RadiniumSASDefinition.RADINUM_SECURITY_OPENCLOSE_DOOR);
                MenuItem contextMenuItemOpen = new MenuItem() { Header = "Open door" };
                contextMenuItemOpen.Click += ContextMenuItemOpen_Click;
                MenuItem contextMenuItemClose = new MenuItem() { Header = "Close door" };
                contextMenuItemClose.Click += ContextMenuItemClose_Click;
                menu.Items.Add(contextMenuItemOpen);
                menu.Items.Add(contextMenuItemClose);

            }
            catch (Exception)
            {

            }

            myPlayback = ClientControl.Instance.GeneratePlaybackController();
            imageViewer = new ImageViewerWpfControl();
            imageViewer2 = new ImageViewerWpfControl();
            imageViewer.PlaybackControllerFQID = myPlayback;
            imageViewer2.PlaybackControllerFQID = myPlayback;
            playbackControl = new PlaybackWpfUserControl();
            playbackControl.Init(myPlayback);
            vc1 = ClientControl.Instance.GenerateVideoControl();
            vc1.Live = true;
            vc2 = ClientControl.Instance.GenerateVideoControl();
            vc2.Live = true;
        }

        private void ContextMenuItemClose_Click(object sender, RoutedEventArgs e)
        {
            if(DoorDataGrid.SelectedIndex < 0)
            {
                return;
            }
            DoorStatusObject obj = DoorDataGrid.Items.GetItemAt(DoorDataGrid.SelectedIndex) as DoorStatusObject;
            if(obj != null)
            {

                Item currentItem = null;
                lock (doorsListLock)
                {
                    currentItem = currentDoorList.Where(y => y.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && y.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID] == obj.GetExtDoorID()).FirstOrDefault();
                }
                CloseDoor(currentItem);
            }
        }

        private void OpenDoor(Item obj)
        {
            if (obj == null)
            {
                return;
            }
            try
            {
                SecurityAccess.CheckPermission(obj, RadiniumSASDefinition.RADINUM_SECURITY_OPENCLOSE_DOOR);
                string actionCommand = RadiniumSASDefinition.RADINIUM_MESSAGE_OPEN_DOOR;
                string userName = VideoOS.Platform.EnvironmentManager.Instance.LoginNetworkCredential.UserName;
                ThreadPool.QueueUserWorkItem(x => {
                    try
                    {
                        comms.TransmitMessage(new Message(RadiniumSASDefinition.RADINIUM_MESSAGE_OPEN_DOOR, obj.FQID, new string[] { actionCommand, userName }), EnvironmentManager.Instance.CurrentSite, null, null);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Unable to open door");
                    }
                });

            }
            catch (Exception)
            { }
        }

        private void CloseDoor(Item obj)
        {
            if (obj == null)
            {
                return;
            }

            try
            {
                SecurityAccess.CheckPermission(obj, RadiniumSASDefinition.RADINUM_SECURITY_OPENCLOSE_DOOR);
                string actionCommand = RadiniumSASDefinition.RADINIUM_MESSAGE_CLOSE_DOOR;
                string userName = VideoOS.Platform.EnvironmentManager.Instance.LoginNetworkCredential.UserName;
                ThreadPool.QueueUserWorkItem(x => {
                    try
                    {
                        comms.TransmitMessage(new Message(RadiniumSASDefinition.RADINIUM_MESSAGE_CLOSE_DOOR, obj.FQID, new string[] { actionCommand, userName }), EnvironmentManager.Instance.CurrentSite, null, null);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Unable to open door");
                    }
                });

            }
            catch (Exception)
            { }
        }
        private bool quitting = false;
        private void ContextMenuItemOpen_Click(object sender, RoutedEventArgs e)
        {
            if (DoorDataGrid.SelectedIndex < 0)
            {
                return;
            }
            DoorStatusObject obj = DoorDataGrid.Items.GetItemAt(DoorDataGrid.SelectedIndex) as DoorStatusObject;
            if (obj != null)
            {
                Item currentItem = null;
                lock (doorsListLock)
                {
                    currentItem = currentDoorList.Where(y => y.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && y.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID] == obj.GetExtDoorID()).FirstOrDefault();
                }
                OpenDoor(currentItem);
            }
        }

        public override void Close()
        {
            quitting = true;
            if (comms != null)
            {
                comms.UnRegisterCommunicationFilter(commsRegisterEvent);
                comms.UnRegisterCommunicationFilter(commsRegisterOnlineState);
                comms.UnRegisterCommunicationFilter(commsAuditResponse);
                comms.UnRegisterCommunicationFilter(photoResponse);
                comms.UnRegisterCommunicationFilter(emergencyStateResponse);
                comms.UnRegisterCommunicationFilter(licenceResponse);
            }
        }

        /// <summary>
        /// Do not show the sliding toolbar!
        /// </summary>
        /// 
        public override bool ShowToolbar
        {
            get { return false; }
        }

        private void ViewItemWpfUserControl_ClickEvent(object sender, EventArgs e)
        {
            FireClickEvent();
        }

        private void ViewItemWpfUserControl_DoubleClickEvent(object sender, EventArgs e)
        {
            FireDoubleClickEvent();
        }

        private object ReceiveDoorStatusesEvent(Message message, FQID destination, FQID sender)
        {
            List<SaltoOnlineDoorStatus> doorsStatuses = (message.Data as SaltoOnlineDoorStatus[]).ToList();
            UpdateDoorStates(doorsStatuses);
            return null;
        }

        private object ReceiveAuditResponse(Message message, FQID destination, FQID sender)
        {
            SaltoAuditEntrySerialize[] entries = message.Data as SaltoAuditEntrySerialize[];
            if (entries == null)
            {
                return null;
            }
            lock (auditEventsLock)
            {
                IEnumerable<SaltoAuditEntry> updatedEntries = entries.Select(x => x.SaltoEntry).Where(x => !auditEvents.Any(y => y.EventId == x.EventId));
                auditEvents.AddRange(updatedEntries);
            }

            IEnumerable<SaltoAuditEntry> filterList;
            lock (auditEventsLock)
            {
                filterList = auditEvents;
            }
            if (!String.IsNullOrWhiteSpace(CurrentUserTextFilter))
            {
                filterList = filterList.Where(x => !String.IsNullOrWhiteSpace(x.SubjectName) && x.SubjectName.ToLowerInvariant().Contains(CurrentUserTextFilter));
            }
            Dispatcher.Invoke(() => {
                if (DoorsCombo.SelectedIndex > 0)
                {
                    filterList = filterList.Where(x => x.DoorId == (DoorsCombo.Items[DoorsCombo.SelectedIndex] as DoorStatusObject).GetExtDoorID());
                }
                if (EventsCombo.SelectedIndex > 0)
                {
                    filterList = filterList.Where(x => (int)x.Operation == (EventsCombo.Items[EventsCombo.SelectedIndex] as ComboItem).Key);
                }
                filterList = filterList.OrderByDescending(x => x.EventDate);
                lock (auditEventsLock)
                {
                    auditEvents = filterList.ToList();
                }

                filterList.ToList().ForEach(x => {
                    if (x.Door != null)
                    {
                        if (!x.DoorName.StartsWith("(Door) "))
                        {
                            x.DoorName = "(Door) " + x.DoorName;
                        }
                        return;
                    }
                    if(x.Locker != null)
                    {
                        if (!x.DoorName.StartsWith("(Locker) "))
                        {
                            x.DoorName = "(Locker) " + x.DoorName;
                        }
                        return;
                    }
                    if(x.Room != null)
                    {
                        if (!x.DoorName.StartsWith("(Room) "))
                        {
                            x.DoorName = "(Room) " + x.DoorName;
                        }
                        return;
                    }
                });

                EventDataGrid.ItemsSource = filterList.Select(x => new { Date = x.EventDate.ToLocalTime().ToString("HH:mm:ss\r\nyy/MM/dd"), Id = x.EventId, Door = x.DoorName, Access = x.IsExit ? "Exit" : "Entry", Subject = x.SubjectName, SubjectType = x.SubjectType.ToString(), Operation = (RadiniumSASBackgroundPlugin.AllOperationsList.Any(y => y.Any(z => (int)z == x.Operation)) ? (SaltoOperation)x.Operation : SaltoOperation.UnknownOperation).ToDescriptionString() }).ToList();
                LoadingImageAudit.Visibility = Visibility.Hidden;
            });
            return null;
        }
        public class EqualityComparer<T> : IEqualityComparer<T>
        {
            public EqualityComparer(Func<T, T, bool> cmp)
            {
                this.cmp = cmp;
            }
            public bool Equals(T x, T y)
            {
                return cmp(x, y);
            }

            public int GetHashCode(T obj)
            {
                return obj.GetHashCode();
            }

            public Func<T, T, bool> cmp { get; set; }
        }

        List<LiveCardObject> currentLiveViewEntries = new List<LiveCardObject>();

        public class LiveCardObject
        {
            private BitmapImage img;

            public BitmapImage AccessImage 
            {
                get
                {
                    return img;
                }
            }

            public string AccessColour 
            {
                get
                {
                    if(AccessBool)
                    {
                        return "#5fa800";
                    }
                    return "#eb2520";
                }
            }
            public string AccessValue
            {
                get
                {
                    string access = Opening ? "ACCESS " : "CLOSE ";
                    access += AccessBool ? "GRANTED" : "DENIED";
                    return access;
                }
            }

            public bool AccessBool { get; set; }
            public bool Opening { get; set; }
            public string AccessTime 
            { 
                get 
                {
                    return AccessDateTime.ToString("HH:mm:ss - yyyy/MM/dd");
                } 
            }
            public string AccessName { get; set; }
            public string AccessNumber { get; set; }
            public DateTime AccessDateTime { get; set; }
            public byte[] AccessPhoto 
            {
                set
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.StreamSource = new MemoryStream(value);
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    bitmap.EndInit();
                    bitmap.Freeze();
                    img = bitmap;
                }
            }
        }


        private object ReceiveLiveEvents(Message message, FQID destination, FQID sender)
        {
            lock (auditEventsLock)
            {
                while (auditEvents.Count > 100)
                {
                    auditEvents.Remove(auditEvents.Find(y => y.EventDate == auditEvents.Select(z => z.EventDate).Min()));
                }
            }
            SaltoAuditEntry[] entries = message.Data as SaltoAuditEntry[];
            if(entries == null)
            {
                return null;
            }
            Dispatcher.Invoke(() => {
                if (LiveViewDoorsCombo.SelectedIndex >= 0)
                {
                    Item selectedObject = LiveViewDoorsCombo.SelectedItem as Item;
                    if(!selectedObject.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID))
                    {
                        return;
                    }
                    string selectedDoorId = selectedObject.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID];
                    foreach (SaltoAuditEntry entry in entries.OrderBy(x => x.EventDate))
                    {
                        if(!(RadiniumSASBackgroundPlugin.DoorOpenEventTypes.Any(x => (int)x == entry.Operation) 
                        || RadiniumSASBackgroundPlugin.DeniedOpenEventTypes.Any(x => (int)x == entry.Operation)
                        || RadiniumSASBackgroundPlugin.DeniedCloseEventTypes.Any(x => (int)x == entry.Operation)
                        || RadiniumSASBackgroundPlugin.DoorCloseEventTypes.Any(x => (int)x == entry.Operation))
                        )
                        {
                            continue;
                        }
                        if (entry.DoorId == selectedDoorId)
                        {
                            LiveCardObject card = new LiveCardObject();
                            if (entry.SubjectType == SaltoSubjectType.CardHolder)
                            {
                                card.AccessName = entry.SubjectName;
                            }
                            if(entry.SubjectType == SaltoSubjectType.SoftwareOperator)
                            {
                                card.AccessName = "(Software Operator)";
                            }
                            card.AccessBool =
                                RadiniumSASBackgroundPlugin.DoorOpenEventTypes.Any(x => (int)x == entry.Operation)
                                || RadiniumSASBackgroundPlugin.DoorCloseEventTypes.Any(x => (int)x == entry.Operation);
                            card.Opening = RadiniumSASBackgroundPlugin.DoorOpenEventTypes.Any(x => (int)x == entry.Operation) || RadiniumSASBackgroundPlugin.DeniedOpenEventTypes.Any(x => (int)x == entry.Operation);
                            card.AccessDateTime = entry.EventDate.ToLocalTime();
                            if(entry.User != null)
                            {
                                card.AccessPhoto = entry.User.Photo;
                                card.AccessNumber = entry.User.PhoneNumber;
                            }
                            currentLiveViewEntries.Add(card);
                        }
                    }
                    currentLiveViewEntries = currentLiveViewEntries.OrderByDescending(x => x.AccessDateTime).Take(100).ToList();
                    CardList.ItemsSource = currentLiveViewEntries;
                }
            });
            foreach (SaltoAuditEntry entry in entries)
            {
                if (!String.IsNullOrWhiteSpace(CurrentUserTextFilter))
                {
                    if (entry.SubjectType != SaltoSubjectType.CardHolder)
                    {
                        continue;
                    }
                    if (!entry.SubjectName.ToLowerInvariant().Contains(CurrentUserTextFilter))
                    {
                        continue;
                    }
                }
                if (CurrentDoorIdFilter != "")
                {
                    if (entry.DoorId != CurrentDoorIdFilter)
                    {
                        continue;
                    }
                }
                if (CurrentOperationFilter != -9999)
                {
                    if ((int)entry.Operation != CurrentOperationFilter)
                    {
                        continue;
                    }
                }
                lock (auditEventsLock)
                {
                    if (!auditEvents.Any(x => x.EventId == entry.EventId))
                    {
                        if (entry.Door != null)
                        {
                            if (!entry.DoorName.StartsWith("(Door) "))
                            {
                                entry.DoorName = "(Door) " + entry.DoorName;
                            }
                        }
                        if (entry.Locker != null)
                        {
                            if (!entry.DoorName.StartsWith("(Locker) "))
                            {
                                entry.DoorName = "(Locker) " + entry.DoorName;
                            }
                        }
                        if (entry.Room != null)
                        {
                            if (!entry.DoorName.StartsWith("(Room) "))
                            {
                                entry.DoorName = "(Room) " + entry.DoorName;
                            }
                        }
                        auditEvents.Add(entry);
                        if(entry.User != null && entry.User.Photo != null)
                        {
                            auditEvents
                                .Where(
                                    x => x.User != null &&
                                    x.User.Photo == null &&
                                    x.SubjectId == entry.SubjectId &&
                                    x.SubjectType == SaltoSubjectType.CardHolder &&
                                    entry.SubjectType == SaltoSubjectType.CardHolder
                                    )
                                .ToList()
                                .ForEach(x =>
                                {
                                    x.User.Photo = entry.User.Photo;
                                });
                        }
                    }
                }
            }
            IEnumerable<SaltoAuditEntry> filterList;
            lock (auditEventsLock)
            {
                filterList = auditEvents.ToList();
            }
            if (!String.IsNullOrWhiteSpace(CurrentUserTextFilter))
            {
                filterList = filterList.Where(x => x.SubjectType == SaltoSubjectType.CardHolder && x.SubjectName.ToLowerInvariant().Contains(CurrentUserTextFilter));
            }

            if (CurrentDoorIdFilter != "")
            {
                filterList = filterList.Where(x => x.DoorId == CurrentDoorIdFilter);
            }

            if (CurrentOperationFilter != -9999)
            {
                filterList = filterList.Where(x => (int)x.Operation == CurrentOperationFilter);
            }
            filterList = filterList.OrderByDescending(x => x.EventDate);
            lock (auditEventsLock)
            {
                auditEvents = filterList.ToList();
            }
            
            Dispatcher.Invoke(() => { 
                EventDataGrid.ItemsSource = filterList.Select(x => new { Date = x.EventDate.ToLocalTime().ToString("HH:mm\r\nyy/MM/dd"), Id = x.EventId, Door = x.DoorName, Access = x.IsExit ? "Exit" : "Entry", Subject = x.SubjectName, SubjectType = x.SubjectType.ToString(), Operation = (RadiniumSASBackgroundPlugin.AllOperationsList.Any(y => y.Any(z => (int)z == x.Operation)) ? (SaltoOperation)x.Operation : SaltoOperation.UnknownOperation).ToDescriptionString() }).ToList();
            });
            return null;
        }

        public dynamic ToDynamic(object value)
        {
            IDictionary<string, object> expando = new System.Dynamic.ExpandoObject();

            foreach (System.ComponentModel.PropertyDescriptor property in System.ComponentModel.TypeDescriptor.GetProperties(value.GetType()))
                expando.Add(property.Name, property.GetValue(value));

            return expando as System.Dynamic.ExpandoObject;
        }

        private string GetItemPropertyString(Item item, string name)
        {
            if (item == null)
            {
                return null;
            }
            if (item.Properties.ContainsKey(name))
            {
                return item.Properties[name];
            }
            return null;
        }

        private void CameraSelectionChanged(string DoorId, DateTime eventTime, bool live)
        {
            Item currentSelectedItem = currentDoorList.Where(x => x.Properties.ContainsKey("ExtDoorID") && x.Properties["ExtDoorID"] == DoorId).FirstOrDefault();

            Guid camera1 = Guid.Empty, camera2 = Guid.Empty;
            string cam1 = GetItemPropertyString(currentSelectedItem, RadiniumSASDefinition.RADINIUM_CAMERA1);
            string cam2 = GetItemPropertyString(currentSelectedItem, RadiniumSASDefinition.RADINIUM_CAMERA2);
            if (cam1 != null && !String.IsNullOrWhiteSpace(cam1))
            {
                Guid.TryParse(cam1, out camera1);
            }
            if (cam2 != null && !string.IsNullOrWhiteSpace(cam2))
            {
                Guid.TryParse(cam2, out camera2);
            }
            Item camera1Item = null, camera2Item = null;
            if (camera1 != Guid.Empty)
            {
                for (int i = 0; i < 4 && camera1Item == null; i++)
                {
                    try
                    {
                        camera1Item = Configuration.Instance.GetItem(camera1, Kind.Camera);
                    }
                    catch (Exception)
                    { }
                }
            }
            if (camera2 != Guid.Empty)
            {
                for (int i = 0; i < 4 && camera2Item == null; i++)
                {
                    try
                    {
                        camera2Item = Configuration.Instance.GetItem(camera2, Kind.Camera);
                    }
                    catch (Exception)
                    { }
                }
            }
            if (live)
            {
                parentPlugin.UpdateCameraViewsLive(camera1Item != null ? camera1Item.FQID : null, camera2Item != null ? camera2Item.FQID : null);
            }
            else
            {
                parentPlugin.UpdateCameraViews(eventTime, camera1Item != null ? camera1Item.FQID : null, camera2Item != null ? camera2Item.FQID : null);
            }
            
            parentPlugin.UpdateMapView(GetItemPropertyString(currentSelectedItem, RadiniumSASDefinition.RADINIUM_MAP));
        }

        private void EventDataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if(EventDataGrid.SelectedIndex < 0 || !EventsTab.IsSelected)
            {
                return;
            }
            dynamic dynamicObj = ToDynamic(EventDataGrid.Items.GetItemAt(EventDataGrid.SelectedIndex));
            object dynId = dynamicObj.Id;
            int dynamicId = int.Parse(dynId.ToString());
            SaltoAuditEntry entry = null;
            lock (auditEventsLock)
            {
                entry = auditEvents.FirstOrDefault(x => x.EventId == dynamicId);
            }
            if (entry == null)
            {
                return;
            }
            CameraSelectionChanged(entry.DoorId, entry.EventDate.AddSeconds(-10), false);
            UserPhotoImage.Source = null;
            TextUserName.Content = "";
            TextUserPhone.Content = "";
            TextUserDetails.Content = "";
            LabelPhoneNumber.Visibility = Visibility.Hidden;
            LabelDetails.Visibility = Visibility.Hidden;

            if (entry.SubjectType == SaltoSubjectType.CardHolder)
            {
                if (entry.SubjectId != null)
                {
                    TextUserName.Content = entry.SubjectName;
                    if (entry.User != null)
                    {
                        if (entry.User.Photo != null && entry.User.Photo.Length > 0)
                        {
                            try
                            {
                                BitmapImage bitmap = new BitmapImage();
                                bitmap.BeginInit();
                                bitmap.StreamSource = new MemoryStream(entry.User.Photo);
                                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                                bitmap.EndInit();
                                bitmap.Freeze();
                                UserPhotoImage.Source = bitmap;
                            }
                            catch (Exception) { }
                        }
                        else
                        {
                            ThreadPool.QueueUserWorkItem(x => {
                                try
                                {
                                    comms.TransmitMessage(new Message(RadiniumSASDefinition.RADINIUM_API_REQUEST_PHOTO, entry.SubjectId), EnvironmentManager.Instance.CurrentSite, null, null);
                                }
                                catch (Exception)
                                {
                                }
                            });
                        }
                        TextUserPhone.Content = entry.User.PhoneNumber ?? "";
                        if(!string.IsNullOrWhiteSpace(entry.User.PhoneNumber))
                        {
                            LabelPhoneNumber.Visibility = Visibility.Visible;
                        }
                        string newText = ((entry.User.IsBanned ?? false) ? "Banned\r\n" : "");
                        if(entry.User.GPF1 != null)
                        {
                            newText += entry.User.GPF1 + "\r\n";
                        }
                        if(entry.User.GPF2 != null)
                        {
                            newText += entry.User.GPF2 + "\r\n";
                        }
                        TextUserDetails.Content = newText;
                        if(!String.IsNullOrWhiteSpace(newText))
                        {
                            LabelDetails.Visibility = Visibility.Visible;
                        }
                    }
                }
            }
        }

        private void DoorDataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (DoorDataGrid.SelectedIndex >= 0 && LockTab.IsSelected)
            {
                DoorStatusObject obj = DoorDataGrid.Items.GetItemAt(DoorDataGrid.SelectedIndex) as DoorStatusObject;
                if (obj == null)
                {
                    return;
                }
                CameraSelectionChanged(obj.GetExtDoorID(), DateTime.UtcNow, true);
            }
        }

        Window reportWindow = null;

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (reportWindow != null)
            {
                reportWindow.Close();
            }
            RadiniumSASReportForm form = new RadiniumSASReportForm(comms);
            reportWindow = new Window();
            reportWindow.Closed += ReportWindow_Closed;
            reportWindow.Title = "Generate Report";
            reportWindow.Width = 606;
            reportWindow.Height = 560;
            reportWindow.Content = form;
            reportWindow.Show();
            reportWindow.Activate();
            reportWindow.Topmost = true;
            reportWindow.Focus();
        }

        private void ReportWindow_Closed(object sender, EventArgs e)
        {
            reportWindow = null;
        }

        private void SearchBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CurrentUserTextFilter = SearchBox.Text.ToLowerInvariant();
            SendAudit();
        }

        private void DoorBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CurrentDoorTextFilter = DoorBox.Text.ToLowerInvariant();
            SendAudit();
        }

        private void DoorListOpenedCombo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            FilterAndDisplayDoorStates(false);
        }

        private void DoorStatusCombo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            FilterAndDisplayDoorStates(false);
        }

        private void SendAudit()
        {

            ThreadPool.QueueUserWorkItem(x =>
            {
                if (comms == null)
                {
                    return;
                }
                AuditRequest currentRequest = new AuditRequest();
                if (!String.IsNullOrWhiteSpace(CurrentUserTextFilter))
                {
                    currentRequest.CardholderName = CurrentUserTextFilter;
                }
                if (CurrentDoorIdFilter != "")
                {
                    currentRequest.DoorId = CurrentDoorIdFilter;
                }
                if (CurrentOperationFilter != -9999)
                {
                    currentRequest.EventType = (SaltoOperation)CurrentOperationFilter;
                }
                try
                {
                    comms.TransmitMessage(new Message(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_AUDIT, currentRequest), EnvironmentManager.Instance.CurrentSite, null, null);
                }
                catch(Exception e)
                {

                }
            });
        }

        private void EventsCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (EventsCombo.SelectedIndex > 0)
            {
                CurrentOperationFilter = (int)(EventsCombo.Items[EventsCombo.SelectedIndex] as ComboItem).GetKey();
            }
            else
            {
                CurrentOperationFilter = -9999;
            }
            SendAudit();
        }

        private void DoorsCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DoorsCombo.SelectedIndex > 0)
            {
                CurrentDoorIdFilter = (DoorsCombo.Items[DoorsCombo.SelectedIndex] as DoorStatusObject).GetExtDoorID();
            }
            else
            {
                CurrentDoorIdFilter = "";
            }
            SendAudit();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentUserTextFilter = SearchBox.Text.ToLowerInvariant();
            SendAudit();
        }

        private void SearchDoorButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentDoorTextFilter = DoorBox.Text.ToLowerInvariant();
            SendAudit();
        }

        Window emergencyWindow = null;
        private void EmergencyButton_Click(CurrentEmergencyState targetState)
        {
            if (emergencyWindow != null)
            {
                emergencyWindow.Close();
            }
            RadiniumSASEmergencyForm form = new RadiniumSASEmergencyForm(comms, EmergencyState, EmergencyDoors, targetState);
            emergencyWindow = new Window();
            emergencyWindow.Closed += EmergencyWindow_Closed;
            emergencyWindow.Title = "Emergency Situations";
            emergencyWindow.Width = 606;
            if (targetState != CurrentEmergencyState.NO_EMERGENCY)
            {
                emergencyWindow.Height = 490;
            }
            else
            {
                emergencyWindow.Height = 50 + 80 + 70 + 40;
            }

            emergencyWindow.Content = form;
            emergencyWindow.Show();
            emergencyWindow.Activate();
            emergencyWindow.Topmost = true;
            emergencyWindow.Focus();
        }

        private void EmergencyLockdown_Click(object sender, RoutedEventArgs e)
        {
            EmergencyButton_Click(CurrentEmergencyState.LOCKDOWN);
        }

        private void EmergencyEvacuate_Click(object sender, RoutedEventArgs e)
        {
            EmergencyButton_Click(CurrentEmergencyState.EVACUATION);
        }

        private void EmergencyEnd_Click(object sender, RoutedEventArgs e)
        {
            EmergencyButton_Click(CurrentEmergencyState.NO_EMERGENCY);
        }

        private CurrentEmergencyState EmergencyState = CurrentEmergencyState.NO_EMERGENCY;
        private string[] EmergencyDoors = new string[0];

        private void EmergencyWindow_Closed(object sender, EventArgs e)
        {
            RadiniumSASEmergencyForm form = emergencyWindow.Content as RadiniumSASEmergencyForm;
            EmergencyDoors = form.doorIds;
            emergencyWindow = null;
            if(form.EmergencyActive != EmergencyState)
            {
                /*
                if(form.EmergencyActive == CurrentEmergencyState.NO_EMERGENCY)
                {
                    MessageBox.Show("Deactivating emergency mode");
                }
                if(form.EmergencyActive == CurrentEmergencyState.LOCKDOWN)
                {
                    MessageBox.Show("Activating lockdown");
                }
                if(form.EmergencyActive == CurrentEmergencyState.EVACUATION)
                {
                    MessageBox.Show("Activating evacuation");
                }*/
                EmergencyButton.Tag = "";
            }
        }

        private void ClearEventFilterButton_Click(object sender, RoutedEventArgs e)
        {
            SearchBox.Text = "";
            if(DoorsCombo.Items.Count > 0)
            {
                DoorsCombo.SelectedIndex = 0;
            }
            if(EventsCombo.Items.Count > 0)
            {
                EventsCombo.SelectedIndex = 0;
            }
            SearchButton_Click(sender, e);
        }

        private void ClearDoorFilterButton_Click(object sender, RoutedEventArgs e)
        {
            DoorBox.Text = "";
            if(DoorListOpenedCombo.Items.Count > 0)
            {
                DoorListOpenedCombo.SelectedIndex = 0;
            }
            if(DoorStatusCombo.Items.Count > 0)
            {
                DoorStatusCombo.SelectedIndex = 0;
            }
            SearchDoorButton_Click(sender, e);
        }

        private DateTime previousTimeClicked = DateTime.MinValue;
        private DateTime previousTimeClickedLock = DateTime.MinValue;
        private System.Windows.Media.SolidColorBrush BackgroundDefaultColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(0x5f, 0xa8, 0x00));
        private System.Windows.Media.SolidColorBrush BackgroundClickedColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(0xec, 0xab, 0x00));

        private Timer timer = null;

        private void LiveViewUnlockButton_Click(object sender, RoutedEventArgs e)
        {
            if(previousTimeClicked.AddSeconds(3) > DateTime.Now || LiveViewDoorsCombo.SelectedIndex < 0)
            {
                return;
            }
            previousTimeClicked = DateTime.Now;
            LiveViewUnlockButton.Background = BackgroundClickedColour;

            Item obj = LiveViewDoorsCombo.SelectedItem as Item;
            OpenDoor(obj);
            timer = new Timer((x) => {
                Dispatcher.Invoke(() => {
                    LiveViewUnlockButton.Background = BackgroundDefaultColour;
                });
            }, null, 3000, Timeout.Infinite);
        }

        private void LiveViewLockButton_Click(object sender, RoutedEventArgs e)
        {
            if (previousTimeClickedLock.AddSeconds(3) > DateTime.Now || LiveViewDoorsCombo.SelectedIndex < 0)
            {
                return;
            }
            previousTimeClickedLock = DateTime.Now;
            LiveViewLockButton.Background = BackgroundClickedColour;
            Item obj = LiveViewDoorsCombo.SelectedItem as Item;
            CloseDoor(obj);

            timer = new Timer((x) => {
                Dispatcher.Invoke(() => {
                    LiveViewLockButton.Background = BackgroundDefaultColour;
                });
            }, null, 3000, Timeout.Infinite);
        }


        private void CardList_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (parentPlugin != null)
            {
                parentPlugin.UpdateTabViews(LiveView.IsSelected);
                if(LiveView.IsSelected)
                {
                    LiveViewDoorsCombo_SelectionChanged(sender, e);
                }
            }
        }

        private void LiveViewDoorsCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(!LiveView.IsSelected)
            {
                return;
            }
            if(LiveViewDoorsCombo.SelectedIndex >= 0)
            {
                Item currentSelectedItem = (LiveViewDoorsCombo.SelectedItem as Item);

                Guid camera1 = Guid.Empty, camera2 = Guid.Empty;
                string cam1 = GetItemPropertyString(currentSelectedItem, RadiniumSASDefinition.RADINIUM_CAMERA1);
                string cam2 = GetItemPropertyString(currentSelectedItem, RadiniumSASDefinition.RADINIUM_CAMERA2);
                if (cam1 != null && !String.IsNullOrWhiteSpace(cam1))
                {
                    Guid.TryParse(cam1, out camera1);
                }
                if (cam2 != null && !string.IsNullOrWhiteSpace(cam2))
                {
                    Guid.TryParse(cam2, out camera2);
                }

                parentPlugin.UpdateTabCameras(camera1, camera2);
            }
            else
            {
                parentPlugin.UpdateTabCameras(Guid.Empty, Guid.Empty);
            }
            
            currentLiveViewEntries.Clear();
            CardList.ItemsSource = currentLiveViewEntries;
            CardList.Items.Refresh();
        }
    }


    [ValueConversion(typeof(string), typeof(String))]
    public class SubjectTypeConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var returnPath = "";
            if (value.Equals("CardHolder"))
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_subjecttype_card.png";
            else if (value.Equals("Door"))
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_subjecttype_door.png";
            else if (value.Equals("SoftwareOperator"))
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_subjecttype_software.png";
            return returnPath;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    [ValueConversion(typeof(string), typeof(String))]
    public class BatteryConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string val = value.ToString();
            int parseVal = -1;
            if(!int.TryParse(val, out parseVal))
            {
                parseVal = -1;
            }
            var returnPath = "";
            if (parseVal >= 80)
            {
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_battery_high.png";
            }
            else if (parseVal >= 40)
            {
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_battery_med.png";
            }
            else if (parseVal > 20)
            {
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_battery_low.png";
            }
            else if (parseVal >= 0)
            {
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_battery_empty.png";
            }
            return returnPath;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(string), typeof(String))]
    public class OpenedConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            string returnPath = "";
            if (value.Equals("Open"))
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_openclose_open.png";
            else if (value.Equals("Closed"))
                returnPath = "pack://application:,,,/RadiniumSAS;component/Resources/ic_openclose_close.png";

            return returnPath;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
