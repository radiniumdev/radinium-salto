using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.Messaging;

namespace RadiniumSAS.Client
{
    public class RadiniumSASWorkSpacePlugin : WorkSpacePlugin
    {

        private List<object> _messageRegistrationObjects = new List<object>();

        public bool IsOnline { get; set; } = false;
        private bool _workSpaceSelected = false;
        private bool _workSpaceViewSelected = false;

        /// <summary>
        /// The Id.
        /// </summary>
        public override Guid Id
        {
            get { return RadiniumSASDefinition.RadiniumSASWorkSpacePluginId; }
        }

        /// <summary>
        /// The name displayed on top
        /// </summary>
        public override string Name
        {
            get { return "SALTO"; }
        }

        /// <summary>
        /// We support setup mode
        /// </summary>
        public override bool IsSetupStateSupported
        {
            get { return true; }
        }

        private RadiniumSASWorkSpaceCameraViewItemPlugin viewItem1;

        /// <summary>
        /// Initializa the plugin
        /// </summary>
        public override void Init()
        {
            LoadProperties(true);

            //add message listeners
            _messageRegistrationObjects.Add(EnvironmentManager.Instance.RegisterReceiver(ShownWorkSpaceChangedReceiver, new MessageIdFilter(MessageId.SmartClient.ShownWorkSpaceChangedIndication)));
            _messageRegistrationObjects.Add(EnvironmentManager.Instance.RegisterReceiver(WorkSpaceStateChangedReceiver, new MessageIdFilter(MessageId.SmartClient.WorkSpaceStateChangedIndication)));
            _messageRegistrationObjects.Add(EnvironmentManager.Instance.RegisterReceiver(SelectedViewChangedReceiver, new MessageIdFilter(MessageId.SmartClient.SelectedViewChangedIndication)));
            //build view layout - modify to your needs. Here we use a matrix of 1000x1000 to define the layout 
            List<Rectangle> rectangles = new List<Rectangle>();
            rectangles.Add(new Rectangle(0, 0, 550, 1000));      // Panel List            
            rectangles.Add(new Rectangle(550, 0, 450, 500));      // Camera
            rectangles.Add(new Rectangle(550, 500, 450, 500));      // Map

            viewItem1 = new RadiniumSASWorkSpaceCameraViewItemPlugin(this);
            ViewAndLayoutItem.Layout = rectangles.ToArray();
            ViewAndLayoutItem.Name = Name;
            ViewAndLayoutItem.InsertViewItemPlugin(0, new RadiniumSASWorkSpaceViewItemPlugin(this), new Dictionary<string, string>());
            ViewAndLayoutItem.InsertViewItemPlugin(1, viewItem1, new Dictionary<string, string>());
            ViewAndLayoutItem.InsertBuiltinViewItem(2, VideoOS.Platform.Client.ViewAndLayoutItem.EmptyBuiltinId, new Dictionary<string, string>());            
            IsOnline = true;
  
        }

        public void UpdateMapView(string mapGuid)
        {

            if (ViewAndLayoutItem.GetChildren().Count <= 2 || (!ViewAndLayoutItem.GetChildren()[2].Properties.ContainsKey("mapguid")) || ViewAndLayoutItem.GetChildren()[2].Properties["mapguid"] != mapGuid)
            {
                Dictionary<string, string> mapDict = new Dictionary<string, string>();
                mapDict["mapguid"] = mapGuid;
                if (mapGuid == null)
                {
                    ViewAndLayoutItem.InsertBuiltinViewItem(2, VideoOS.Platform.Client.ViewAndLayoutItem.EmptyBuiltinId, mapDict);
                }
                else
                {
                    ViewAndLayoutItem.InsertBuiltinViewItem(2, VideoOS.Platform.Client.ViewAndLayoutItem.MapBuiltinId, mapDict);
                }
            }
        }
        bool fullScreen = false;
        public void UpdateTabViews(bool fullscreen)
        {
            if(fullscreen == fullScreen)
            {
                return;
            }
            fullScreen = fullscreen;
            if(fullscreen)
            {
                ViewAndLayoutItem.InsertBuiltinViewItem(1, VideoOS.Platform.Client.ViewAndLayoutItem.EmptyBuiltinId, new Dictionary<string, string>());
                ViewAndLayoutItem.InsertBuiltinViewItem(2, VideoOS.Platform.Client.ViewAndLayoutItem.EmptyBuiltinId, new Dictionary<string, string>());
            }
            else
            {
                ViewAndLayoutItem.InsertViewItemPlugin(1, viewItem1, new Dictionary<string, string>());
                ViewAndLayoutItem.InsertBuiltinViewItem(2, VideoOS.Platform.Client.ViewAndLayoutItem.EmptyBuiltinId, new Dictionary<string, string>());
            }
        }

        public void UpdateTabCameras(Guid camera1, Guid camera2)
        {
            if(camera1 != Guid.Empty)
            {
                Dictionary<string, string> CameraDictionary = new Dictionary<string, string>();
                CameraDictionary.Add("CameraId", camera1.ToString());
                ViewAndLayoutItem.InsertBuiltinViewItem(1, VideoOS.Platform.Client.ViewAndLayoutItem.CameraBuiltinId, CameraDictionary);
            }
            else
            {
                ViewAndLayoutItem.InsertBuiltinViewItem(1, VideoOS.Platform.Client.ViewAndLayoutItem.EmptyBuiltinId, new Dictionary<string, string>());
            }
            if(camera2 != Guid.Empty)
            {
                Dictionary<string, string> CameraDictionary = new Dictionary<string, string>();
                CameraDictionary.Add("CameraId", camera2.ToString());
                ViewAndLayoutItem.InsertBuiltinViewItem(2, VideoOS.Platform.Client.ViewAndLayoutItem.CameraBuiltinId, CameraDictionary);
            }
            else
            {
                ViewAndLayoutItem.InsertBuiltinViewItem(2, VideoOS.Platform.Client.ViewAndLayoutItem.EmptyBuiltinId, new Dictionary<string, string>());
            }
        }

        public void UpdateCameraViewsLive(FQID camera1, FQID camera2)
        {
            viewItem1.SwitchToLiveCameras(camera1, camera2);
        }

        public void UpdateCameraViews(DateTime eventTime, FQID camera1, FQID camera2)
        {
            viewItem1.SwitchToCamerasOnTime(eventTime, camera1, camera2);
        }

        /// <summary>
        /// Close workspace and clean up
        /// </summary>
        public override void Close()
        {
            foreach (object messageRegistrationObject in _messageRegistrationObjects)
            {
                EnvironmentManager.Instance.UnRegisterReceiver(messageRegistrationObject);
            }
            _messageRegistrationObjects.Clear();
        }

        /// <summary>
        /// User modified something in setup mode
        /// </summary>
        /// <param name="index"></param>
        public override void ViewItemConfigurationModified(int index)
        {
            base.ViewItemConfigurationModified(index);

            if (ViewAndLayoutItem.ViewItemId(index) == ViewAndLayoutItem.CameraBuiltinId)
            {
                SetProperty("Camera" + index, ViewAndLayoutItem.ViewItemConfigurationString(index));
                SaveProperties(true);
            }
        }

        /// <summary>
        /// Keep track of what workspace is selected, if this is selected the _workSpaceViewSelected is true.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        /// <param name="related"></param>
        /// <returns></returns>
        private object ShownWorkSpaceChangedReceiver(Message message, FQID sender, FQID related)
        {
            if (message.Data is Item && ((Item)message.Data).FQID.ObjectId == Id)
            {
                _workSpaceSelected = true;
                Notification = null;
            }
            else
            {
                _workSpaceSelected = false;
            }
            return null;
        }

        /// <summary>
        /// Keep track of current state: in setup or normal
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        /// <param name="related"></param>
        /// <returns></returns>
        private object WorkSpaceStateChangedReceiver(Message message, FQID sender, FQID related)
        {
            if (_workSpaceSelected && ((WorkSpaceState)message.Data) == WorkSpaceState.Normal)
            {
                // Went in or out of Setup state
            }
            return null;
        }


        /// <summary>
        /// Keep track of what workspace is selected, if this is selected the _workSpaceViewSelected is true.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        /// <param name="related"></param>
        /// <returns></returns>
        private object SelectedViewChangedReceiver(Message message, FQID sender, FQID related)
        {
            if (message.Data is Item && ((Item)message.Data).FQID.ObjectId == ViewAndLayoutItem.FQID.ObjectId)
            {
                _workSpaceViewSelected = true;
            }
            else
            {
                _workSpaceViewSelected = false;
            }
            return null;
        }

    }
}
