using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Cygnetic.MIPPluginCommons.Licence;
using Cygnetic.MIPPluginCommons.Util;
using RadiniumSAS.Admin;
using RadiniumSAS.Background;
using RadiniumSAS.Client;
using VideoOS.Platform;
using VideoOS.Platform.Admin;
using VideoOS.Platform.Background;
using VideoOS.Platform.Client;
using VideoOS.Platform.RuleAction;

namespace RadiniumSAS
{


    /// <summary>
    /// The PluginDefinition is the ‘entry’ point to any plugin.  
    /// This is the starting point for any plugin development and the class MUST be available for a plugin to be loaded.  
    /// Several PluginDefinitions are allowed to be available within one DLL.
    /// Here the references to all other plugin known objects and classes are defined.
    /// The class is an abstract class where all implemented methods and properties need to be declared with override.
    /// The class is constructed when the environment is loading the DLL.
    /// </summary>
    public class RadiniumSASDefinition : PluginDefinition
    {
        private static System.Drawing.Image _treeNodeImage;
        private static System.Drawing.Image _topTreeNodeImage;
        private static System.Drawing.Image _evacuationImage;
        private static System.Drawing.Image _lockdownImage;
        public  static Icon _doorIcon;

        internal static Guid RadiniumSASPluginId = new Guid("05a65774-5650-4e84-b8fb-4ad345d69594");
        internal static Guid RadiniumSASKind = new Guid("731eab53-5ac6-4274-b0e4-79b62a162aeb");
        internal static Guid RadiniumSASBackgroundPlugin = new Guid("1d39f15f-a9fa-49c2-a1a8-8fbce1defc6d");
        internal static Guid RadinumSaltoItemUpdaterPlugin = new Guid("7e59e1b1-1818-4b5a-91d4-6b69ebb32835");
        internal static Guid RadinumSaltoStateUpdaterPlugin = new Guid("850564af-3a69-47ab-83f2-7303ad1f4456");
        internal static Guid RadiniumSaltoQueryResponderPlugin = new Guid("b5a574f8-0698-4705-a18e-58a8ae15ea68");
        internal static Guid RadiniumSASWorkSpacePluginId = new Guid("39ba3cb7-a04c-48b4-ae93-840055ae54f8");
        internal static Guid RadiniumSASWorkSpaceViewItemPluginId = new Guid("f50f5684-8f03-493a-993b-3917af4a0966");
        internal static Guid RadiniumSASWorkSpaceCameraViewItemPluginId = new Guid("d6b42ba5-952e-4e73-a203-cdb9ace536f2");
        internal static Guid RadiniumSASTabPluginId = new Guid("929c595c-c9db-4f43-b7af-4f98146419fd");
        internal static Guid RadiniumSASViewLayoutId = new Guid("03def28c-57c3-4914-a353-b716f952ebb9");
        // IMPORTANT! Due to shortcoming in Visual Studio template the below cannot be automatically replaced with proper unique GUIDs, so you will have to do it yourself
        internal static Guid RadiniumSASServerSettings = new Guid("bd99daa7-1eb2-47d2-a01d-6bdcc1eadaba");
        internal static Guid OptionsGuid = new Guid("cec02a6d-f95b-4985-8eaf-1e298b55c9ec");
        internal static Guid UserSettingsGuid = new Guid("f6ee14e6-77a2-4a4a-a720-152dffe66352");
        internal static Guid LockdownItemKind = new Guid("2316FE8B-0C89-4305-836D-32AFBC4B7DF8");
        internal static Guid EvacuationItemKind = new Guid("94273f72-b975-4f8a-8500-b824d4ab43b4");

        public const string RadiniumAccessAlarmStr = "c8771bc4-6b7e-40cb-b415-6e4373b48c9f";
        public const string RadiniumAccessStartOfficeModeStr = "2e932659-97cd-48cd-9e62-c0f13defeeb6";
        public const string RadiniumAccessEndOfficeModeStr = "49e206c2-ebc5-4352-a719-921f29a913e7";
        public const string RadiniumAccessPrivacyStartStr = "3a9a7823-4609-4c0f-8301-5cb7840b14e3";
        public const string RadiniumAccessPrivacyEndStr = "d7d11be0-086b-451a-8dd5-00488c5323fa";
        public const string RadiniumAccessAdminStr = "2684ed8c-a86e-4b7c-b425-b62b8afef791";
        public const string RadiniumAccessDeniedOpenStr = "d7c5c54e-5de3-4c8c-a0c3-725432c1a696";
        public const string RadiniumAccessDeniedCloseStr = "f1849cce-9542-4bfd-ad9a-8cbc561907a0";
        public const string RadiniumAccessCommsLostStr = "c799dda4-b1f9-4a58-b7cb-c9467c817cb8";
        public const string RadiniumAccessCommsReestablishedStr = "48b793dc-d2e8-41e1-af3d-94ac30ce3cf3";
        public const string RadiniumAccessHardwareFailStr = "be64b9bf-f2ef-4d3e-863f-cff8cf3bb1c1";
        public const string RadiniumAccessDoorOpenStr = "f20d885a-04a9-4c96-a13d-fe1a1b3bed92";
        public const string RadiniumAccessDoorCloseStr = "f52172dd-7bf0-4959-a1c4-ee8438a61e24";
        public const string RadiniumAccessLowBatteryStr = "c84b9a3e-7603-4ceb-a731-35e4f07f2515";

        public static readonly Guid RadiniumAccessAlarm = new Guid(RadiniumAccessAlarmStr);
        public static readonly Guid RadiniumAccessStartOfficeMode = new Guid(RadiniumAccessStartOfficeModeStr);
        public static readonly Guid RadiniumAccessEndOfficeMode = new Guid(RadiniumAccessEndOfficeModeStr);
        public static readonly Guid RadiniumAccessPrivacyStart = new Guid(RadiniumAccessPrivacyStartStr);
        public static readonly Guid RadiniumAccessPrivacyEnd = new Guid(RadiniumAccessPrivacyEndStr);
        public static readonly Guid RadiniumAccessAdmin = new Guid(RadiniumAccessAdminStr);
        public static readonly Guid RadiniumAccessDeniedOpen = new Guid(RadiniumAccessDeniedOpenStr);
        public static readonly Guid RadiniumAccessDeniedClose = new Guid(RadiniumAccessDeniedCloseStr);
        public static readonly Guid RadiniumAccessCommsLost = new Guid(RadiniumAccessCommsLostStr);
        public static readonly Guid RadiniumAccessCommsReestablished = new Guid(RadiniumAccessCommsReestablishedStr);
        public static readonly Guid RadiniumAccessHardwareFail = new Guid(RadiniumAccessHardwareFailStr);
        public static readonly Guid RadiniumAccessDoorOpen = new Guid(RadiniumAccessDoorOpenStr);
        public static readonly Guid RadiniumAccessDoorClose = new Guid(RadiniumAccessDoorCloseStr);
        public static readonly Guid RadiniumAccessLowBattery = new Guid(RadiniumAccessLowBatteryStr);



        public static readonly Guid RadiniumAccessGroup = new Guid("3855fd54-a9a6-4597-9650-29918118fb06");
        internal static String RadiniumSASPluginLicense = "Radinium SAS";
        public static readonly string LicenceRequestType = "SALTO";

        public const string RADINIUM_CAMERA1 = "Camera1";
        public const string RADINIUM_CAMERA2 = "Camera2";
        public const string RADINIUM_CAMERA3 = "Camera3";
        public const string RADINIUM_CAMERA4 = "Camera4";
        public const string RADINIUM_MAP = "LinkedMap";
        public const string RADINIUM_EXTDOORID = "ExtDoorID";
        public const string RADINIUM_ITEM_TYPE = "ItemType";
        public const string RADINIUM_ITEMTYPE_DOOR = "Door";
        public const string RADINIUM_ITEMTYPE_LOCKER = "Locker";
        public const string RADINIUM_ITEMTYPE_ROOM = "Room";
        public const string RADINIUM_LOCKDOWN_GROUP = "LockdownDoors";
        public const string RADINIUM_ONLINE_DOOR_TYPE = "OnlineDoorType";

        #region Private fields

        private UserControl _treeNodeInofUserControl;
        
        //
        // Note that all the plugin are constructed during application start, and the constructors
        // should only contain code that references their own dll, e.g. resource load.

        public const string RADINIUM_MESSAGE_NEW_AUDIT_EVENTS = "Radinium.Salto.AuditEvents";
        public const string RADINIUM_MESSAGE_NEW_EVENT = "Radinium.Salto.Event";
        public const string RADINIUM_MESSAGE_DOOR_STATUSES = "Radinium.Salto.DoorStatuses";
        public const string RADINIUM_MESSAGE_REQUEST_REPORT = "Radinium.Salto.RequestReport";
        public const string RADINIUM_MESSAGE_REQUEST_ITEMS = "Radinium.Salto.RequestItems";
        public const string RADINIUM_MESSAGE_RESPONSE_REPORT = "Radinium.Salto.ResponseReport";
        public const string RADINIUM_MESSAGE_RESPONSE_ITEMS = "Radinium.Salto.ResponseItems";
        public const string RADINIUM_MESSAGE_REQUEST_AUDIT = "Radinium.Salto.RequestAudit";
        public const string RADINIUM_MESSAGE_RESPONSE_AUDIT = "Radinium.Salto.ResponseAudit";
        public const string RADINIUM_MESSAGE_ANALYTICS_EVENT = "Radinium.Salto.Analytics";
        public const string RADINIUM_MESSAGE_OPEN_DOOR = "Radinium.Salto.OpenDoor";
        public const string RADINIUM_MESSAGE_CLOSE_DOOR = "Radinium.Salto.CloseDoor";
        public const string RADINIUM_MESSAGE_EMERGENCY_OPEN = "Radinium.Salto.LockdownOpen";
        public const string RADINIUM_MESSAGE_LOCKDOWN_CLOSE = "Radinium.Salto.LockdownClose";
        public const string RADINIUM_MESSAGE_LOCKDOWN_END = "Radinium.Salto.LockdownEnd";
        public const string RADINIUM_MESSAGE_EMERGENCY_STATE = "Radinium.Salto.EmergencyState";
        public const string RADINIUM_MESSAGE_LICENCE_INVALID = "Radinium.Salto.LicenceInvalid";

        public const string RADINIUM_API_REQUEST_USERS = "Radinium.Salto.RequestUsers";
        public const string RADINIUM_API_RESPONSE_USERS = "Radinium.Salto.ResponseUsers";
        public const string RADINIUM_API_REQUEST_PHOTO = "Radinium.Salto.RequestPhoto";
        public const string RADINIUM_API_RESPONSE_PHOTO = "Radinium.Salto.ResponsePhoto";
        public const string RADINIUM_API_UPDATEPHOTO = "Radinium.Salto.UpdatePhoto";
        public const string RADINIUM_API_UPDATEUSER = "Radinium.Salto.UpdateUser";

        public const string RADINUM_SECURITY_OPENCLOSE_DOOR = "GENERIC_WRITE";
        public const string RADINUM_SECURITY_OPENCLOSE_LOCKDOWN = "GENERIC_WRITE";

        private List<BackgroundPlugin> _backgroundPlugins = new List<BackgroundPlugin>();
        private Collection<SettingsPanelPlugin> _settingsPanelPlugins = new Collection<SettingsPanelPlugin>();
        private List<ViewItemPlugin> _viewItemPlugins = new List<ViewItemPlugin>();
        private List<ItemNode> _itemNodes = new List<ItemNode>();
        private List<SidePanelPlugin> _sidePanelPlugins = new List<SidePanelPlugin>();
        private List<String> _messageIdStrings = new List<string>() 
        { 
            RADINIUM_MESSAGE_NEW_AUDIT_EVENTS, 
            RADINIUM_MESSAGE_NEW_EVENT, 
            RADINIUM_MESSAGE_DOOR_STATUSES, 
            RADINIUM_MESSAGE_REQUEST_REPORT, 
            RADINIUM_MESSAGE_REQUEST_ITEMS, 
            RADINIUM_MESSAGE_RESPONSE_REPORT, 
            RADINIUM_MESSAGE_RESPONSE_ITEMS, 
            RADINIUM_MESSAGE_REQUEST_AUDIT, 
            RADINIUM_MESSAGE_RESPONSE_AUDIT, 
            RADINIUM_MESSAGE_ANALYTICS_EVENT,
            RADINIUM_MESSAGE_OPEN_DOOR,
            RADINIUM_MESSAGE_CLOSE_DOOR,
            RADINIUM_API_REQUEST_USERS,
            RADINIUM_API_RESPONSE_USERS,
            RADINIUM_API_REQUEST_PHOTO,
            RADINIUM_API_RESPONSE_PHOTO,
            RADINIUM_API_UPDATEPHOTO,
            RADINIUM_API_UPDATEUSER,
            RADINIUM_MESSAGE_EMERGENCY_OPEN,
            RADINIUM_MESSAGE_LOCKDOWN_CLOSE,
            RADINIUM_MESSAGE_LOCKDOWN_END,
            RADINIUM_MESSAGE_EMERGENCY_STATE,
            RADINIUM_MESSAGE_LICENCE_INVALID
        };
        private List<SecurityAction> _securityActions = new List<SecurityAction>()
        {
            new SecurityAction(RADINUM_SECURITY_OPENCLOSE_DOOR, "Open or close a Salto door")
        };
        private List<SecurityAction> _lockdownSecurityActions = new List<SecurityAction>()
        { 
            new SecurityAction(RADINUM_SECURITY_OPENCLOSE_LOCKDOWN, "Lockdown group emergency")
        };

        private List<WorkSpacePlugin> _workSpacePlugins = new List<WorkSpacePlugin>();
        private List<TabPlugin> _tabPlugins = new List<TabPlugin>();
        private List<ViewItemToolbarPlugin> _viewItemToolbarPlugins = new List<ViewItemToolbarPlugin>();
        private List<WorkSpaceToolbarPlugin> _workSpaceToolbarPlugins = new List<WorkSpaceToolbarPlugin>();
        private List<ToolsOptionsDialogPlugin> _toolsOptionsDialogPlugins = new List<ToolsOptionsDialogPlugin>();

        internal static LicenceValidator LicenceValidator = new RadiniumSASLicenceValidator();

        #endregion

        #region Initialization

        public override Collection<Guid> CustomSettingsGuids
        {
            get
            {
                return new Collection<Guid>() { OptionsGuid, RadiniumSASServerSettings, UserSettingsGuid };
            }
        }


        /// <summary>
        /// Load resources 
        /// </summary>
        static RadiniumSASDefinition()
        {
            _treeNodeImage = Properties.Resources.Door_Icon_Map_Small;
            _topTreeNodeImage = Properties.Resources.AccessLogo_vectorized_small;
            _evacuationImage = Properties.Resources.Evacuation_16;
            _lockdownImage = Properties.Resources.Lockdown_16;

            _doorIcon = ResourceUtility.ScaleImageToIconSizes(Properties.Resources.Door_Icon_Map);
        }


        /// <summary>
        /// Get the icon for the plugin
        /// </summary>
        internal static Image TreeNodeImage
        {
            get { return _treeNodeImage; }
        }

        #endregion
        private Collection<MapAlarmContextMenu> accessContextMenu = new Collection<MapAlarmContextMenu>() {
            new MapAlarmContextMenu("Open Door", RADINIUM_MESSAGE_OPEN_DOOR, null),
            new MapAlarmContextMenu("Close Door", RADINIUM_MESSAGE_CLOSE_DOOR, null),
        };
        /// <summary>
        /// This method is called when the environment is up and running.
        /// Registration of Messages via RegisterReceiver can be done at this point.
        /// </summary>
        public override void Init()
        {
            // Populate all relevant lists with your plugins etc.
            _itemNodes.Add(new ItemNode(RadiniumSASKind, Guid.Empty,
                                        "SALTO", _treeNodeImage,
                                        "Locks", _treeNodeImage,
                                        Category.Text, true,
                                        ItemsAllowed.Many,
                                        new RadiniumSASItemManager(RadiniumSASKind),
                                        null,
                                        _securityActions
                                        ) { MapIcon = _doorIcon });
            _itemNodes.Add(new ItemNode(LockdownItemKind, Guid.Empty, 
                "Lockdown Zone", _lockdownImage, 
                "Lockdown Zones", _lockdownImage, 
                Category.Text, true,
                ItemsAllowed.Many, 
                new LockdownItemManager(LockdownItemKind),
                null,
                _lockdownSecurityActions
                ));
            _itemNodes.Add(new ItemNode(EvacuationItemKind, Guid.Empty,
                "Evacuation Zone", _evacuationImage,
                "Evacuation Zones", _evacuationImage,
                Category.Text, true,
                ItemsAllowed.Many,
                new LockdownItemManager(EvacuationItemKind),
                null,
                _lockdownSecurityActions
                ));
            if (EnvironmentManager.Instance.EnvironmentType == EnvironmentType.SmartClient)
            {
                RadiniumSASWorkSpacePlugin plug = new RadiniumSASWorkSpacePlugin();
                _workSpacePlugins.Add(plug);
                _viewItemPlugins.Add(new RadiniumSASWorkSpaceViewItemPlugin(plug));
                _viewItemPlugins.Add(new RadiniumSASWorkSpaceCameraViewItemPlugin(plug));
            }
            if(EnvironmentManager.Instance.EnvironmentType == EnvironmentType.Service
                || EnvironmentManager.Instance.EnvironmentType == EnvironmentType.Standalone)
            {
                _backgroundPlugins.Add(new RadiniumSASBackgroundPlugin());
                _backgroundPlugins.Add(new RadiniumSaltoItemUpdater());
                _backgroundPlugins.Add(new RadiniumSaltoStateUpdater());
                _backgroundPlugins.Add(new QueryResponder());
            }
            
        }

        /// <summary>
        /// The main application is about to be in an undetermined state, either logging off or exiting.
        /// You can release resources at this point, it should match what you acquired during Init, so additional call to Init() will work.
        /// </summary>
        public override void Close()
        {
            _itemNodes.Clear();
            _sidePanelPlugins.Clear();
            _viewItemPlugins.Clear();
            _settingsPanelPlugins.Clear();
            _backgroundPlugins.Clear();
            _workSpacePlugins.Clear();
            _tabPlugins.Clear();
            _viewItemToolbarPlugins.Clear();
            _workSpaceToolbarPlugins.Clear();
            _toolsOptionsDialogPlugins.Clear();
        }

        private RadiniumSASActionManager actionManager = new RadiniumSASActionManager();

        public override ActionManager ActionManager 
        {
            get
            {
                return actionManager;
            }
        }

        /// <summary>
        /// Return any new messages that this plugin can use in SendMessage or PostMessage,
        /// or has a Receiver set up to listen for.
        /// The suggested format is: "YourCompany.Area.MessageId"
        /// </summary>
        public override List<string> PluginDefinedMessageIds
        {
            get
            {
                return _messageIdStrings;
            }
        }

        /// <summary>
        /// If authorization is to be used, add the SecurityActions the entire plugin 
        /// would like to be available.  E.g. Application level authorization.
        /// </summary>
        public override List<SecurityAction> SecurityActions
        {
            get
            {
                return _securityActions;
            }
            set
            {
            }
        }

        #region Identification Properties

        /// <summary>
        /// Gets the unique id identifying this plugin component
        /// </summary>
        public override Guid Id
        {
            get
            {
                return RadiniumSASPluginId;
            }
        }

        /// <summary>
        /// This Guid can be defined on several different IPluginDefinitions with the same value,
        /// and will result in a combination of this top level ProductNode for several plugins.
        /// Set to Guid.Empty if no sharing is enabled.
        /// </summary>
        public override Guid SharedNodeId
        {
            get
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Define name of top level Tree node - e.g. A product name
        /// </summary>
        public override string Name
        {
            get { return "Radinium Access"; }
        }

        /// <summary>
        /// Your company name
        /// </summary>
        public override string Manufacturer
        {
            get
            {
                return "Radinium (Pty) Ltd";
            }
        }

        /// <summary>
        /// Version of this plugin.
        /// </summary>
        public override string VersionString
        {
            get
            {
                return "2.0.0.1";
            }
        }

        /// <summary>
        /// Icon to be used on top level - e.g. a product or company logo
        /// </summary>
        public override System.Drawing.Image Icon
        {
            get { return _topTreeNodeImage; }
        }

        #endregion


        #region Administration properties

        /// <summary>
        /// A list of server side configuration items in the administrator
        /// </summary>
        public override List<ItemNode> ItemNodes
        {
            get { return _itemNodes; }
        }

        /// <summary>
        /// An extension plug-in running in the Administrator to add a tab for built-in devices and hardware.
        /// </summary>
        public override ICollection<TabPlugin> TabPlugins
        {
            get { return _tabPlugins; }
        }

        /// <summary>
        /// An extension plug-in running in the Administrator to add more tabs to the Tools-Options dialog.
        /// </summary>
        public override List<ToolsOptionsDialogPlugin> ToolsOptionsDialogPlugins
        {
            get { return _toolsOptionsDialogPlugins; }
        }

        /// <summary>
        /// A user control to display when the administrator clicks on the top TreeNode
        /// </summary>
        public override UserControl GenerateUserControl()
        {
            _treeNodeInofUserControl = new HelpPage();
            return _treeNodeInofUserControl;
        }

        /// <summary>
        /// This property can be set to true, to be able to display your own help UserControl on the entire panel.
        /// When this is false - a standard top and left side is added by the system.
        /// </summary>
        public override bool UserControlFillEntirePanel
        {
            get { return true; }
        }
        #endregion

        #region Client related methods and properties

        /// <summary>
        /// A list of Client side definitions for Smart Client
        /// </summary>
        public override List<ViewItemPlugin> ViewItemPlugins
        {
            get { return _viewItemPlugins; }
        }

        /// <summary>
        /// An extension plug-in running in the Smart Client to add more choices on the Settings panel.
        /// Supported from Smart Client 2017 R1. For older versions use OptionsDialogPlugins instead.
        /// </summary>
        public override Collection<SettingsPanelPlugin> SettingsPanelPlugins
        {
            get { return _settingsPanelPlugins; }
        }

        /// <summary> 
        /// An extension plugin to add to the side panel of the Smart Client.
        /// </summary>
        public override List<SidePanelPlugin> SidePanelPlugins
        {
            get { return _sidePanelPlugins; }
        }

        /// <summary>
        /// Return the workspace plugins
        /// </summary>
        public override List<WorkSpacePlugin> WorkSpacePlugins
        {
            get { return _workSpacePlugins; }
        }

        /// <summary> 
        /// An extension plug-in to add to the view item toolbar in the Smart Client.
        /// </summary>
        public override List<ViewItemToolbarPlugin> ViewItemToolbarPlugins
        {
            get { return _viewItemToolbarPlugins; }
        }

        /// <summary> 
        /// An extension plug-in to add to the work space toolbar in the Smart Client.
        /// </summary>
        public override List<WorkSpaceToolbarPlugin> WorkSpaceToolbarPlugins
        {
            get { return _workSpaceToolbarPlugins; }
        }

        #endregion


        /// <summary>
        /// Create and returns the background task.
        /// </summary>
        public override List<BackgroundPlugin> BackgroundPlugins
        {
            get { return _backgroundPlugins; }
        }

    }

    public class RadiniumSASLicenceValidator : LicenceValidator
    {
        public RadiniumSASLicenceValidator() : base(typeof(RadiniumSASDefinition), new RadiniumSASFingerprintUtil(), RadiniumSASDefinition.RadiniumSASPluginId, RadiniumSASDefinition.RadiniumSASPluginLicense)
        {
        }

        public override string RetrieveLicenceConfiguration()
        {
            return Utility.ReadLicence();
        }

        public override void SaveLicenceConfiguration(string licenseNode)
        {
            Utility.SaveConfiguration(licenseNode);
        }
    }
}
