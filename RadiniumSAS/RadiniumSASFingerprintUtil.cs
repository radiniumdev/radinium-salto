﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cygnetic.MIPPluginCommons.Licence;
using Cygnetic.MIPPluginCommons.Util;
using Newtonsoft.Json;
using RadiniumSAS;
using VideoOS.Platform;
using VideoOS.Platform.ConfigurationItems;
using VideoOS.Platform.Messaging;

namespace RadiniumSAS
{
    public class RadiniumSASFingerprintUtil : FingerprintUtil
    {
        public override string GenerateFingerprint(object[] parts)
        {
            string fullIdentifier = CpuId() + ":" + BiosId() + ":" + MotherboardId();
            return Base64(Base64(RadiniumSASDefinition.LicenceRequestType) + ":" + Base64(fullIdentifier));

        }

    }
}
