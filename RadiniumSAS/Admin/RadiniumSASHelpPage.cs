using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VideoOS.Platform;
using VideoOS.Platform.Admin;
using Cygnetic.MIPPluginCommons.Licence;
using static RadiniumSAS.Admin.Utility;
using System.Xml.Linq;
using System.Xml;
using RadiniumSAS.Data;
using System.Data.SqlClient;
using System.IO;
using RadiniumSAS.Background;
using System.Threading;
using Cygnetic.MIPPluginCommons.Data;
using VideoOS.Platform.Messaging;
using System.Windows.Threading;
using System.Xml.Serialization;
using VideoOS.Platform.Util;

namespace RadiniumSAS.Admin
{
    public partial class HelpPage : ItemNodeUserControl
    {
        List<Entry> entries = Utility.GetEntries();
        /// <summary>
        /// User control to display help page
        /// </summary>	
        /// 

        private List<SaltoDbUser> userList;
        private DatabaseCredentialsUserControl dbCred = null;
        public HelpPage()
        {
            InitializeComponent();

            MessageCommunicationManager.Start(EnvironmentManager.Instance.CurrentSite.ServerId);
            comms = MessageCommunicationManager.Get(EnvironmentManager.Instance.CurrentSite.ServerId);
        }

        private void LicensingUserControl1_SaveLicenceSettings(object sender, EventArgs e)
        {
            Utility.SaveLicensingApiKey(licensingUserControl1.ApiKey);
        }


        private volatile MessageCommunication comms;

        private volatile bool quitting = false;

        private object photoResponse, userResponse;

        /// <summary>
        /// Display information from or about the Item selected.
        /// </summary>
        /// <param name="item"></param>
        /// 
        public class LockSelection
        { 
            public static LockSelection convertItemToLock(Item item)
            {
                LockSelection selection = new LockSelection();
                if(!item.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ITEM_TYPE))
                {
                    item.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] = RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR;
                }
                if(!item.Properties.ContainsKey("ExtDoorID"))
                {
                    return null;
                }
                selection.ExtDoorID = item.Properties["ExtDoorID"];
                selection.LockType = item.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE];
                if(selection.LockType == RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR)
                {
                    selection.OriginalEntity = SaltoDbDoor.ConvertItemToDoor(item);
                    selection.Name = item.Name;
                }
                if (selection.LockType == RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER)
                {
                    selection.OriginalEntity = SaltoDbLocker.ConvertItemToLocker(item);
                    selection.Name = item.Name;
                }
                if (selection.LockType == RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM)
                {
                    selection.OriginalEntity = SaltoDbRoom.ConvertItemToRoom(item);
                    selection.Name = item.Name;
                }
                return selection;
            }

            public override bool Equals(object obj)
            {
                if(obj == null || obj.GetType() != typeof(LockSelection))
                {
                    return false;
                }
                return ExtDoorID == ((LockSelection)obj).ExtDoorID;
            }

            public override int GetHashCode()
            {
                return ExtDoorID.GetHashCode();
            }

            public override string ToString()
            {
                return Name;
            }

            public static LockSelection convertDoorToLock(SaltoDbDoor door)
            {
                LockSelection selection = new LockSelection();
                selection.ExtDoorID = door.ExtDoorID;
                selection.Name = "(Door) " + door.Name;
                selection.OriginalEntity = door;
                selection.LockType = RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR;
                return selection;
            }

            public static LockSelection convertLockerToLock(SaltoDbLocker locker)
            {
                LockSelection selection = new LockSelection();
                selection.ExtDoorID = locker.ExtDoorID;
                selection.Name = "(Locker) " + locker.Name;
                selection.OriginalEntity = locker;
                selection.LockType = RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER;
                return selection;
            }

            public static LockSelection convertRoomToLock(SaltoDbRoom room)
            {
                LockSelection selection = new LockSelection();
                selection.ExtDoorID = room.ExtDoorID;
                selection.Name = "(Room) " + room.Name;
                selection.OriginalEntity = room;
                selection.LockType = RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM;
                return selection;
            }

            public string ExtDoorID { get; set; }
            public string Name { get; set; }
            public string LockType { get; set; }

            public object OriginalEntity { get; set; }
        }

        private List<LockSelection> allDoorsList = new List<LockSelection>();
        private List<LockSelection> leftDoors = new List<LockSelection>();
        private List<LockSelection> rightDoors = new List<LockSelection>();

        XmlSerializer doorSerializer = new XmlSerializer(typeof(SaltoDbDoor));
        XmlSerializer lockerSerializer = new XmlSerializer(typeof(SaltoDbLocker));
        XmlSerializer roomSerializer = new XmlSerializer(typeof(SaltoDbRoom));

        private void UpdateDisplay()
        {
            Dispatcher.CurrentDispatcher.Invoke(() => {
                string filter = textBoxDoorSearch.Text.ToLowerInvariant();
                List<LockSelection> selectedItems = new List<LockSelection>();
                List<LockSelection> unselectedItems = new List<LockSelection>();
                List<LockSelection> leftItems = new List<LockSelection>();
                List<LockSelection> rightItems = new List<LockSelection>();
                Point prevScrollSelected = listBoxSelectedDoors.AutoScrollOffset;
                Point prevScrollUnselected = listBoxUnselectedDoors.AutoScrollOffset;

                for(int i = 0; i < listBoxUnselectedDoors.SelectedItems.Count; i++)
                {
                    unselectedItems.Add(listBoxUnselectedDoors.SelectedItems[i] as LockSelection);
                }

                for (int i = 0; i < listBoxSelectedDoors.SelectedItems.Count; i++)
                {
                    selectedItems.Add(listBoxSelectedDoors.SelectedItems[i] as LockSelection);
                }

                for (int i = 0; i < listBoxUnselectedDoors.Items.Count; i++)
                {
                    leftItems.Add(listBoxUnselectedDoors.Items[i] as LockSelection);
                }

                for (int i = 0; i < listBoxSelectedDoors.Items.Count; i++)
                {
                    rightItems.Add(listBoxSelectedDoors.Items[i] as LockSelection);
                }
                IEnumerable<LockSelection> filteredRight = rightDoors.Where(x => x.ToString().ToLowerInvariant().Contains(filter)).OrderBy(x => x.ToString().ToLowerInvariant());
                IEnumerable<LockSelection> filteredLeft = leftDoors.Where(x => x.ToString().ToLowerInvariant().Contains(filter)).OrderBy(x => x.ToString().ToLowerInvariant()); 
                if (rightItems.Count != filteredRight.Count() || leftItems.Count != filteredLeft.Count())
                {
                    listBoxUnselectedDoors.SelectedItems.Clear();

                    listBoxUnselectedDoors.DataSource = filteredLeft.ToList();
                    listBoxSelectedDoors.DataSource = filteredRight.ToList();
                    
                    foreach (LockSelection unselectedItem in unselectedItems)
                    {
                        if (filteredLeft.Contains(unselectedItem))
                        {
                            listBoxUnselectedDoors.SelectedItems.Add(unselectedItem);
                        }
                    }

                    foreach (LockSelection selectedItem in selectedItems)
                    {
                        if (filteredRight.Contains(selectedItem))
                        {
                            listBoxSelectedDoors.SelectedItems.Add(selectedItem);
                        }
                    }
                    listBoxSelectedDoors.AutoScrollOffset = prevScrollSelected;
                    listBoxUnselectedDoors.AutoScrollOffset = prevScrollUnselected;
                }

                int numLicences = 0;
                if (licensingUserControl1.Licensed.Count > 0)
                {
                    numLicences = licensingUserControl1.Licensed[0].Licence.NumLicences;
                }
                int licenced = rightDoors.Count;

                labelLicences.Text = licenced.ToString() + " / " + numLicences.ToString();

            });
        }

        private object ReceiveItemInfo(VideoOS.Platform.Messaging.Message msg, FQID destination, FQID source)
        {
            string filter = textBoxDoorSearch.Text.ToLowerInvariant();

            if(msg.Data as List<string>[] == null || (msg.Data as List<string>[]).Length != 4)
            {
                return null;
            }
            List<string> userList = (msg.Data as List<string>[])[0];
            List<string> doorsList = (msg.Data as List<string>[])[1];
            List<string> lockersList = (msg.Data as List<string>[])[2];
            List<string> roomsList = (msg.Data as List<string>[])[3];

            List<SaltoDbDoor> completeDoorsList = new List<SaltoDbDoor>();
            List<SaltoDbLocker> completeLockersList = new List<SaltoDbLocker>();
            List<SaltoDbRoom> completeRoomsList = new List<SaltoDbRoom>();

            foreach (var doorStr in doorsList)
            {
                SaltoDbDoor door = doorSerializer.Deserialize(new StringReader(doorStr)) as SaltoDbDoor;
                completeDoorsList.Add(door);
                if (!allDoorsList.Any(x => x.ExtDoorID == door.ExtDoorID))
                {
                    LockSelection doorLock = LockSelection.convertDoorToLock(door);
                    allDoorsList.Add(doorLock);
                    leftDoors.Add(doorLock);
                }
            }

            foreach (var lockerStr in lockersList)
            {
                SaltoDbLocker locker = lockerSerializer.Deserialize(new StringReader(lockerStr)) as SaltoDbLocker;
                completeLockersList.Add(locker);
                if (!allDoorsList.Any(x => x.ExtDoorID == locker.ExtDoorID))
                {
                    LockSelection lockerLock = LockSelection.convertLockerToLock(locker);
                    allDoorsList.Add(lockerLock);
                    leftDoors.Add(lockerLock);
                }
            }

            foreach (var roomStr in roomsList)
            {
                SaltoDbRoom room = roomSerializer.Deserialize(new StringReader(roomStr)) as SaltoDbRoom;
                completeRoomsList.Add(room);
                if (!allDoorsList.Any(x => x.ExtDoorID == room.ExtDoorID))
                {
                    LockSelection roomLock = LockSelection.convertRoomToLock(room);
                    allDoorsList.Add(roomLock);
                    leftDoors.Add(roomLock);
                }
            }

            foreach (LockSelection allDoor in allDoorsList)
            {
                if(completeDoorsList.Any(x => x.ExtDoorID == allDoor.ExtDoorID))
                {
                    completeDoorsList.Remove(completeDoorsList.First(y => y.ExtDoorID == allDoor.ExtDoorID));
                }
                if (completeLockersList.Any(x => x.ExtDoorID == allDoor.ExtDoorID))
                {
                    completeLockersList.Remove(completeLockersList.First(y => y.ExtDoorID == allDoor.ExtDoorID));
                }
                if (completeRoomsList.Any(x => x.ExtDoorID == allDoor.ExtDoorID))
                {
                    completeRoomsList.Remove(completeRoomsList.First(y => y.ExtDoorID == allDoor.ExtDoorID));
                }
            }

            foreach (SaltoDbDoor removedDoor in completeDoorsList)
            {
                LockSelection left = leftDoors.FirstOrDefault(x => x.ExtDoorID == removedDoor.ExtDoorID);
                LockSelection right = rightDoors.FirstOrDefault(x => x.ExtDoorID == removedDoor.ExtDoorID);
                if(left != null)
                {
                    leftDoors.Remove(left);
                }
                if(right != null)
                {
                    rightDoors.Remove(right);
                }
            }

            foreach (SaltoDbLocker removedDoor in completeLockersList)
            {
                LockSelection left = leftDoors.FirstOrDefault(x => x.ExtDoorID == removedDoor.ExtDoorID);
                LockSelection right = rightDoors.FirstOrDefault(x => x.ExtDoorID == removedDoor.ExtDoorID);
                if (left != null)
                {
                    leftDoors.Remove(left);
                }
                if (right != null)
                {
                    rightDoors.Remove(right);
                }
            }

            foreach (SaltoDbRoom removedDoor in completeRoomsList)
            {
                LockSelection left = leftDoors.FirstOrDefault(x => x.ExtDoorID == removedDoor.ExtDoorID);
                LockSelection right = rightDoors.FirstOrDefault(x => x.ExtDoorID == removedDoor.ExtDoorID);
                if (left != null)
                {
                    leftDoors.Remove(left);
                }
                if (right != null)
                {
                    rightDoors.Remove(right);
                }
            }


            leftDoors.Sort(Comparer<LockSelection>.Create((doorA, doorB) => { return doorA.ToString().CompareTo(doorB.ToString()); }));
            rightDoors.Sort(Comparer<LockSelection>.Create((doorA, doorB) => { return doorA.ToString().CompareTo(doorB.ToString()); }));

            UpdateDisplay();

            return null;
        }

        public void FillDoorsLists()
        {
            object commsFilterItems = comms.RegisterCommunicationFilter(ReceiveItemInfo, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_MESSAGE_RESPONSE_ITEMS));
            while (!quitting && (leftDoors == null || leftDoors.Count <= 0))
            {
                Thread.Sleep(5000);
                comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_MESSAGE_REQUEST_ITEMS), null, null, null);
            }
            comms.UnRegisterCommunicationFilter(commsFilterItems);
        }

        public override void Init(Item item)
        {
            List<Item> currentItems = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind);
            foreach(Item salto in currentItems)
            {
                LockSelection doorItem = LockSelection.convertItemToLock(salto);
                rightDoors.Add(doorItem);
                allDoorsList.Add(doorItem);
            }

            listBoxUnselectedDoors.DataSource = leftDoors;
            listBoxSelectedDoors.DataSource = rightDoors;

            if (licensingUserControl1 == null)
            {
                licensingUserControl1 = new LicensingUserControl(RadiniumSASDefinition.LicenceValidator);
                this.licensingUserControl1.Location = new System.Drawing.Point(6, 6);
                this.licensingUserControl1.Name = "licensingUserControl1";
                this.licensingUserControl1.Size = new System.Drawing.Size(461, 517);
                this.licensingUserControl1.TabIndex = 32;

                licensingUserControl1.ApiKey = Utility.ReadLicensingApiKey();
                licensingUserControl1.LicenceRequestType = RadiniumSASDefinition.LicenceRequestType;
                licensingUserControl1.SaveLicenceSettings += LicensingUserControl1_SaveLicenceSettings;

                licensingUserControl1.Licencables = new List<Licencable>();

                licensingUserControl1.Licencables.Add(new Licencable(EnvironmentManager.Instance.SystemLicense.SLC, new object[] { }));

                this.Licensing.Controls.Add(this.licensingUserControl1);
                licensingUserControl1.Update();
            }

            UpdateDisplay();

            if (dbCred == null)
            {
                dbCred = new DatabaseCredentialsUserControl();
                dbCred.Username = Utility.ReadDBUsername();
                dbCred.Password = Utility.ReadDBPassword();
                dbCred.Catalogue = Utility.ReadDBCatalogue();
                dbCred.DataSource = Utility.ReadDBDataSource();
                dbCred.Top = 5;
                panelDbCred.Controls.Add(dbCred);
            }

            userResponse = comms.RegisterCommunicationFilter(ReceiveUserInfo, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_API_RESPONSE_USERS));
            photoResponse = comms.RegisterCommunicationFilter(ReceiveUserPhoto, new CommunicationIdFilter(RadiniumSASDefinition.RADINIUM_API_RESPONSE_PHOTO));

            ThreadPool.QueueUserWorkItem((x) => {

                Thread.Sleep(7000);
                while (!quitting && (userList == null || userList.Count <= 0))
                {
                    comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_API_REQUEST_USERS), null, null, null);
                    Thread.Sleep(5000);
                }
            });

            SaltoUtility.UpdateConfig(Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.RadiniumSASServerSettings, false));

            textBoxShipHost.Text = SaltoUtility.ServerHost;
            textBoxShipPort.Text = SaltoUtility.ServerPort.ToString();
            textBoxSHIPKey.Text = SaltoUtility.ShipKey;
            if(SaltoUtility.Protocol.StartsWith("HTTPS"))
            {
                comboBoxProtocol.SelectedIndex = 2;
            }
            else if (SaltoUtility.Protocol.StartsWith("HTTP"))
            {
                comboBoxProtocol.SelectedIndex = 1;
            }
            else
            {
                comboBoxProtocol.SelectedIndex = 0;
            }
            ThreadPool.QueueUserWorkItem((x) => { FillDoorsLists(); });
        }
        /// <summary>
        /// Close any session and release any resources used.
        /// </summary>
        public override void Close()
        {
            quitting = true;
            comms.UnRegisterCommunicationFilter(photoResponse);
            comms.UnRegisterCommunicationFilter(userResponse);
            MessageCommunicationManager.Stop(EnvironmentManager.Instance.CurrentSite.ServerId);
        }

        const string AuditTrailRequestXml = @"<?xml version='1.0' encoding='UTF-8'?>
<RequestCall>
    <RequestName>SaltoDBAuditTrail.Read</RequestName>
    <Params>
        <MaxCount>2</MaxCount>
        <ShowDoorIDAs>0</ShowDoorIDAs>
        <ShowUserIDAs>0</ShowUserIDAs>
        <DescendingOrder>1</DescendingOrder>
        <ShowTimeInUTC>1</ShowTimeInUTC>
    </Params>
</RequestCall>";

        private bool VerifyAuditTrailRequest()
        {
            string Protocol;
            if ((comboBoxProtocol.SelectedItem as string).StartsWith("HTTPS"))
            {
                Protocol = "HTTPS";
            }
            else if ((comboBoxProtocol.SelectedItem as string).StartsWith("HTTP"))
            {
                Protocol = "HTTP";
            }
            else
            {
                Protocol = "STP";
            }
            int port;
            if (int.TryParse(textBoxShipPort.Text, out port))
            {
                if (port < 1 || port > 32767)
                {
                    MessageBox.Show("Please use a port number between 1 and 32768");
                    return false;
                }
            }
            byte[] data = SaltoUtility.SendSaltoRequest(textBoxShipHost.Text, port, Protocol, textBoxSHIPKey.Text, UTF8Encoding.UTF8.GetBytes(AuditTrailRequestXml));
            if (data != null)
            {
                try
                {
                    string dataStr = UTF8Encoding.UTF8.GetString(data);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(dataStr);
                    XmlNodeList children = doc.SelectSingleNode("//SaltoDBAuditTrail").ChildNodes;

                    for (int i = 0; i < children.Count; i++)
                    {
                        XmlNode currentChild = children.Item(i);
                        if (currentChild.NodeType == XmlNodeType.Element)
                        {
                            SaltoAuditEntry entry = SaltoAuditEntry.ParseFromXmlNode(currentChild);
                            if (entry == null)
                            {
                                return false;
                            }
                            return true;
                        }
                    }
                    return true;
                }
                catch (Exception e)
                {
                }
            }
            return false;
        }

        private void SaveShipSettings_Click(object sender, EventArgs e)
        {
            string shipApiKey = textBoxSHIPKey.Text;
            string Protocol;
            if ((comboBoxProtocol.SelectedItem as string).StartsWith("HTTPS"))
            {
                Protocol = "HTTPS";
            }
            else if ((comboBoxProtocol.SelectedItem as string).StartsWith("HTTP"))
            {
                Protocol = "HTTP";
            }
            else
            {
                Protocol = "STP";
            }

            int port;
            if (int.TryParse(textBoxShipPort.Text, out port))
            {
                if(port < 1 || port > 32767)
                {
                    MessageBox.Show("Please use a port number between 1 and 32768.");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please use a number for the SHIP port.");
                return;
            }
            if (VerifyAuditTrailRequest())
            {
                MessageBox.Show("Successfully verified SHIP connection.");
            }
            else
            {
                MessageBox.Show("SHIP connection did not work, not saving configuration.");
                return;
            }
            XDocument doc = XDocument.Parse("<ROOT></ROOT>");
            XElement shipHost = new XElement(XName.Get("SaltoShipHost"));
            shipHost.SetValue(textBoxShipHost.Text);
            XElement shipPort = new XElement(XName.Get("SaltoShipPort"));
            shipPort.SetValue(textBoxShipPort.Text);
            XElement shipProtocol = new XElement(XName.Get("SaltoProtocol"));
            shipProtocol.SetValue(Protocol);
            XElement shipKey = new XElement(XName.Get("SaltoKey"));
            shipKey.SetValue(shipApiKey);
            doc.Root.Add(shipHost);
            doc.Root.Add(shipPort);
            doc.Root.Add(shipProtocol);
            doc.Root.Add(shipKey);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(doc.ToString());
            Configuration.Instance.SaveOptionsConfiguration(RadiniumSASDefinition.RadiniumSASServerSettings, false, xmlDoc);
            MessageBox.Show("Saved settings!");
        }

        private void buttonSavePicture_Click(object sender, EventArgs e)
        {
            SaltoDbUser selectedUser = null;
            if (listBoxUsers.SelectedIndex >= 0)
            {
                selectedUser = listBoxUsers.Items[listBoxUsers.SelectedIndex] as SaltoDbUser;
            }
            else
            {
                MessageBox.Show("Please select a cardholder before uploading a picture");
            }
            if(selectedUser == null)
            {
                return;
            }
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.DefaultExt = "png";
            fileDialog.Filter = "PNG image (*.png)|*.png|JPG image (*.jpg)|*.jpg|JPEG image|*.jpeg";
            fileDialog.Title = "Save report";
            fileDialog.CheckPathExists = true;
            fileDialog.CheckFileExists = true;

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if(new FileInfo(fileDialog.FileName).Length > 524288)
                    {
                        MessageBox.Show("The file is too large! Please limit to 512KB maximum.");
                        return;
                    }
                    byte[] data = File.ReadAllBytes(fileDialog.FileName);
                    selectedUser.Photo = data;
                    if (selectedUser.Photo == null)
                    {
                        pictureBoxCardholder.Image = null;
                    }
                    else
                    {
                        try
                        {
                            pictureBoxCardholder.Image = Image.FromStream(new MemoryStream(selectedUser.Photo));
                        }
                        catch (Exception)
                        {

                        }
                    }
                    comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_API_UPDATEPHOTO, new byte[][] { UTF8Encoding.UTF8.GetBytes(selectedUser.ExtUserID), data }), null, null, null);
                }
                catch (Exception)
                {

                }
                
            }
        }

        private volatile bool suppressSave = false;

        private void listBoxUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBoxCardholder.Image = null;
            labelCardholderID.Text = "";
            if (listBoxUsers.SelectedIndex >= 0)
            {
                SaltoDbUser selectedUser = listBoxUsers.Items[listBoxUsers.SelectedIndex] as SaltoDbUser;
                if (selectedUser != null)
                {
                    ThreadPool.QueueUserWorkItem(x =>
                    {
                        comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_API_REQUEST_PHOTO, selectedUser.ExtUserID), null, null, null);
                    });
                    suppressSave = true;
                    labelCardholderID.Text = selectedUser.Title + " " + selectedUser.LastName + ", " + selectedUser.FirstName;
                    txtContact.Text = selectedUser.PhoneNumber;
                    txtDepartment.Text = selectedUser.GPF1;
                    txtDescription.Text = selectedUser.GPF2;
                    if (selectedUser.Photo != null && selectedUser.Photo.Length > 0)
                    {
                        try
                        {
                            pictureBoxCardholder.Image = Image.FromStream(new MemoryStream(selectedUser.Photo));
                        }
                        catch (Exception)
                        {
                        }
                    }
                    suppressSave = false;
                }
            }
        }

        private void buttonDeletePicture_Click(object sender, EventArgs e)
        {
            SaltoDbUser selectedUser = null;
            if (listBoxUsers.SelectedIndex >= 0)
            {
                selectedUser = listBoxUsers.Items[listBoxUsers.SelectedIndex] as SaltoDbUser;
            }
            else
            {
                MessageBox.Show("Please select a cardholder before deleting their picture");
            }
            if (selectedUser == null)
            {
                return;
            }

            ThreadPool.QueueUserWorkItem(x =>
            {
                comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_API_UPDATEPHOTO, new byte[][] { UTF8Encoding.UTF8.GetBytes(selectedUser.ExtUserID), null }), null, null, null);
            });
            selectedUser.Photo = null;
        }

        private void saveText()
        {
            if(suppressSave)
            {
                return;
            }

            SaltoDbUser selectedUser = null;
            if (listBoxUsers.SelectedIndex >= 0)
            {
                selectedUser = listBoxUsers.Items[listBoxUsers.SelectedIndex] as SaltoDbUser;
            }
            if (selectedUser == null)
            {
                return;
            }
            selectedUser.PhoneNumber = txtContact.Text;
            selectedUser.GPF1 = txtDepartment.Text;
            selectedUser.GPF2 = txtDescription.Text;
            ThreadPool.QueueUserWorkItem(x =>
            {
                comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_API_UPDATEUSER, new string[] { selectedUser.ExtUserID, txtContact.Text, txtDepartment.Text, txtDescription.Text }), null, null, null);
            });
            MessageBox.Show("Saved user details");
        }

        private void txtDepartment_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtContact_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            int desiredLines = 3;
            using (Graphics g = txtDescription.CreateGraphics())
            {
                int lineHeight = TextRenderer.MeasureText(g, "abcdefghijklmnopqrstuvfxyz", txtDescription.Font).Height;
                int currentHeight = TextRenderer.MeasureText(g, txtDescription.Text, txtDescription.Font).Height;

                while (currentHeight > desiredLines * lineHeight)
                {
                    txtDescription.Text = txtDescription.Text.Remove(txtDescription.Text.Length - 1);
                    currentHeight = TextRenderer.MeasureText(g, txtDescription.Text, txtDescription.Font).Height;
                }
            }
        }

        private void cardholderTextBox_TextChanged(object sender, EventArgs e)
        {
            string textSearchLower = cardholderTextBox.Text.ToLower();
            if(userList != null)
            {
                listBoxUsers.DataSource = userList.Where(x => (x.Title + " " + x.FirstName + " " + x.LastName).ToLower().Contains(textSearchLower)).OrderBy(X => X.LastName).ThenBy(x => x.FirstName).ToList();
            }
        }

        private bool successfulUserInfo = false;
        
        private object ReceiveUserInfo(VideoOS.Platform.Messaging.Message msg, FQID destination, FQID source)
        {
            SaltoDbUser[] saltoUsers = msg.Data as SaltoDbUser[];
            if(saltoUsers == null)
            {
                MessageBox.Show("Database credentials invalid. Please verify server details and credentials.");
            }
            successfulUserInfo = true;

            userList = saltoUsers.ToList();
            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                string textSearchLower = cardholderTextBox.Text.ToLower();
                if (string.IsNullOrWhiteSpace(textSearchLower))
                {
                    listBoxUsers.DataSource = userList.Where(x => (x.Title + " " + x.FirstName + " " + x.LastName).ToLower().Contains(textSearchLower)).OrderBy(X => X.LastName).ThenBy(x => x.FirstName).ToList();
                }
                else
                {
                    listBoxUsers.DataSource = userList.OrderBy(X => X.LastName).ThenBy(x => x.FirstName).ToList();
                }
            });

            return null;
        }

        private object ReceiveUserPhoto(VideoOS.Platform.Messaging.Message msg, FQID destination, FQID source)
        {
            byte[][] payload = msg.Data as byte[][];
            if(payload.Length != 2)
            {
                return null;
            }
            string extUserId = UTF8Encoding.UTF8.GetString(payload[0]);
            userList.Where(x => x.ExtUserID == extUserId).ToList().ForEach(x => x.Photo = payload[1]);
            
            if(payload[1] != null && payload[1].Length > 0)
            {
                Dispatcher.CurrentDispatcher.Invoke(() =>
                {
                    try
                    {
                        SaltoDbUser selectedUser = null;
                        if (listBoxUsers.SelectedIndex >= 0)
                        {
                            selectedUser = listBoxUsers.Items[listBoxUsers.SelectedIndex] as SaltoDbUser;
                        }
                        if (selectedUser == null)
                        {
                            return;
                        }
                        if(selectedUser.ExtUserID == extUserId)
                        {
                            pictureBoxCardholder.Image = Image.FromStream(new MemoryStream(payload[1]));
                        }
                    }
                    catch (Exception)
                    {
                        pictureBoxCardholder.Image = null;
                    }
                });

            }
            return null;
        }

        private bool refreshNotificationSent = false;

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (!refreshNotificationSent)
            {
                MessageBox.Show("Please remember to refresh the management client after selecting the licenced doors.");
                refreshNotificationSent = true;
            }
            List<SaltoDbDoor> toSaveDoorList = new List<SaltoDbDoor>();
            List<SaltoDbLocker> toSaveLockerList = new List<SaltoDbLocker>();
            List<SaltoDbRoom> toSaveRoomList = new List<SaltoDbRoom>();

            for (int i = 0; i < listBoxUnselectedDoors.SelectedItems.Count; i++)
            {
                LockSelection newDoor = (listBoxUnselectedDoors.SelectedItems[i] as LockSelection);
                if(newDoor == null)
                {
                    continue;
                }
                rightDoors.Add(newDoor);
                leftDoors.Remove(newDoor);
                switch (newDoor.LockType)
                {
                    case RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR:
                        toSaveDoorList.Add(newDoor.OriginalEntity as SaltoDbDoor);
                        break;
                    case RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER:
                        toSaveLockerList.Add(newDoor.OriginalEntity as SaltoDbLocker);
                        break;
                    case RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM:
                        toSaveRoomList.Add(newDoor.OriginalEntity as SaltoDbRoom);
                        break;
                }
            }

            Configuration.Instance.BeginMassUpdate();

            foreach (SaltoDbDoor door in toSaveDoorList)
            {
                Item newDoorItem = SaltoDbDoor.ConvertDoorToItem(door);
                newDoorItem.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] = RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR;
                Configuration.Instance.SaveItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, newDoorItem);
                SecurityAccess.RegisterItem(newDoorItem);
            }
            foreach (SaltoDbLocker locker in toSaveLockerList)
            {
                Item newLockerItem = SaltoDbLocker.ConvertLockerToItem(locker);
                newLockerItem.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] = RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER;
                Configuration.Instance.SaveItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, newLockerItem);
                SecurityAccess.RegisterItem(newLockerItem);
            }
            foreach (SaltoDbRoom room in toSaveRoomList)
            {
                Item newRoomItem = SaltoDbRoom.ConvertRoomToItem(room);
                newRoomItem.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] = RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM;
                Configuration.Instance.SaveItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, newRoomItem);
                SecurityAccess.RegisterItem(newRoomItem);
            }

            Configuration.Instance.EndMassUpdate();
            Configuration.Instance.RefreshConfiguration(RadiniumSASDefinition.RadiniumSASKind);

            UpdateDisplay();
        }
        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (!refreshNotificationSent)
            {
                MessageBox.Show("Please remember to refresh the management client after selecting the licenced doors.");
                refreshNotificationSent = true;
            }

            List<SaltoDbDoor> toRemoveDoorList = new List<SaltoDbDoor>();
            List<SaltoDbLocker> toRemoveLockerList = new List<SaltoDbLocker>();
            List<SaltoDbRoom> toRemoveRoomList = new List<SaltoDbRoom>();
            List<Item> sasItems = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind);

            for (int i = 0; i < listBoxSelectedDoors.SelectedItems.Count; i++)
            {
                LockSelection oldDoor = (listBoxSelectedDoors.SelectedItems[i] as LockSelection);
                if (oldDoor == null)
                {
                    continue;
                }
                rightDoors.Remove(oldDoor);
                leftDoors.Add(oldDoor);
                switch (oldDoor.LockType)
                {
                    case RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR:
                        toRemoveDoorList.Add(oldDoor.OriginalEntity as SaltoDbDoor);
                        break;
                    case RadiniumSASDefinition.RADINIUM_ITEMTYPE_LOCKER:
                        toRemoveLockerList.Add(oldDoor.OriginalEntity as SaltoDbLocker);
                        break;
                    case RadiniumSASDefinition.RADINIUM_ITEMTYPE_ROOM:
                        toRemoveRoomList.Add(oldDoor.OriginalEntity as SaltoDbRoom);
                        break;
                }
            }

            Configuration.Instance.BeginMassUpdate();
            foreach(SaltoDbDoor door in toRemoveDoorList)
            {
                Item sasItem = sasItems.Where(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && x.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID] == door.ExtDoorID).FirstOrDefault();
                if(sasItem == null)
                {
                    continue;
                }
                Configuration.Instance.DeleteItemConfiguration(RadiniumSASDefinition.RadiniumSASKind, sasItem);
                SecurityAccess.UnregisterItem(sasItem);
            }
            foreach (SaltoDbLocker locker in toRemoveLockerList)
            {
                Item sasItem = sasItems.Where(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && x.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID] == locker.ExtDoorID).FirstOrDefault();
                if (sasItem == null)
                {
                    continue;
                }
                Configuration.Instance.DeleteItemConfiguration(RadiniumSASDefinition.RadiniumSASKind, sasItem);
                SecurityAccess.UnregisterItem(sasItem);
            }
            foreach (SaltoDbRoom room in toRemoveRoomList)
            {
                Item sasItem = sasItems.Where(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_EXTDOORID) && x.Properties[RadiniumSASDefinition.RADINIUM_EXTDOORID] == room.ExtDoorID).FirstOrDefault();
                if (sasItem == null)
                {
                    continue;
                }
                Configuration.Instance.DeleteItemConfiguration(RadiniumSASDefinition.RadiniumSASKind, sasItem);
                SecurityAccess.UnregisterItem(sasItem);
            }
            Configuration.Instance.EndMassUpdate();
            Configuration.Instance.RefreshConfiguration(RadiniumSASDefinition.RadiniumSASKind);

            UpdateDisplay();
        }

        private void textBoxDoorSearch_TextChanged(object sender, EventArgs e)
        {
            UpdateDisplay();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveText();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            successfulUserInfo = false;
            string sqlConnStr = SQL_CONNECTION_STRING(dbCred.Username, dbCred.Password, dbCred.DataSource, dbCred.Catalogue);

            ThreadPool.QueueUserWorkItem(x =>
            {
                comms.TransmitMessage(new VideoOS.Platform.Messaging.Message(RadiniumSASDefinition.RADINIUM_API_REQUEST_USERS, (sqlConnStr)), null, null, null);
            });
            DateTime endWait = DateTime.Now.AddSeconds(10);
            while(!successfulUserInfo && DateTime.Now < endWait)
            {
                Thread.Sleep(100);
            }
            if(!successfulUserInfo)
            {
                MessageBox.Show("Database credentials invalid. Please verify server details and credentials.");
            }
            else
            {
                Utility.SaveConfiguration(Utility.ReadLicence(), dbCred.DataSource, dbCred.Catalogue, dbCred.Username, dbCred.Password);
                MessageBox.Show("Successfully connected and saved database credentials");
            }
        }
    }
}
