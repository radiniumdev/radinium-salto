﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VideoOS.Platform;
using VideoOS.Platform.Admin;
using VideoOS.Platform.Data;
using VideoOS.Platform.Util;

namespace RadiniumSAS.Admin
{
    public class LockdownItemManager : ItemManager
    {
        private LockdownUserControl _userControl;
        private Guid _kind;

        #region Constructors

        public LockdownItemManager(Guid kind) : base()
        {
            _kind = kind;
        }

        /// <summary>
        /// Is called when the Environment is initializing, and will soon call GetItem methods
        /// IF you need to establish connection to a remote server, this is a good place to initialize.
        /// </summary>
        public override void Init()
        {
        }

        /// <summary>
        /// Is called when server is changing or application is closing down.
        /// You should close any remote session you may have, and flush cache.
        /// </summary>
        public override void Close()
        {
        }
        #endregion


        private static Collection<EventGroup> _eventGroups = new Collection<VideoOS.Platform.Data.EventGroup>();
        private static Collection<EventType> _eventTypes = null;

        public override System.Collections.ObjectModel.Collection<VideoOS.Platform.Data.EventGroup> GetKnownEventGroups(System.Globalization.CultureInfo culture)
        {
            return _eventGroups;
        }

        public override Collection<EventType> GetKnownEventTypes(CultureInfo culture)
        {
            return _eventTypes;
        }
        public override bool IsContextMenuValid(string command)
        {
            return true;
        }

        #region UserControl Methods

        private EventHandler configChanged;

        /// <summary>
        /// Generate the UserControl for configuring a type of item that this ItemManager manages.
        /// </summary>
        /// <returns></returns>
        public override UserControl GenerateDetailUserControl()
        {
            if(configChanged == null)
            {
                configChanged = new EventHandler(ConfigurationChangedByUserHandler);
            }
            _userControl = new LockdownUserControl();

            _userControl.ConfigurationChangedByUser += configChanged;
            return _userControl;
        }

        /// <summary>
        /// The UserControl is no longer being used, and related resources can be released.
        /// </summary>
        public override void ReleaseUserControl()
        {
            if (_userControl != null)
            {
                if(configChanged != null)
                {
                    _userControl.ConfigurationChangedByUser -= configChanged;
                }
                _userControl = null;
            }
        }

        /// <summary>
        /// Clear all user entries on the UserControl, all visible fields should be blank or default values.
        /// </summary>
        public override void ClearUserControl()
        {
            CurrentItem = null;
            if (_userControl != null)
            {
                _userControl.ClearContent();
            }
        }

        /// <summary>
        /// Fill the UserControl with the content of the Item or the data it represent.
        /// </summary>
        /// <param name="item">The Item to work with</param>
        public override void FillUserControl(Item item)
        {
            CurrentItem = item;
            if (_userControl != null)
            {
                _userControl.FillContent(item);
            }
        }

        /// <summary>
        /// Create a UserControl to be used for adding a new item.<br/>
        /// Just fill the user control with default content, but do NOT add it to your configuration yet.<br/>
        /// Implementation of this method will determine the flow of calls for creating new Items.<br/>
        /// When implemented, this UserControl is shown to the user, and upon OK pressed the ValidateAddUserControl is called
        /// for the plugin to validate the entry, and a OK response will result in closing the form containing the AddUserControl and 
        /// a call the the Create() method including the AddUserControl.<br/>
        /// If not implementing this method, the Create() method WITHOUT the AddUserControl parameter is called, and a default
        /// Item is then created, stored (where ever this ItemManager stores it configuration), and displayed in the
        /// TreeView.<br/>
        /// Last step is that the new item node is selected and the ItemManager.FillUserControl is called
        /// to fill the large configuration form.
        /// </summary>
        /// <returns></returns>
        public override UserControl GenerateAddUserControl(Item parentItem, FQID suggestedFQID)
        {
            return new RadiniumSASAddUserControl();
        }

        /// <summary>
        /// Validate the content of the AddUserControl generated by this class's GenerateAddUserControl.
        /// </summary>
        /// <param name="addUserControl"></param>
        /// <returns>True if the content is valid.</returns>
        public override bool ValidateAddUserControl(UserControl addUserControl)
        {
            return true;
        }
        #endregion

        #region Working with currentItem

        /// <summary>
        /// Get the name of the current Item.
        /// </summary>
        /// <returns></returns>
        public override string GetItemName()
        {
            if (_userControl != null)
            {
                return _userControl.DisplayName;
            }
            return "";
        }

        /// <summary>
        /// Update the name for current Item.  the user edited the Name via F2 in the TreeView
        /// </summary>
        /// <param name="name"></param>
        public override void SetItemName(string name)
        {
            if (_userControl != null)
            {
                _userControl.DisplayName = name;
            }
        }

        /// <summary>
        /// Validate the user entry, and return true for OK.<br/>
        /// External configuration should be saved during this call.<br/>
        /// Any errors should be displayed to the user, and the field in 
        /// error should get focus.
        /// </summary>
        /// <returns>Indicates error in user entry.  True is a valid entry</returns>
        public override bool ValidateAndSaveUserControl()
        {
            if (CurrentItem != null)
            {
                if (_userControl != null)
                {
                    //Get user entered fields
                    _userControl.UpdateItem(CurrentItem);
                }

                //In this template we save configuration on the VMS system
                Configuration.Instance.SaveItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, CurrentItem);
            }
            return true;
        }

        /// <summary>
        /// Create a new item. Insert default values. 
        /// The parentFQID can be null, when a top level node is created (e.g. no parent)
        /// The new fqid should be filled with ServerID, ParentId, ObjectId or ObjectIdString and Kind.
        /// </summary>
        /// <param name="parentItem">Identifies the configuration parent to the new item.</param>
        /// <param name="suggestedFQID">A pre-generated fqid with above fields filled. 
        /// The ObjectId or ObjectIdString can be overridden.</param>
        /// <returns>A new Item, only the FQID and Name field are required to be filled.  The return values is used to identify and select the item tree node</returns>
        public override Item CreateItem(Item parentItem, FQID suggestedFQID)
        {
            CurrentItem = new Item(suggestedFQID, "Enter a name");
            if (_userControl != null)
            {
                _userControl.FillContent(CurrentItem);
            }
            Configuration.Instance.SaveItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, CurrentItem);
            return CurrentItem;
        }

        /// <summary>
        /// Create a new item. Insert values as user has entered on the AddUserControl.<br/>
        /// The parentFQID can be null, when a top level node is created (e.g. no parent)
        /// The new fqid should be filled with ServerID, ParentId, ObjectId or ObjectIdString and Kind.
        /// </summary>
        /// <param name="parentItem">Identifies the configuration parent to the new item.</param>
        /// <param name="suggestedFQID">A pre-generated fqid with above fields filled. 
        /// The ObjectId or ObjectIdString can be overridden.</param>
        /// <param name="addUserControl">A filled user control returned by the GeneratedAddUserControl method after it has been displayed and edited by the user</param>
        /// <returns>A new Item, only the FQID and Name field are required to be filled.  The return value is used to identify and select the item tree node</returns>
        public override Item CreateItem(Item parentItem, FQID suggestedFQID, UserControl addUserControl)
        {
            CurrentItem = new Item(suggestedFQID, ((RadiniumSASAddUserControl)addUserControl).ItemName);
            Configuration.Instance.SaveItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, CurrentItem);
            return CurrentItem;
        }

        /// <summary>
        /// When an administrator selects the context menu Delete item, or press the DEL key, 
        /// a confirmation dialog is displayed and upon administrator confirms this method is called.
        /// <code>
        /// For configurations saved on the video server, the following code can be used:
        /// if (item != null)
        /// {
        ///     Configuration.Instance.DeleteItemConfiguration(MyPluginId, item);
        ///	}
        /// </code>
        /// </summary>
        /// <param name="item">The Item to delete</param>
        public override void DeleteItem(Item item)
        {
            if (item != null)
            {
                Configuration.Instance.DeleteItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, item);
            }

        }
        #endregion

        #region Configuration Access Methods

        /// <summary>
        /// Returns a list of all Items of this Kind
        /// </summary>
        /// <returns>A list of items.  Allowed to return null if no Items found.</returns>
        public override List<Item> GetItems()
        {
            //All items in this sample are stored with the Video, therefor no ServerIs or parent ids is used.
            List<Item> items = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, _kind).OrderBy(x => x.Name).ToList();
            return items;
        }

        /// <summary>
        /// Returns a list of all Items from a specific server.
        /// </summary>
        /// <param name="parentItem">The parent Items</param>
        /// <returns>A list of items.  Allowed to return null if no Items found.</returns>
        public override List<Item> GetItems(Item parentItem)
        {
            List<Item> items = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, parentItem, _kind).OrderBy(x => x.Name).ToList();
            return items;
        }

        /// <summary>
        /// Returns the Item defined by the FQID. Will return null if not found.
        /// </summary>
        /// <param name="fqid">Fully Qualified ID of an Item</param>
        /// <returns>An Item</returns>
        public override Item GetItem(FQID fqid)
        {
            Item item = Configuration.Instance.GetItemConfiguration(RadiniumSASDefinition.RadiniumSASPluginId, _kind, fqid.ObjectId);
            return item;
        }

        #endregion

        #region Messages and Status


        /// <summary>
        /// Return the operational state of a specific Item.
        /// This is used by the Event Server.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override OperationalState GetOperationalState(Item item)
        {
            return OperationalState.Ok;     // Everything is OK for the specified Item
        }

        #endregion
    }
}
