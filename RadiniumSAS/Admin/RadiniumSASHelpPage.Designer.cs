namespace RadiniumSAS.Admin
{
    partial class HelpPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label cardholderIDLabel;
            System.Windows.Forms.Label expiryDateLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpPage));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.About = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Licensing = new System.Windows.Forms.TabPage();
            this.tabPageServerSettings = new System.Windows.Forms.TabPage();
            this.panelDbCred = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxSHIPKey = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxProtocol = new System.Windows.Forms.ComboBox();
            this.buttonSaveShip = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxShipHost = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxShipPort = new System.Windows.Forms.TextBox();
            this.tabPageDoorSelection = new System.Windows.Forms.TabPage();
            this.labelLicences = new System.Windows.Forms.Label();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxDoorSearch = new System.Windows.Forms.TextBox();
            this.listBoxSelectedDoors = new System.Windows.Forms.ListBox();
            this.listBoxUnselectedDoors = new System.Windows.Forms.ListBox();
            this.Cardholders = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtContact = new System.Windows.Forms.TextBox();
            this.txtDepartment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelCardholderID = new System.Windows.Forms.Label();
            this.listBoxUsers = new System.Windows.Forms.ListBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.cardholderTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBoxCardholder = new System.Windows.Forms.PictureBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            label16 = new System.Windows.Forms.Label();
            cardholderIDLabel = new System.Windows.Forms.Label();
            expiryDateLabel = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.About.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPageServerSettings.SuspendLayout();
            this.panelDbCred.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageDoorSelection.SuspendLayout();
            this.Cardholders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardholder)).BeginInit();
            this.SuspendLayout();
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(4, 57);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(630, 26);
            label16.TabIndex = 53;
            label16.Text = "Search for cardholders to view, add or delete a picture of the cardholder. The ca" +
    "rdholder picture is used in the XProtect Smart Client \r\nwhen an access control e" +
    "vent has been registered.";
            // 
            // cardholderIDLabel
            // 
            cardholderIDLabel.AutoSize = true;
            cardholderIDLabel.Location = new System.Drawing.Point(344, 330);
            cardholderIDLabel.Name = "cardholderIDLabel";
            cardholderIDLabel.Size = new System.Drawing.Size(0, 13);
            cardholderIDLabel.TabIndex = 62;
            // 
            // expiryDateLabel
            // 
            expiryDateLabel.AutoSize = true;
            expiryDateLabel.Location = new System.Drawing.Point(344, 350);
            expiryDateLabel.Name = "expiryDateLabel";
            expiryDateLabel.Size = new System.Drawing.Size(0, 13);
            expiryDateLabel.TabIndex = 63;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.About);
            this.tabControl1.Controls.Add(this.Licensing);
            this.tabControl1.Controls.Add(this.tabPageServerSettings);
            this.tabControl1.Controls.Add(this.tabPageDoorSelection);
            this.tabControl1.Controls.Add(this.Cardholders);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(702, 565);
            this.tabControl1.TabIndex = 1;
            // 
            // About
            // 
            this.About.Controls.Add(this.pictureBox1);
            this.About.Controls.Add(this.label6);
            this.About.Controls.Add(this.label4);
            this.About.Controls.Add(this.label3);
            this.About.Controls.Add(this.pictureBox2);
            this.About.Location = new System.Drawing.Point(4, 22);
            this.About.Name = "About";
            this.About.Padding = new System.Windows.Forms.Padding(3);
            this.About.Size = new System.Drawing.Size(694, 539);
            this.About.TabIndex = 0;
            this.About.Text = "About";
            this.About.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Image = global::RadiniumSAS.Properties.Resources.saltologo;
            this.pictureBox1.Location = new System.Drawing.Point(63, 142);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(127, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(228, 105);
            this.label6.TabIndex = 12;
            this.label6.Text = "\r\nVersion 2.0.0.1\r\n\r\n\r\nCopyright 2016-2021 Radinium (Pty) Ltd.\r\n\r\nwww.radinium.co" +
    "m\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(290, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "SALTO integration into Milestone XProtect �\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 397);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(565, 91);
            this.label3.TabIndex = 10;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Image = global::RadiniumSAS.Properties.Resources.RadiniumAccess_LogoREV2;
            this.pictureBox2.Location = new System.Drawing.Point(9, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(284, 136);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // Licensing
            // 
            this.Licensing.Location = new System.Drawing.Point(4, 22);
            this.Licensing.Name = "Licensing";
            this.Licensing.Padding = new System.Windows.Forms.Padding(3);
            this.Licensing.Size = new System.Drawing.Size(694, 539);
            this.Licensing.TabIndex = 1;
            this.Licensing.Text = "Licensing";
            this.Licensing.UseVisualStyleBackColor = true;
            // 
            // tabPageServerSettings
            // 
            this.tabPageServerSettings.Controls.Add(this.panelDbCred);
            this.tabPageServerSettings.Controls.Add(this.groupBox1);
            this.tabPageServerSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageServerSettings.Name = "tabPageServerSettings";
            this.tabPageServerSettings.Size = new System.Drawing.Size(694, 539);
            this.tabPageServerSettings.TabIndex = 7;
            this.tabPageServerSettings.Text = "Server Settings";
            this.tabPageServerSettings.UseVisualStyleBackColor = true;
            // 
            // panelDbCred
            // 
            this.panelDbCred.Controls.Add(this.button1);
            this.panelDbCred.Location = new System.Drawing.Point(3, 190);
            this.panelDbCred.Name = "panelDbCred";
            this.panelDbCred.Size = new System.Drawing.Size(389, 301);
            this.panelDbCred.TabIndex = 8;
            this.panelDbCred.TabStop = false;
            this.panelDbCred.Text = "Milestone database server settings";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 272);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(383, 23);
            this.button1.TabIndex = 99;
            this.button1.Text = "Test database connection and save settings";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxSHIPKey);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.comboBoxProtocol);
            this.groupBox1.Controls.Add(this.buttonSaveShip);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxShipHost);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.textBoxShipPort);
            this.groupBox1.Location = new System.Drawing.Point(3, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(389, 172);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SHIP settings";
            // 
            // textBoxSHIPKey
            // 
            this.textBoxSHIPKey.Location = new System.Drawing.Point(131, 110);
            this.textBoxSHIPKey.Name = "textBoxSHIPKey";
            this.textBoxSHIPKey.Size = new System.Drawing.Size(255, 20);
            this.textBoxSHIPKey.TabIndex = 5;
            this.toolTip.SetToolTip(this.textBoxSHIPKey, "The port on which Salto SHIP listens for connections.\r\nYou can find this in the s" +
        "ame place you enabled SHIP in\r\nSalto ProAccess Space, in the System -> General O" +
        "ptions button.\r\n");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(131, 94);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Salto SHIP Key";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(0, 94);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Communications protocol";
            // 
            // comboBoxProtocol
            // 
            this.comboBoxProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProtocol.FormattingEnabled = true;
            this.comboBoxProtocol.Items.AddRange(new object[] {
            "STP (Unencrypted)",
            "HTTP (Unencrypted)",
            "HTTPS (Encrypted)"});
            this.comboBoxProtocol.Location = new System.Drawing.Point(3, 110);
            this.comboBoxProtocol.Name = "comboBoxProtocol";
            this.comboBoxProtocol.Size = new System.Drawing.Size(122, 21);
            this.comboBoxProtocol.TabIndex = 4;
            // 
            // buttonSaveShip
            // 
            this.buttonSaveShip.Location = new System.Drawing.Point(3, 143);
            this.buttonSaveShip.Name = "buttonSaveShip";
            this.buttonSaveShip.Size = new System.Drawing.Size(383, 23);
            this.buttonSaveShip.TabIndex = 6;
            this.buttonSaveShip.Text = "Test SHIP connection and save settings";
            this.buttonSaveShip.UseVisualStyleBackColor = true;
            this.buttonSaveShip.Click += new System.EventHandler(this.SaveShipSettings_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Salto SHIP Server Hostname";
            // 
            // textBoxShipHost
            // 
            this.textBoxShipHost.Location = new System.Drawing.Point(3, 32);
            this.textBoxShipHost.Name = "textBoxShipHost";
            this.textBoxShipHost.Size = new System.Drawing.Size(383, 20);
            this.textBoxShipHost.TabIndex = 2;
            this.toolTip.SetToolTip(this.textBoxShipHost, "This is the SHIP hostname, usually the same as the\r\nProAccess Space server.  Type" +
        " either the hostname or\r\nIP address of that server here.");
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(0, 55);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(154, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Salto SHIP Server TCP/IP Port";
            // 
            // textBoxShipPort
            // 
            this.textBoxShipPort.Location = new System.Drawing.Point(3, 71);
            this.textBoxShipPort.Name = "textBoxShipPort";
            this.textBoxShipPort.Size = new System.Drawing.Size(383, 20);
            this.textBoxShipPort.TabIndex = 3;
            this.toolTip.SetToolTip(this.textBoxShipPort, "The port on which Salto SHIP listens for connections.\r\nYou can find this in the s" +
        "ame place you enabled SHIP in\r\nSalto ProAccess Space, in the System -> General O" +
        "ptions button.\r\n");
            // 
            // tabPageDoorSelection
            // 
            this.tabPageDoorSelection.Controls.Add(this.labelLicences);
            this.tabPageDoorSelection.Controls.Add(this.buttonRemove);
            this.tabPageDoorSelection.Controls.Add(this.buttonAdd);
            this.tabPageDoorSelection.Controls.Add(this.label11);
            this.tabPageDoorSelection.Controls.Add(this.label10);
            this.tabPageDoorSelection.Controls.Add(this.label9);
            this.tabPageDoorSelection.Controls.Add(this.textBoxDoorSearch);
            this.tabPageDoorSelection.Controls.Add(this.listBoxSelectedDoors);
            this.tabPageDoorSelection.Controls.Add(this.listBoxUnselectedDoors);
            this.tabPageDoorSelection.Location = new System.Drawing.Point(4, 22);
            this.tabPageDoorSelection.Name = "tabPageDoorSelection";
            this.tabPageDoorSelection.Size = new System.Drawing.Size(694, 539);
            this.tabPageDoorSelection.TabIndex = 8;
            this.tabPageDoorSelection.Text = "Lock Selection";
            this.tabPageDoorSelection.UseVisualStyleBackColor = true;
            // 
            // labelLicences
            // 
            this.labelLicences.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelLicences.AutoSize = true;
            this.labelLicences.Location = new System.Drawing.Point(389, 77);
            this.labelLicences.Name = "labelLicences";
            this.labelLicences.Size = new System.Drawing.Size(30, 13);
            this.labelLicences.TabIndex = 10;
            this.labelLicences.Text = "0 / 0";
            this.labelLicences.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(191, 218);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(87, 23);
            this.buttonRemove.TabIndex = 7;
            this.buttonRemove.Text = "Remove <";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(191, 189);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(87, 23);
            this.buttonAdd.TabIndex = 6;
            this.buttonAdd.Text = "Add >";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Search Locks";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Unselected";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(281, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Selected";
            // 
            // textBoxDoorSearch
            // 
            this.textBoxDoorSearch.Location = new System.Drawing.Point(3, 25);
            this.textBoxDoorSearch.Name = "textBoxDoorSearch";
            this.textBoxDoorSearch.Size = new System.Drawing.Size(463, 20);
            this.textBoxDoorSearch.TabIndex = 2;
            this.textBoxDoorSearch.TextChanged += new System.EventHandler(this.textBoxDoorSearch_TextChanged);
            // 
            // listBoxSelectedDoors
            // 
            this.listBoxSelectedDoors.FormattingEnabled = true;
            this.listBoxSelectedDoors.Location = new System.Drawing.Point(284, 93);
            this.listBoxSelectedDoors.Name = "listBoxSelectedDoors";
            this.listBoxSelectedDoors.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxSelectedDoors.Size = new System.Drawing.Size(182, 277);
            this.listBoxSelectedDoors.TabIndex = 1;
            // 
            // listBoxUnselectedDoors
            // 
            this.listBoxUnselectedDoors.FormattingEnabled = true;
            this.listBoxUnselectedDoors.Location = new System.Drawing.Point(3, 93);
            this.listBoxUnselectedDoors.Name = "listBoxUnselectedDoors";
            this.listBoxUnselectedDoors.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxUnselectedDoors.Size = new System.Drawing.Size(182, 277);
            this.listBoxUnselectedDoors.TabIndex = 0;
            // 
            // Cardholders
            // 
            this.Cardholders.Controls.Add(this.button2);
            this.Cardholders.Controls.Add(this.pictureBox3);
            this.Cardholders.Controls.Add(this.label8);
            this.Cardholders.Controls.Add(this.txtDescription);
            this.Cardholders.Controls.Add(this.txtContact);
            this.Cardholders.Controls.Add(this.txtDepartment);
            this.Cardholders.Controls.Add(this.label7);
            this.Cardholders.Controls.Add(this.label5);
            this.Cardholders.Controls.Add(this.label2);
            this.Cardholders.Controls.Add(this.labelCardholderID);
            this.Cardholders.Controls.Add(this.listBoxUsers);
            this.Cardholders.Controls.Add(expiryDateLabel);
            this.Cardholders.Controls.Add(cardholderIDLabel);
            this.Cardholders.Controls.Add(this.button7);
            this.Cardholders.Controls.Add(this.button6);
            this.Cardholders.Controls.Add(this.label18);
            this.Cardholders.Controls.Add(this.cardholderTextBox);
            this.Cardholders.Controls.Add(label16);
            this.Cardholders.Controls.Add(this.label17);
            this.Cardholders.Controls.Add(this.pictureBoxCardholder);
            this.Cardholders.Location = new System.Drawing.Point(4, 22);
            this.Cardholders.Name = "Cardholders";
            this.Cardholders.Size = new System.Drawing.Size(694, 539);
            this.Cardholders.TabIndex = 6;
            this.Cardholders.Text = "Cardholders";
            this.Cardholders.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(488, 512);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(203, 23);
            this.button2.TabIndex = 74;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(7, 6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(48, 48);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 73;
            this.pictureBox3.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 72;
            this.label8.Text = "Search:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(262, 433);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(429, 73);
            this.txtDescription.TabIndex = 71;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(262, 394);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(429, 20);
            this.txtContact.TabIndex = 70;
            this.txtContact.TextChanged += new System.EventHandler(this.txtContact_TextChanged);
            // 
            // txtDepartment
            // 
            this.txtDepartment.Location = new System.Drawing.Point(262, 355);
            this.txtDepartment.Name = "txtDepartment";
            this.txtDepartment.Size = new System.Drawing.Size(429, 20);
            this.txtDepartment.TabIndex = 69;
            this.txtDepartment.TextChanged += new System.EventHandler(this.txtDepartment_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(263, 417);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 68;
            this.label7.Text = "Description";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(263, 378);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 67;
            this.label5.Text = "Contact #";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(263, 339);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 66;
            this.label2.Text = "Department";
            // 
            // labelCardholderID
            // 
            this.labelCardholderID.AutoSize = true;
            this.labelCardholderID.Location = new System.Drawing.Point(365, 94);
            this.labelCardholderID.Name = "labelCardholderID";
            this.labelCardholderID.Size = new System.Drawing.Size(18, 13);
            this.labelCardholderID.TabIndex = 65;
            this.labelCardholderID.Text = "ID";
            // 
            // listBoxUsers
            // 
            this.listBoxUsers.FormattingEnabled = true;
            this.listBoxUsers.Location = new System.Drawing.Point(7, 112);
            this.listBoxUsers.Name = "listBoxUsers";
            this.listBoxUsers.Size = new System.Drawing.Size(249, 394);
            this.listBoxUsers.TabIndex = 64;
            this.listBoxUsers.SelectedIndexChanged += new System.EventHandler(this.listBoxUsers_SelectedIndexChanged);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(488, 141);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(108, 23);
            this.button7.TabIndex = 59;
            this.button7.Text = "Delete picture";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.buttonDeletePicture_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(488, 112);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(108, 23);
            this.button6.TabIndex = 58;
            this.button6.Text = "Select picture...";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.buttonSavePicture_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(262, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 20);
            this.label18.TabIndex = 56;
            this.label18.Text = "Cardholder";
            // 
            // cardholderTextBox
            // 
            this.cardholderTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cardholderTextBox.Location = new System.Drawing.Point(54, 86);
            this.cardholderTextBox.Name = "cardholderTextBox";
            this.cardholderTextBox.Size = new System.Drawing.Size(202, 20);
            this.cardholderTextBox.TabIndex = 54;
            this.cardholderTextBox.TextChanged += new System.EventHandler(this.cardholderTextBox_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(61, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 20);
            this.label17.TabIndex = 52;
            this.label17.Text = "Cardholders";
            // 
            // pictureBoxCardholder
            // 
            this.pictureBoxCardholder.Location = new System.Drawing.Point(262, 112);
            this.pictureBoxCardholder.Name = "pictureBoxCardholder";
            this.pictureBoxCardholder.Size = new System.Drawing.Size(220, 220);
            this.pictureBoxCardholder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCardholder.TabIndex = 57;
            this.pictureBoxCardholder.TabStop = false;
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 15000;
            this.toolTip.InitialDelay = 100;
            this.toolTip.ReshowDelay = 100;
            // 
            // HelpPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.tabControl1);
            this.Name = "HelpPage";
            this.Size = new System.Drawing.Size(726, 717);
            this.tabControl1.ResumeLayout(false);
            this.About.ResumeLayout(false);
            this.About.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPageServerSettings.ResumeLayout(false);
            this.panelDbCred.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageDoorSelection.ResumeLayout(false);
            this.tabPageDoorSelection.PerformLayout();
            this.Cardholders.ResumeLayout(false);
            this.Cardholders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCardholder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage About;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage Licensing;
        private Cygnetic.MIPPluginCommons.Licence.LicensingUserControl licensingUserControl1;
        private System.Windows.Forms.TabPage Cardholders;
        private System.Windows.Forms.TextBox cardholderTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.PictureBox pictureBoxCardholder;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tabPageServerSettings;
        private System.Windows.Forms.TextBox textBoxShipPort;
        private System.Windows.Forms.TextBox textBoxShipHost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSaveShip;
        private System.Windows.Forms.ListBox listBoxUsers;
        private System.Windows.Forms.Label labelCardholderID;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtContact;
        private System.Windows.Forms.TextBox txtDepartment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox panelDbCred;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TabPage tabPageDoorSelection;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxDoorSearch;
        private System.Windows.Forms.ListBox listBoxSelectedDoors;
        private System.Windows.Forms.ListBox listBoxUnselectedDoors;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label labelLicences;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxProtocol;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxSHIPKey;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button2;
    }
}
