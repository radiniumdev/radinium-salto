namespace RadiniumSAS.Admin
{
    partial class LockdownUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.listBoxSelectedDoors = new System.Windows.Forms.ListBox();
            this.listBoxUnselectedDoors = new System.Windows.Forms.ListBox();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(70, 25);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(419, 20);
            this.textBoxName.TabIndex = 3;
            this.textBoxName.Text = "Enter a name";
            this.textBoxName.TextChanged += new System.EventHandler(this.OnUserChange);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name:";
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(214, 227);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(87, 23);
            this.buttonRemove.TabIndex = 13;
            this.buttonRemove.Text = "Remove <";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(214, 198);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(87, 23);
            this.buttonAdd.TabIndex = 12;
            this.buttonAdd.Text = "Add >";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Unselected";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(304, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Selected";
            // 
            // listBoxSelectedDoors
            // 
            this.listBoxSelectedDoors.FormattingEnabled = true;
            this.listBoxSelectedDoors.Location = new System.Drawing.Point(307, 102);
            this.listBoxSelectedDoors.Name = "listBoxSelectedDoors";
            this.listBoxSelectedDoors.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxSelectedDoors.Size = new System.Drawing.Size(182, 277);
            this.listBoxSelectedDoors.TabIndex = 9;
            // 
            // listBoxUnselectedDoors
            // 
            this.listBoxUnselectedDoors.FormattingEnabled = true;
            this.listBoxUnselectedDoors.Location = new System.Drawing.Point(26, 102);
            this.listBoxUnselectedDoors.Name = "listBoxUnselectedDoors";
            this.listBoxUnselectedDoors.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxUnselectedDoors.Size = new System.Drawing.Size(182, 277);
            this.listBoxUnselectedDoors.TabIndex = 8;
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(70, 63);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(419, 20);
            this.textBoxSearch.TabIndex = 14;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearch_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Search:";
            // 
            // LockdownUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.listBoxSelectedDoors);
            this.Controls.Add(this.listBoxUnselectedDoors);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.label1);
            this.Name = "LockdownUserControl";
            this.Size = new System.Drawing.Size(520, 390);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox listBoxSelectedDoors;
        private System.Windows.Forms.ListBox listBoxUnselectedDoors;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Label label2;
    }
}
