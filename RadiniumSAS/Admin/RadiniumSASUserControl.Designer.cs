namespace RadiniumSAS.Admin
{
    partial class RadiniumSASUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxMap = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxCamera4 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxCamera3 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxCamera2 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxCamera1 = new System.Windows.Forms.ComboBox();
            this.camera4 = new System.Windows.Forms.GroupBox();
            this.camera3 = new System.Windows.Forms.GroupBox();
            this.camera1 = new System.Windows.Forms.GroupBox();
            this.camera2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxMap
            // 
            this.comboBoxMap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMap.FormattingEnabled = true;
            this.comboBoxMap.Location = new System.Drawing.Point(10, 191);
            this.comboBoxMap.Name = "comboBoxMap";
            this.comboBoxMap.Size = new System.Drawing.Size(214, 21);
            this.comboBoxMap.TabIndex = 52;
            this.comboBoxMap.SelectedIndexChanged += new System.EventHandler(this.comboBoxMap_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 53;
            this.label9.Text = "Map";
            // 
            // comboBoxCamera4
            // 
            this.comboBoxCamera4.FormattingEnabled = true;
            this.comboBoxCamera4.Location = new System.Drawing.Point(10, 498);
            this.comboBoxCamera4.Name = "comboBoxCamera4";
            this.comboBoxCamera4.Size = new System.Drawing.Size(214, 21);
            this.comboBoxCamera4.TabIndex = 50;
            this.comboBoxCamera4.Visible = false;
            this.comboBoxCamera4.SelectedIndexChanged += new System.EventHandler(this.comboBoxCamera4_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 481);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 51;
            this.label8.Text = "Camera 4";
            this.label8.Visible = false;
            // 
            // comboBoxCamera3
            // 
            this.comboBoxCamera3.FormattingEnabled = true;
            this.comboBoxCamera3.Location = new System.Drawing.Point(10, 456);
            this.comboBoxCamera3.Name = "comboBoxCamera3";
            this.comboBoxCamera3.Size = new System.Drawing.Size(214, 21);
            this.comboBoxCamera3.TabIndex = 48;
            this.comboBoxCamera3.Visible = false;
            this.comboBoxCamera3.SelectedIndexChanged += new System.EventHandler(this.comboBoxCamera3_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 439);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 49;
            this.label7.Text = "Camera 3";
            this.label7.Visible = false;
            // 
            // comboBoxCamera2
            // 
            this.comboBoxCamera2.FormattingEnabled = true;
            this.comboBoxCamera2.Location = new System.Drawing.Point(10, 150);
            this.comboBoxCamera2.Name = "comboBoxCamera2";
            this.comboBoxCamera2.Size = new System.Drawing.Size(214, 21);
            this.comboBoxCamera2.TabIndex = 46;
            this.comboBoxCamera2.SelectedIndexChanged += new System.EventHandler(this.comboBoxCamera2_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 47;
            this.label6.Text = "Camera 2";
            // 
            // comboBoxCamera1
            // 
            this.comboBoxCamera1.FormattingEnabled = true;
            this.comboBoxCamera1.Location = new System.Drawing.Point(10, 108);
            this.comboBoxCamera1.Name = "comboBoxCamera1";
            this.comboBoxCamera1.Size = new System.Drawing.Size(214, 21);
            this.comboBoxCamera1.TabIndex = 44;
            this.comboBoxCamera1.SelectedIndexChanged += new System.EventHandler(this.comboBoxCamera1_SelectedIndexChanged);
            // 
            // camera4
            // 
            this.camera4.Location = new System.Drawing.Point(540, 321);
            this.camera4.Name = "camera4";
            this.camera4.Size = new System.Drawing.Size(304, 224);
            this.camera4.TabIndex = 43;
            this.camera4.TabStop = false;
            this.camera4.Text = "Camera 4";
            this.camera4.Visible = false;
            // 
            // camera3
            // 
            this.camera3.Location = new System.Drawing.Point(540, 91);
            this.camera3.Name = "camera3";
            this.camera3.Size = new System.Drawing.Size(304, 224);
            this.camera3.TabIndex = 42;
            this.camera3.TabStop = false;
            this.camera3.Text = "Camera 3";
            this.camera3.Visible = false;
            // 
            // camera1
            // 
            this.camera1.Location = new System.Drawing.Point(230, 91);
            this.camera1.Name = "camera1";
            this.camera1.Size = new System.Drawing.Size(304, 224);
            this.camera1.TabIndex = 41;
            this.camera1.TabStop = false;
            this.camera1.Text = "Camera 1";
            // 
            // camera2
            // 
            this.camera2.Location = new System.Drawing.Point(230, 321);
            this.camera2.Name = "camera2";
            this.camera2.Size = new System.Drawing.Size(304, 224);
            this.camera2.TabIndex = 40;
            this.camera2.TabStop = false;
            this.camera2.Text = "Camera 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Camera 1";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(96, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(325, 20);
            this.textBox1.TabIndex = 31;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Name:";
            // 
            // RadiniumSASUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.camera3);
            this.Controls.Add(this.comboBoxMap);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBoxCamera4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxCamera3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboBoxCamera2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBoxCamera1);
            this.Controls.Add(this.camera4);
            this.Controls.Add(this.camera1);
            this.Controls.Add(this.camera2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label10);
            this.Name = "RadiniumSASUserControl";
            this.Size = new System.Drawing.Size(873, 579);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxMap;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxCamera4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxCamera3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxCamera2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxCamera1;
        private System.Windows.Forms.GroupBox camera4;
        private System.Windows.Forms.GroupBox camera3;
        private System.Windows.Forms.GroupBox camera1;
        private System.Windows.Forms.GroupBox camera2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
    }
}
