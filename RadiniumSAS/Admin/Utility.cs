﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using VideoOS.Platform;

namespace RadiniumSAS.Admin
{
    class Utility
    {
        public static void AppendNodeWithText(XmlNode root, string nodeName, string text)
        {
            XmlNode licencesNode = root.OwnerDocument.CreateElement(nodeName);
            licencesNode.InnerText = text;
            root.AppendChild(licencesNode);
        }

        public static void SaveConfiguration(string licence)
        {
            SaveConfiguration(licence, ReadDBDataSource(), ReadDBCatalogue(), ReadDBUsername(), ReadDBPassword());
        }

        public static void SaveConfiguration(string licence, string dbHost, string dbDatabase, string dbUsername, string dbPassword)
        {
            //var fingerprint = new RadiniumClickFingerprintUtil().GenerateFingerprint(null);
            var hostname = Environment.MachineName;
            var options = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.OptionsGuid, false);
            if (options == null)
            {
                XmlDocument doc = new XmlDocument();
                options = doc.CreateElement("Root");
            }

            IEnumerator children = options.ChildNodes.GetEnumerator();
            bool found = false;
            while (children.MoveNext())
            {
                XmlNode option = children.Current as XmlNode;
                if (option != null && option.Name == "licences")
                {
                    option.InnerText = licence;
                    found = true;
                }
                if (option != null && option.Name == "dbHost")
                {
                    option.InnerText = dbHost;
                }
                if (option != null && option.Name == "dbDatabase")
                {
                    option.InnerText = dbDatabase;
                }
                if (option != null && option.Name == "dbUsername")
                {
                    option.InnerText = dbUsername;
                }
                if (option != null && option.Name == "dbPassword")
                {
                    option.InnerText = dbPassword;
                }
            }
            if (!found)
            {
                AppendNodeWithText(options, "licences", licence);
                AppendNodeWithText(options, "dbHost", dbHost);
                AppendNodeWithText(options, "dbDatabase", dbDatabase);
                AppendNodeWithText(options, "dbUsername", dbUsername);
                AppendNodeWithText(options, "dbPassword", dbPassword);
            }
            Configuration.Instance.SaveOptionsConfiguration(RadiniumSASDefinition.OptionsGuid, false, options);


            MessageBox.Show("Your configuration has been saved.");
        }

        public class Entry
        {
            public string hostname { get; set; }
            public string fingerprint { get; set; }
        }

        internal static List<Entry> GetEntries()
        {
            List<Entry> hostnames = new List<Entry>();
            XmlNode node = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.OptionsGuid, false);
            if (node != null)
            {
                IEnumerator children = node.ChildNodes.GetEnumerator();
                while (children.MoveNext())
                {
                    XmlNode option = children.Current as XmlNode;
                    if (option == null || option.Name != "entry")
                    {
                        continue;
                    }
                    else
                    {
                        hostnames.Add(new Entry { hostname = option.ChildNodes[1].InnerText, fingerprint = option.ChildNodes[0].InnerText });
                    }
                }

                return hostnames;
            }
            else
            {
                return null;
            }
        }
        public static string ReadNode(string name, string def)
        {
            XmlNode node = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.OptionsGuid, false);
            if (node != null)
            {
                try
                {
                    return node.SelectSingleNode("//" + name).InnerText;
                }
                catch (Exception e)
                { }
            }
            return def;
        }

        public static string ReadLicence()
        {
            XmlNode node = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.OptionsGuid, false);
            if (node != null)
            {
                try
                {
                    return node.SelectSingleNode("//licences").InnerText;
                }
                catch (Exception) { }
            }
            return "";
        }

        public static void SaveLicensingApiKey(string apiKey)
        {
            XmlNode node = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.OptionsGuid, false);
            XmlDocument doc = new XmlDocument();
            if (node == null)
            {
                node = doc.CreateElement("Root");
            }
            else
            {
                doc = node.OwnerDocument;
            }
            try
            {
                node.SelectSingleNode("//licensingApiKey").InnerText = apiKey;
            }
            catch (Exception) 
            {
                XmlNode licenceNode = doc.CreateElement("licensingApiKey");
                licenceNode.InnerText = apiKey;
                node.AppendChild(licenceNode);
            }
            Configuration.Instance.SaveOptionsConfiguration(RadiniumSASDefinition.OptionsGuid, false, node);
        }

        public static string ReadLicensingApiKey()
        {

            XmlNode node = Configuration.Instance.GetOptionsConfiguration(RadiniumSASDefinition.OptionsGuid, false);
            if (node != null)
            {
                try
                {
                    return node.SelectSingleNode("//licensingApiKey").InnerText;
                }
                catch (Exception) { }
            }
            return "";
        }

        public static string ReadDBUsername()
        {
            return ReadNode("dbUsername", "");
        }

        public static string ReadDBPassword()
        {
            return ReadNode("dbPassword", "");
        }

        public static string ReadDBDataSource()
        {
            string dataSource = ReadNode("dbHost", "localhost");

            if (String.IsNullOrEmpty(dataSource))
            {
                dataSource = EnvironmentManager.Instance.CurrentSite.ServerId.ServerHostname;

                if ("localhost".Equals(dataSource))
                {
                    dataSource = Environment.MachineName;
                }
            }
            return dataSource;
        }
        public static string ReadDBCatalogue()
        {
            return ReadNode("dbDatabase", "RadiniumAccess");
        }
        internal static readonly string SQL_CONN_STRING = @"Integrated Security=SSPI;data source={0};initial catalog={1};Connect Timeout=60";
        internal static readonly string USERNAME_PASSWORD_SQL_CONNECTION_STRING = "User Id=\"{0}\";Password=\"{1}\";data source={2};initial catalog={3};Connect Timeout=60";

        public static DbConnection getSqlConnection()
        {
            return new SqlConnection(SQL_CONNECTION_STRING());
        }

        public static string SQL_CONNECTION_STRING(string username, string password, string dataSource, string catalogue)
        {
            if (username.Length > 0)
            {
                return String.Format(USERNAME_PASSWORD_SQL_CONNECTION_STRING, new object[] { username, password, dataSource, catalogue });
            }
            else
            {
                return String.Format(SQL_CONN_STRING, new object[] { dataSource, catalogue });
            }
        }

        public static string SQL_CONNECTION_STRING()
        {
            string username = ReadDBUsername();
            string dataSource = ReadDBDataSource();
            string catalogue = ReadDBCatalogue();

            if (username.Length > 0)
            {
                string password = ReadDBPassword();
                return String.Format(USERNAME_PASSWORD_SQL_CONNECTION_STRING, new object[] { username, password, dataSource, catalogue });
            }
            else
            {
                return String.Format(SQL_CONN_STRING, new object[] { dataSource, catalogue });
            }
        }
    }
}
