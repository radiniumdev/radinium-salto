using Cygnetic.MIPPluginCommons.Admin;
using Cygnetic.MIPPluginCommons.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using VideoOS.Platform;
using VideoOS.Platform.Admin;
using VideoOS.Platform.Log;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.UI;

namespace RadiniumSAS.Admin
{
    /// <summary>
    /// This UserControl only contains a configuration of the Name for the Item.
    /// The methods and properties are used by the ItemManager, and can be changed as you see fit.
    /// </summary>
    public partial class RadiniumSASUserControl : UserControl
    {
        const int NUM_PREVIEW_CAMERAS = 4;

        internal event EventHandler ConfigurationChangedByUser;

        private ItemSearchHandler ItemSearchHandler;
        private AdminImageViewerControl[] viewerControls = new AdminImageViewerControl[NUM_PREVIEW_CAMERAS];
        private List<Item> cameraList;

        private volatile bool closing = false;
        private bool onChangeEnabled = false;

        private MessageCommunication _client;
        private object _mapResponseFilter;


        private string mapid;

        public string MapID
        {
            get { return (string)comboBoxMap.SelectedValue; }
            set
            {
                mapid = value;
                if (comboBoxMap.Items.Count > 0)
                {
                    comboBoxMap.SelectedIndex = 0;

                    for (int i = 0; i < comboBoxMap.Items.Count; i++)
                    {
                        if ((comboBoxMap.Items[i] as Map).Id == mapid)
                        {
                            comboBoxMap.SelectedIndex = i;
                        }
                    }
                }
                OnUserChange(null, null);
            }
        }
        public void GetMaps()
        {
            MapRequestData data = new MapRequestData()
            {
                MapGuid = "", //Get all top level maps
            };
            _client.TransmitMessage(new VideoOS.Platform.Messaging.Message(MessageId.Server.GetMapRequest, data),
                EnvironmentManager.Instance.CurrentSite, null, null);
        }

        ~RadiniumSASUserControl()
        {
            try
            {
                _client.UnRegisterCommunicationFilter(_mapResponseFilter);
                _client.Dispose();
                _client = null;
            }
            catch(Exception)
            {

            }
        }


        public RadiniumSASUserControl()
        {
            InitializeComponent();


            for (int i = 0; i < NUM_PREVIEW_CAMERAS; i++)
            {
                viewerControls[i] = ClientControl.Instance.GenerateAdminImageViewerControl();
            }

            for (int i = 0; i < NUM_PREVIEW_CAMERAS; i++)
            {
                viewerControls[i].Width = camera1.Width - 20;
                viewerControls[i].Height = camera1.Height - 30;
                viewerControls[i].Top = 15;
                viewerControls[i].Left = 10;
                viewerControls[i].Visible = true;
                viewerControls[i].Name = "ViewerControl";
            }


            camera1.Controls.Add(viewerControls[0]);
            camera2.Controls.Add(viewerControls[1]);
            camera3.Controls.Add(viewerControls[2]);
            camera4.Controls.Add(viewerControls[3]);

            if (cameraList == null)
            {
                Task.Factory.StartNew(() =>
                {
                    cameraList = new List<Item>() { new Item() { FQID = new FQID(null, Guid.Empty, Guid.Empty, FolderType.No, Guid.Empty), Name = "None" } }.Concat(CameraUtility.GetCamerasList()).Distinct().ToList();
                    ItemSearchHandler = new ItemSearchHandler(cameraList);
                    comboBoxCamera1.KeyUp += ItemSearchHandler.ItemComboSearch;
                    comboBoxCamera2.KeyUp += ItemSearchHandler.ItemComboSearch;
                    comboBoxCamera3.KeyUp += ItemSearchHandler.ItemComboSearch;
                    comboBoxCamera4.KeyUp += ItemSearchHandler.ItemComboSearch;
                }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default).ContinueWith((t) =>
                {
                    onChangeEnabled = false;
                    try
                    {
                        comboBoxCamera1.Items.AddRange(cameraList.ToArray());
                        comboBoxCamera2.Items.AddRange(cameraList.ToArray());
                        comboBoxCamera3.Items.AddRange(cameraList.ToArray());
                        comboBoxCamera4.Items.AddRange(cameraList.ToArray());
                        Camera1 = Camera1;
                        Camera2 = Camera2;
                        Camera3 = Camera3;
                        Camera4 = Camera4;

                        AutoCompleteStringCollection cameraStrings = new AutoCompleteStringCollection();
                        cameraList.ForEach(x => cameraStrings.Add(x.Name));

                        comboBoxCamera1.AutoCompleteMode = AutoCompleteMode.Suggest;
                        comboBoxCamera1.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        comboBoxCamera1.AutoCompleteCustomSource = cameraStrings;

                        comboBoxCamera2.AutoCompleteMode = AutoCompleteMode.Suggest;
                        comboBoxCamera2.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        comboBoxCamera2.AutoCompleteCustomSource = cameraStrings;
                    }
                    catch (Exception)
                    {

                    }
                    onChangeEnabled = true;
                });
            }
            else
            {
                onChangeEnabled = false;
                comboBoxCamera1.Items.AddRange(cameraList.ToArray());
                comboBoxCamera1.SelectedIndex = 0;
                comboBoxCamera2.Items.AddRange(cameraList.ToArray());
                comboBoxCamera2.SelectedIndex = 0;
                comboBoxCamera3.Items.AddRange(cameraList.ToArray());
                comboBoxCamera3.SelectedIndex = 0;
                comboBoxCamera4.Items.AddRange(cameraList.ToArray());
                comboBoxCamera4.SelectedIndex = 0;
            }

            MessageCommunicationManager.Start(EnvironmentManager.Instance.CurrentSite.ServerId);
            _client = MessageCommunicationManager.Get(EnvironmentManager.Instance.CurrentSite.ServerId);

                ThreadPool.QueueUserWorkItem((x) =>
            {
                try
                {
                    Thread.Sleep(1000);
                    _mapResponseFilter = _client.RegisterCommunicationFilter(MapResponseHandler,
                        new CommunicationIdFilter(MessageId.Server.GetMapResponse));
                    GetMaps();
                }
                catch (Exception)
                { }
            });
        }

        internal String DisplayName
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }

        private Guid cam1, cam2, cam3, cam4;

        public Guid Camera1
        {
            get
            {
                return cam1;
            }

            set
            {
                cam1 = value;
                if(value == Guid.Empty)
                {
                    if(comboBoxCamera1.Items.Count > 0) 
                    {
                        comboBoxCamera1.SelectedIndex = 0;
                    }
                    displayCameraFootage(1, Camera1);
                }
                if (comboBoxCamera1.Items.Count > 0)
                {
                    for (int i = 0; i < comboBoxCamera1.Items.Count; i++)
                    {
                        if ((comboBoxCamera1.Items[i] as Item) != null && (comboBoxCamera1.Items[i] as Item).FQID.ObjectId == value)
                        {
                            camera1.Text = (comboBoxCamera1.Items[i] as Item).Name;
                            comboBoxCamera1.SelectedIndex = i;
                            displayCameraFootage(1, Camera1);
                            return;
                        }
                    }
                }
            }
        }


        public Guid Camera2
        {
            get
            {
                return cam2;
            }

            set
            {
                cam2 = value;
                if (value == Guid.Empty)
                {
                    if (comboBoxCamera2.Items.Count > 0)
                    {
                        comboBoxCamera2.SelectedIndex = 0;
                    }
                    displayCameraFootage(2, Camera2);
                }

                if (comboBoxCamera2.Items.Count > 0)
                {
                    for (int i = 0; i < comboBoxCamera2.Items.Count; i++)
                    {
                        if ((comboBoxCamera2.Items[i] as Item) != null && (comboBoxCamera2.Items[i] as Item).FQID.ObjectId == value)
                        {
                            camera2.Text = (comboBoxCamera2.Items[i] as Item).Name;
                            comboBoxCamera2.SelectedIndex = i;
                            displayCameraFootage(2, Camera2);
                            return;
                        }
                    }
                }
            }
        }


        public Guid Camera3
        {
            get
            {
                return cam3;
            }

            set
            {
                cam3 = value;
                if (value == Guid.Empty)
                {
                    if (comboBoxCamera3.Items.Count > 0)
                    {
                        comboBoxCamera3.SelectedIndex = 0;
                    }
                    displayCameraFootage(3, Camera3);
                }

                if (comboBoxCamera3.Items.Count > 0)
                {
                    for (int i = 0; i < comboBoxCamera3.Items.Count; i++)
                    {
                        if ((comboBoxCamera3.Items[i] as Item) != null && (comboBoxCamera3.Items[i] as Item).FQID.ObjectId == value)
                        {
                            camera3.Text = (comboBoxCamera3.Items[i] as Item).Name;
                            comboBoxCamera3.SelectedIndex = i;
                            displayCameraFootage(3, Camera3);
                            return;
                        }
                    }
                }
            }
        }


        public Guid Camera4
        {
            get
            {
                return cam4;
            }

            set
            {
                cam4 = value;
                if (value == Guid.Empty)
                {
                    if (comboBoxCamera4.Items.Count > 0)
                    {
                        comboBoxCamera4.SelectedIndex = 0;
                    }
                    displayCameraFootage(4, Camera4);
                }

                if (comboBoxCamera4.Items.Count > 0)
                {
                    for (int i = 0; i < comboBoxCamera4.Items.Count; i++)
                    {
                        if ((comboBoxCamera4.Items[i] as Item) != null && (comboBoxCamera4.Items[i] as Item).FQID.ObjectId == value)
                        {
                            camera4.Text = (comboBoxCamera4.Items[i] as Item).Name;
                            comboBoxCamera4.SelectedIndex = i;
                            displayCameraFootage(4, Camera4);
                            return;
                        }
                    }
                }
            }
        }

        private object MapResponseHandler(VideoOS.Platform.Messaging.Message message, FQID to, FQID from)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MessageReceiver(MapResponseHandler), new object[] { message, to, from });
                return null;
            }
            else
            {
                MapResponseData data = message.Data as MapResponseData;
                if (data != null)
                {
                    List<Map> maps = Map.Maps(data.MapCollection);
                    maps.Insert(0, new Map("", "")); //Insert blank option
                    onChangeEnabled = false;
                    comboBoxMap.DataSource = maps;
                    comboBoxMap.DisplayMember = "DisplayName";
                    comboBoxMap.ValueMember = "Id";
                    if (mapid != null) { MapID = mapid; };
                    onChangeEnabled = true;
                }

                return null;
            }
        }

        /// <summary>
        /// Ensure that all user entries will call this method to enable the Save button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void OnUserChange(object sender, EventArgs e)
        {
            if (ConfigurationChangedByUser != null && !fillingContent)
            {
                ConfigurationChangedByUser(this, new EventArgs());
            }
        }


        private void displayCameraFootage(int cameraPreviewNo, Guid cameraId)
        {
            if (cameraId == null) return;

            if (!Guid.Empty.Equals(cameraId)) //CameraId should never be empty, but we handle this just in case
            {
                if (viewerControls[cameraPreviewNo - 1].State != AdminImageState.Closed)
                {
                    try
                    {
                        viewerControls[cameraPreviewNo - 1].CloseVideo();
                    }
                    catch { }
                }
                try
                {
                    OnUserChange(null, null);

                    viewerControls[cameraPreviewNo - 1].OpenVideo(cameraId);
                    viewerControls[cameraPreviewNo - 1].Visible = true;
                }
                catch { }
            }
            else
            {
                if (viewerControls[cameraPreviewNo - 1].State != AdminImageState.Closed)
                {
                    try
                    {
                        viewerControls[cameraPreviewNo - 1].CloseVideo();
                    }
                    catch { }
                }
                viewerControls[cameraPreviewNo - 1].Visible = false;
            }
        }

        private void comboBoxCamera1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Camera1 = (comboBoxCamera1.Items[comboBoxCamera1.SelectedIndex] as Item).FQID.ObjectId;
        }

        private void comboBoxCamera2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Camera2 = (comboBoxCamera2.Items[comboBoxCamera2.SelectedIndex] as Item).FQID.ObjectId;
        }

        private void comboBoxCamera3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Camera3 = (comboBoxCamera3.Items[comboBoxCamera3.SelectedIndex] as Item).FQID.ObjectId;
        }

        private void comboBoxCamera4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Camera4 = (comboBoxCamera4.Items[comboBoxCamera4.SelectedIndex] as Item).FQID.ObjectId;
        }

        void SetPropertyGuid(Item Item, string name, Guid value)
        {
            Item.Properties[name] = value.ToString();
        }

        Guid GetPropertyGuid(Item item, string name)
        {
            if(!item.Properties.ContainsKey(name))
            {
                return Guid.Empty;
            }
            return item.Properties[name] != null ? Guid.Parse(item.Properties[name]) : Guid.Empty;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            OnUserChange(sender, e);
        }

        private bool fillingContent = true;

        private void comboBoxMap_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnUserChange(sender, e);
        }


        internal void FillContent(Item item)
        {
            fillingContent = true;
            textBox1.Text = item.Name;
            Camera1 = GetPropertyGuid(item, RadiniumSASDefinition.RADINIUM_CAMERA1);
            Camera2 = GetPropertyGuid(item, RadiniumSASDefinition.RADINIUM_CAMERA2);
            Camera3 = GetPropertyGuid(item, RadiniumSASDefinition.RADINIUM_CAMERA3);
            Camera4 = GetPropertyGuid(item, RadiniumSASDefinition.RADINIUM_CAMERA4);
            MapID = item.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_MAP) ? item.Properties[RadiniumSASDefinition.RADINIUM_MAP] : null;
            fillingContent = false;
        }

        internal void UpdateItem(Item item)
        {
            item.Name = DisplayName;
            SetPropertyGuid(item, RadiniumSASDefinition.RADINIUM_CAMERA1, Camera1);
            SetPropertyGuid(item, RadiniumSASDefinition.RADINIUM_CAMERA2, Camera2);
            SetPropertyGuid(item, RadiniumSASDefinition.RADINIUM_CAMERA3, Camera3);
            SetPropertyGuid(item, RadiniumSASDefinition.RADINIUM_CAMERA4, Camera4);
            item.Properties[RadiniumSASDefinition.RADINIUM_MAP] = MapID;
        }

        internal void ClearContent()
        {
            textBox1.Text = "";
        }

    }
}
