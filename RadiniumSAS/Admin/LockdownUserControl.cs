using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VideoOS.Platform;
using RadiniumSAS.Background;

namespace RadiniumSAS.Admin
{
    /// <summary>
    /// This control should contain the fields required for generating a valid Item.
    /// The template only has a Name field, but other fields should be added as required.
    /// 
    /// This dialog will be opened in a new form when user is selecting "New... " on the context menu in the administrator.
    /// When user presses "OK" the ItemManager.ValidateAddUserControl is called for validation, and if correct,
    /// the ItemManager.CreateItem is called with this class as the 3rd parameter.
    /// </summary>
    public partial class LockdownUserControl : UserControl
    {

        private class LockItem
        {
            public string LockGuid;
            public string Name;
            public string LockType;
            public override string ToString()
            {
                return Name;
            }
        }

        private class LockItemComparer : IComparer<LockItem>
        {
            public int Compare(LockItem a, LockItem b)
            {
                return a.ToString().CompareTo(b.ToString());
            }
        }

        private LockItemComparer comparer = new LockItemComparer();

        private List<LockItem> allLocksList = null;
        private List<LockItem> selectedLocksList = null;
        private List<LockItem> unselectedLocksList = null;
        public LockdownUserControl()
        {
            InitializeComponent();

            allLocksList = Configuration.Instance.GetItemConfigurations(RadiniumSASDefinition.RadiniumSASPluginId, null, RadiniumSASDefinition.RadiniumSASKind)
                .Where(x => x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ONLINE_DOOR_TYPE)
                    &&
                    x.Properties[RadiniumSASDefinition.RADINIUM_ONLINE_DOOR_TYPE] != DoorType.UNKNOWN.ToString()
                )
                .Select(x => 
                new LockItem()
                {
                    Name = x.Name,
                    LockType = x.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_ITEM_TYPE) ? x.Properties[RadiniumSASDefinition.RADINIUM_ITEM_TYPE] : RadiniumSASDefinition.RADINIUM_ITEMTYPE_DOOR,
                    LockGuid = x.FQID.ObjectId.ToString()
                }).OrderBy(y => y.ToString()).ToList();
        }

        public string DisplayName
        {
            get 
            {
                return textBoxName.Text;
            }
            set 
            {
                if(textBoxName != null)
                {
                    textBoxName.Text = value;
                }
            }
        }

        private bool fillingContent = true;

        internal void OnUserChange(object sender, EventArgs e)
        {
            if (ConfigurationChangedByUser != null && !fillingContent)
            {
                ConfigurationChangedByUser(this, new EventArgs());
            }
        }

        internal event EventHandler ConfigurationChangedByUser;

        internal void FillContent(Item item)
        {
            fillingContent = true;
            DisplayName = item.Name;
            textBoxSearch.Text = "";
            if(item.Properties.ContainsKey(RadiniumSASDefinition.RADINIUM_LOCKDOWN_GROUP))
            {
                string lockdownItemGuids = item.Properties[RadiniumSASDefinition.RADINIUM_LOCKDOWN_GROUP];
                string[] splitGuids = lockdownItemGuids.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                selectedLocksList = allLocksList.Where(x => splitGuids.Contains(x.LockGuid)).OrderBy(x => x.ToString()).ToList();
                unselectedLocksList = allLocksList.Where(x => !splitGuids.Contains(x.LockGuid)).OrderBy(x => x.ToString()).ToList();
            }
            else
            {
                unselectedLocksList = allLocksList.OrderBy(x => x.ToString()).ToList();
                selectedLocksList = new List<LockItem>();
            }

            listBoxUnselectedDoors.DataSource = unselectedLocksList;
            listBoxSelectedDoors.DataSource = selectedLocksList;
            fillingContent = false;
        }

        internal void ClearContent()
        {
            textBoxName.Text = "";
        }

        internal void UpdateItem(Item item)
        {
            item.Name = DisplayName;
            item.Properties[RadiniumSASDefinition.RADINIUM_LOCKDOWN_GROUP] = string.Join(";", selectedLocksList.Select(x => x.LockGuid).ToArray());
        }

        /// <summary>
        /// The name entered by the user
        /// </summary>
        public string ItemName
        {
            set { textBoxName.Text = value; }
            get { return textBoxName.Text; }
        }

        private string filterText = "";
        private void filterItems()
        {
            listBoxUnselectedDoors.DataSource = unselectedLocksList.Where(x => x.Name.ToLowerInvariant().Contains(filterText.ToLowerInvariant())).OrderBy(x => x.ToString()).ToList();
            listBoxSelectedDoors.DataSource = selectedLocksList.Where(x => x.Name.ToLowerInvariant().Contains(filterText.ToLowerInvariant())).OrderBy(x => x.ToString()).ToList();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            for(int i = listBoxUnselectedDoors.SelectedItems.Count - 1; i >= 0; i--)
            {
                LockItem item = listBoxUnselectedDoors.SelectedItems[i] as LockItem;
                selectedLocksList.Add(item);
                unselectedLocksList.Remove(item);
            }
            selectedLocksList.Sort(comparer);
            filterItems();
            OnUserChange(sender, e);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            for (int i = listBoxSelectedDoors.SelectedItems.Count - 1; i >= 0; i--)
            {
                LockItem item = listBoxSelectedDoors.SelectedItems[i] as LockItem;
                unselectedLocksList.Add(item);
                selectedLocksList.Remove(item);
            }
            unselectedLocksList.Sort(comparer);
            filterItems();
            OnUserChange(sender, e);
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            filterText = textBoxSearch.Text;
            filterItems();
        }
    }
}
